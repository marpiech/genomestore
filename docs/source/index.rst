.. seqinspector documentation master file, created by
   sphinx-quickstart on Wed Jan 22 22:23:51 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Seqinspector: position-based navigation through the ChIP-seq data landscape to identify gene expression regulators
==================================================================================================================

=================
Table of contents
=================
.. toctree::
   :maxdepth: 1
   :numbered:

   seqinspector
   seqone

==========
License
==========
seqinspector is freely available under a GNU Public License (Version 2).

=================
Contact
=================
piechota . marcin [at] gmail . com
