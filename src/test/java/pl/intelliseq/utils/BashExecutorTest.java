package pl.intelliseq.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BashExecutorTest {

	BashExecutor exec = new BashExecutor();
	
	@Test
	public void bashExecutorTest() throws Exception {
		
		/*
		 * testing if bash process works
		 */
		assertEquals(exec.execute("ls src"), "main\ntest\n");
		
		/*
		 * testing if Jim Kent's executables are working
		 */
		String process = 
				"executables/bigWigSummary data/mm9_SRF_1.bw chr14 70476252 70478252 1";
		
		assertEquals(exec.execute(process), "1.25385\n");

		/*
		 * testing if 
		 */
		process = 
				"executables/bigWigMaxOverBed data/mm9_SRF_1.bw stdin stdout";
		String stdin = "chr14\t70476252\t70478252\t1";
		
		System.out.println(exec.execute(process, stdin));
	}
	
}
