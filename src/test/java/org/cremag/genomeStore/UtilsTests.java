package org.cremag.genomeStore;

import java.net.URISyntaxException;
import java.util.Set;

import org.apache.log4j.Logger;
import org.cremag.dataSource.biomart.BiomartDAO;
import org.cremag.dataSource.flatFile.FlatFileDAO;
import org.cremag.genomic.Position;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class UtilsTests {
private static final Logger logger = Logger.getLogger(UtilsTests.class);
	
	ApplicationContext context = new FileSystemXmlApplicationContext(new String[] {
		"conf/application-context.xml"});

	FlatFileDAO flatFileDAO = (FlatFileDAO) context.getBean("flatFileDAO");
	BiomartDAO biomartDAO = (BiomartDAO) context.getBean("biomartDAO");
	
	@Before
	public void setUp() {
		logger.info("Setting up Genome Store Test");
	}

	@Test
	public void test() throws URISyntaxException {
		Set<String> symbols = flatFileDAO.getGeneSymbols("Egr", 5);
		for(String symbol : symbols)
			logger.info("UtilsTest; symbol: " + symbol);
		
		Set<Position> tss = biomartDAO.getListOfTranscriptionStartSitesForGene("Egr2", "mm9");
		for(Position pos : tss)
			System.out.println(pos);
	}
}
