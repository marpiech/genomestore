package org.cremag.genomeStore;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.cremag.genomic.BedItem;
import org.cremag.tools.LiftOver;
import org.junit.Test;

public class ToolsTest {
	
	@Test
	public void liftOverTest() throws IOException, InterruptedException {
		
		List <BedItem> bedItems = new ArrayList <BedItem>();
		bedItems.add(new BedItem("chr2:73519829-73521829"));
		bedItems.add(new BedItem("chr8:22547737-22551737"));
		assertEquals(LiftOver.lift("hg19", "mm9", bedItems).size(), 2);

	}
	
}
