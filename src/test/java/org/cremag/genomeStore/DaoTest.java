package org.cremag.genomeStore;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.cremag.dao.AnnotationDAO;
import org.cremag.dao.GenomicFeaturesDAO;
import org.cremag.genomic.BedItem;
import org.cremag.genomic.Position;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"file:conf/dao-context.xml"})
public class DaoTest {

	private static final Logger logger = Logger.getLogger(DaoTest.class);
	
	@Autowired
	GenomicFeaturesDAO featuresDAO;
	
	@Autowired
	AnnotationDAO annotationDAO;
	
	@Before
	public void setUp() {
		logger.info("Setting up SqlDaoTest");
	}

	@Test
	public void featuresTest() throws Exception {

		assertEquals(featuresDAO.getSymbolsByBeginningString("Egr", "mm9").get(0), "Egr1");
		List <String> symbols = new ArrayList<String>();
		symbols.add("Egr2");
		symbols.add("Egr1");
		for(String symbol : featuresDAO.getEnsemblIDsBySymbol(symbols, "mm9")) {
			System.out.println(symbol);
		}
		for(Position position : featuresDAO.getStartPositionsByEnsemblID(featuresDAO.getEnsemblIDsBySymbol(symbols, "mm9"), "mm9")) {
			System.out.println(position);
		}		
	}
	
	@Test
	public void annotationTest() throws Exception {
		System.out.println("Start");
		List <BedItem> bedItems = new ArrayList <BedItem> ();
		bedItems.add(new BedItem("chr10:66999617-67001617"));
		bedItems.add(new BedItem("chr18:35019861-35021861"));
		bedItems.add(new BedItem("chr14:70476252-70478252"));
		bedItems.add(new BedItem("chr6:85462583-85464583"));
		for(String symbol : annotationDAO.annotate(bedItems, "mm9"))
			System.out.println(symbol);
		System.out.println("End");		
	}
}
