package org.cremag.genomeStore;

import static org.junit.Assert.*;

import org.cremag.genomeStore.web.RestController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
	"file:src/main/webapp/WEB-INF/spring/test-controller-context.xml"})
public class GenomeStoreControllerTest {
	
	private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    
    @Autowired
	private RestController restController;
	
    @Before
    public void setUp() {
    	request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
    }

    @Test
    public void testGetSymbols() throws Exception {
   	
    	request.setMethod("GET");
        request.setRequestURI("/getsymbols.htm");
        request.addParameter("start", "Egr");
        request.addParameter("genome", "mm9");

    	@SuppressWarnings("unused")
		ModelAndView mav = new AnnotationMethodHandlerAdapter()
                .handle(request, response, restController);
        
        assertEquals(response.getContentAsString(), "[{\"id\":\"Egr1\",\"value\":\"Egr1\"},{\"id\":\"Egr2\",\"value\":\"Egr2\"},{\"id\":\"Egr3\",\"value\":\"Egr3\"},{\"id\":\"Egr4\",\"value\":\"Egr4\"}]");

    }

    @Test
    public void testGetSymbolsHuman() throws Exception {
   	
    	request.setMethod("GET");
        request.setRequestURI("/getsymbols.htm");
        request.addParameter("start", "EGR");
        request.addParameter("genome", "hg19");

    	@SuppressWarnings("unused")
		ModelAndView mav = new AnnotationMethodHandlerAdapter()
                .handle(request, response, restController);
        
        //assertEquals(response.getContentAsString(), "[{\"id\":\"Egr1\",\"value\":\"Egr1\"},{\"id\":\"Egr2\",\"value\":\"Egr2\"},{\"id\":\"Egr3\",\"value\":\"Egr3\"},{\"id\":\"Egr4\",\"value\":\"Egr4\"}]");
        
        System.out.println("SYMBOLS: " + response.getContentAsString());
       
    }
    
    @Test
    public void testGetTrackDescription() throws Exception {
   	
        request.setMethod("GET");
        request.setRequestURI("/gettrackdescription.htm");
        request.addParameter("trackId", "mm9_CREB_1.bw");
        request.addParameter("genome", "mm9");

    	@SuppressWarnings("unused")
		ModelAndView mav = new AnnotationMethodHandlerAdapter()
                .handle(request, response, restController);
        
        assertEquals(response.getContentAsString(), "[{\"value\":\"Polymerase\",\"key\":\"class\"},{\"value\":\"CREB1_1\",\"key\":\"name\"}]");

    }  
}