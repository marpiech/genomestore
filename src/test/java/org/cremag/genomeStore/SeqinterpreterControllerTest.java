package org.cremag.genomeStore;

import static org.junit.Assert.*;

import org.cremag.seqinterpreter.SeqinterpreterController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
	"file:src/main/webapp/WEB-INF/spring/test-controller-context.xml"})
public class SeqinterpreterControllerTest {
	
	private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    
    @Autowired
	private SeqinterpreterController controller;
	
	@Before
    public void setUp() {
    	request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();        
    }

    @Test
    public void testSubmit() throws Exception {

    	request.setMethod("POST");
        request.setRequestURI("/getstatformultibed.htm");
        request.addParameter("genome", "mm9");
        request.addParameter("database", "mm9");
        request.addParameter("query", "chr10:66999617-67001617\nchr18:35019861-35021861\nchr14:70476252-70478252");
        request.addParameter("background", "chr10.xml");
        
        @SuppressWarnings("unused")
		final ModelAndView mavBack = new AnnotationMethodHandlerAdapter()
                .handle(request, response, controller);
        
        assertEquals(response.getContentType(), "application/json");
        
        System.out.println(response.getContentAsString());

    }
    
}