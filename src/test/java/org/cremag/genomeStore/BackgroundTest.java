/*
 * Copyright (C) 2012 - Marcin Piechota
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 Unported (CC BY-NC 3.0)
 * You may obtain a copy of the License at http://creativecommons.org/licenses/by-nc/3.0/legalcode
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package org.cremag.genomeStore;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.apache.log4j.Logger;
import org.cremag.genomic.BedItem;
import org.cremag.utils.file.BedFile;
import org.cremag.utils.stats.StatisticalResultSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author marpiech
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
	"classpath:org/cremag/genomeStore/mock/mock-context.xml"})
public class BackgroundTest {
	
	@Autowired
	GenomeStore genomeStore;
	
	private static final Logger logger = Logger.getLogger(BackgroundTest.class);

	@Before
	public void setUp() {
		logger.info("Setting up QuerySet Test");
	}

	@Test
	public void test() throws Exception {
		
		/* setting genome */
		Genome genome = genomeStore.getGenome("mm9");
		List<BedItem> bedItems = new BedFile("src/test/resources/chr10.bgnd.bed").getBedItems();
		
		/* computing background */
		QuerySet querySet = new QuerySet(bedItems, "/tmp/test_background.xml");
		querySet.compute(genome);
		querySet.save();
		querySet.load();
		
		/* testing background */
		BedItem testBed = new BedItem("chr12:56592634-56594634");
		StatisticalResultSet result = querySet.zScoreTest(testBed);
		
		assertNotNull(result);
		
	}	
}
