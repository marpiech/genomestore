package org.cremag.genomeStore;

import static org.junit.Assert.*;

import java.io.File;
import net.sf.picard.liftover.LiftOver;
import net.sf.picard.util.Interval;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

public class LiftOverTest {
	private static final Logger logger = Logger.getLogger(LiftOverTest.class);

	@Before
	public void setUp() {
		logger.info("Setting up Lift Over Test");
	}

	@Test
	public void test() {
		LiftOver liftOver = new LiftOver(new File("src/test/resources/mm10ToMm9.over.chain.gz"));
		Interval out = liftOver.liftOver(new Interval("chr10", 10000000, 10001000));
		assertNotNull(out);
	}
	
}
