package org.cremag.genomeStore;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.log4j.Logger;
import org.cremag.genomic.BedItem;
import org.cremag.utils.file.genomic.BigWig;
import org.cremag.utils.file.genomic.GenomicTrack;
import org.cremag.utils.plot.ColorBrewer;
import org.cremag.utils.plot.PlotProperties;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class SeqinspectorHistogramTest {
private static final Logger logger = Logger.getLogger(UtilsTests.class);
	
	ApplicationContext context = new FileSystemXmlApplicationContext(new String[] {
		"conf/application-context.xml"});
	GenomeStore genomeStore = (GenomeStore) context.getBean("genomeStore");
	
	@Before
	public void setUp() {
		logger.info("Setting up seQinspector");
	}

	@Test
	public void test() throws IOException {
		Genome genome = genomeStore.getGenome("mm9");
		

        // Create an instance of the SVG Generator.
        SVGGraphics2D svgGenerator = new SVGGraphics2D(
        		GenericDOMImplementation.getDOMImplementation().createDocument(
        				"http://www.w3.org/2000/svg", "svg", null)
        				);

   		//BedItem bed = new BedItem("chr15:74500300-74507500");
   		BedItem bed = new BedItem("chr13:94,050,060-94,200,000");		
        
   		PlotProperties plot = new PlotProperties();
   		plot.setX(80);
   		plot.setY(0);
  		plot.setWidth(600);
  		plot.setHeight(40);
  		plot.setColor(ColorBrewer.GREY_80);
  		bed.plot(plot, svgGenerator);
        		
  		plot.setY(plot.getY() + plot.getHeight() + 20);
   		plot.setHeight(50);
   		plot.setLegend("Ensembl genes");
        		
   		GenomicTrack track = genome.selectTracksByKeyAndElement("name", "UCSC genes").iterator().next();
   		track.plot(plot, svgGenerator, bed);
        		
   		//GenomicFile file = genome.selectTracksByKeyAndElement("Name", "CREB_01").iterator().next();
   		for(BigWig file : genome.getBigWigTracks()) {
   			plot.setLegend(genome.getNameOfTrack(file));
   			plot.changeColor();
   			plot.setY(plot.getY() + plot.getHeight() + 5);
   			plot.setHeight(40);
   			file.plot(plot, svgGenerator, bed);
   		}
		        
        //Writer out = new FileWriter(new File("/home/marpiech/Resources/Emd_85s.svg"));
   		Writer out = new FileWriter(new File("/tmp/plot.svg"));
		BufferedWriter bw = new BufferedWriter(out);
		svgGenerator.stream(bw);
	}
}
