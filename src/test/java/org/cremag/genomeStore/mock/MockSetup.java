package org.cremag.genomeStore.mock;

import org.apache.log4j.Logger;
import org.cremag.genomeStore.Genome;
import org.cremag.genomeStore.GenomeStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class MockSetup {
	
	/*private static final Logger logger = Logger.getLogger(MockSetup.class);
	
	@Autowired
	private GenomeStore genomeStore;
	
	Genome mouseGenome;
	Genome humanGenome;
	
	public void init() {
	
		logger.trace("Mock Init()");
		
		mouseGenome = genomeStore.getGenome("mm9");

		try {
			mouseGenome.addTrack("http://edison.cremag.org/resources/genomeStore/mm9/cremag-anno/mousensembl.bb", true);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		try {
			mouseGenome.addTrack("src/test/resources/test-data/mm9_CREB_1.bw", false);
			mouseGenome.addTrack("src/test/resources/test-data/mm9_CREB_2.bw", false);
			mouseGenome.addTrack("src/test/resources/test-data/mm9_CREB_3.bw", false);
			mouseGenome.addTrack("src/test/resources/test-data/mm9_CREB_4.bw", false);
			mouseGenome.addTrack("src/test/resources/test-data/mm9_SRF_1.bw", false);
			mouseGenome.addTrack("src/test/resources/test-data/mm9_SRF_2.bw", false);
			mouseGenome.addTrack("src/test/resources/test-data/mm9_SRF_3.bw", false);
			mouseGenome.addTrack("src/test/resources/test-data/mm9_SRF_4.bw", false);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		try {
			mouseGenome.addBackground("src/test/resources/chr10.bgnd.bed", "chr10", "desc");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		humanGenome = genomeStore.getGenome("hg19");

		try {
			humanGenome.addTrack("http://edison.cremag.org/resources/genomeStore/hg19/cremag-anno/humanensembl.bb", true);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		try {
			humanGenome.addTrack("src/test/resources/test-data/hg19_CREB_1.bw", false);
			humanGenome.addTrack("src/test/resources/test-data/hg19_CREB_2.bw", false);
			humanGenome.addTrack("src/test/resources/test-data/hg19_CREB_3.bw", false);
			humanGenome.addTrack("src/test/resources/test-data/hg19_CREB_4.bw", false);
			humanGenome.addTrack("src/test/resources/test-data/hg19_SRF_1.bw", false);
			humanGenome.addTrack("src/test/resources/test-data/hg19_SRF_2.bw", false);
			humanGenome.addTrack("src/test/resources/test-data/hg19_SRF_3.bw", false);
			humanGenome.addTrack("src/test/resources/test-data/hg19_SRF_4.bw", false);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		try {
			humanGenome.addBackground("src/test/resources/human.bed", "chr10", "desc");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		
	}
	
	public void destroy() {
		
		logger.trace("Mock Destroy()");
		
		mouseGenome.removeTrack("mm9_CREB_1.bw");
		mouseGenome.removeTrack("mm9_CREB_2.bw");
		mouseGenome.removeTrack("mm9_CREB_3.bw");
		mouseGenome.removeTrack("mm9_CREB_4.bw");
		mouseGenome.removeTrack("mm9_SRF_1.bw");
		mouseGenome.removeTrack("mm9_SRF_2.bw");
		mouseGenome.removeTrack("mm9_SRF_3.bw");
		mouseGenome.removeTrack("mm9_SRF_4.bw");
		
		humanGenome.removeTrack("hg19_CREB_1.bw");
		humanGenome.removeTrack("hg19_CREB_2.bw");
		humanGenome.removeTrack("hg19_CREB_3.bw");
		humanGenome.removeTrack("hg19_CREB_4.bw");
		humanGenome.removeTrack("hg19_SRF_1.bw");
		humanGenome.removeTrack("hg19_SRF_2.bw");
		humanGenome.removeTrack("hg19_SRF_3.bw");
		humanGenome.removeTrack("hg19_SRF_4.bw");
	}*/
}
