package org.cremag.genomeStore;

import static org.junit.Assert.*;

import org.cremag.seqinspector.SeqinspectorController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;
 

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
	"file:src/main/webapp/WEB-INF/spring/test-controller-context.xml"})
public class SeqinspectorControllerTest {
	
	private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    
    @Autowired
	private SeqinspectorController controller;
	
	@Before
    public void setUp() {
    	request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();        
    }

    //@Test
    public void testStatistics() throws Exception {

    	request.setMethod("GET");
        request.setRequestURI("/getscoresforbed.htm");
        request.addParameter("genome", "mm9");
        request.addParameter("database", "mm9");
        request.addParameter("range", "chr14:70476252-70478252");
        request.addParameter("background", "chr10");

        @SuppressWarnings("unused")
		final ModelAndView mavBack = new AnnotationMethodHandlerAdapter()
                .handle(request, response, controller);
        
        assertEquals(response.getContentType(), "application/json");
        
        System.out.println("MOUSE: " + response.getContentAsString());
    }
    
    //@Test
    public void testStatisticsHuman() throws Exception {    
        
        request.setRequestURI("/getscoresforbed.htm");
        request.addParameter("genome", "hg19");
        request.addParameter("database", "hg19");
        request.addParameter("range", "chr10:67510223-67542223");
        request.addParameter("background", "chr10");

        @SuppressWarnings("unused")
		final ModelAndView mavHist = new AnnotationMethodHandlerAdapter()
        .handle(request, response, controller);
        
        assertEquals(response.getContentType(), "application/json");
        
        System.out.println("HUMAN: " + response.getContentAsString());
    }

    @Test
    public void testTss() throws Exception {

    	request.setMethod("GET");
        request.setRequestURI("/gettss.htm");
        request.addParameter("genome", "hg19");
        request.addParameter("symbol", "EGR4");
        
        @SuppressWarnings("unused")
		final ModelAndView mavBack = new AnnotationMethodHandlerAdapter()
                .handle(request, response, controller);
        
        //assertEquals(response.getContentType(), "application/json");
        
        System.out.println(response.getContentAsString());
    	
    }
    
	public SeqinspectorController getController() {
		return controller;
	}

	public void setController(SeqinspectorController controller) {
		this.controller = controller;
	}    
}