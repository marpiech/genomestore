package org.cremag.genomeStore;

import java.awt.Color;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;

import javax.imageio.ImageIO;

import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.log4j.Logger;
import org.cremag.utils.file.BamStock;
import org.cremag.utils.file.GtfFile;
import org.cremag.utils.genome.Gene;
import org.cremag.utils.plot.ColorBrewer;
import org.cremag.utils.plot.GenePlotter;
import org.cremag.utils.plot.GraphPlotter;
import org.junit.Test;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;


@SuppressWarnings("unused")
public class PlotterTest {
	
	private static final Logger logger = Logger.getLogger(PlotterTest.class);

	protected void setUp() {
		logger.trace("GtfTest setUp()");
	}

	@Test
	public void test() throws Exception {
		logger.trace("GtfTest test()");

		/*
		
		GtfFile gtfFile = new GtfFile("/home/marpiech/Resources/ensGene.gtf");
		Gene gene = gtfFile.readGene("ENSMUSG00000002831");

		GenePlotter genePlotter = new GenePlotter(gene);
		BamStock stock = new BamStock("/home/marpiech/Resources/bam/description");
		
		// Vector Graphics
		DOMImplementation domImpl =
		           GenericDOMImplementation.getDOMImplementation();

        // Create an instance of org.w3c.dom.Document.
        String svgNS = "http://www.w3.org/2000/svg";
        Document document = domImpl.createDocument(svgNS, "svg", null);

        // Create an instance of the SVG Generator.
        SVGGraphics2D svgGenerator = new SVGGraphics2D(document);

        // Ask the test to render into the SVG Graphics2D implementation.
        //new GraphPlotter(svgGenerator, 0, imageWidth, 0, imageHeight).plotBackground(Color.WHITE);
        //new GraphPlotter(svgGenerator, horizontalPosition, width, verticalPosition, height).plotLinedBackground(ColorBrewer.LIGHT_GREY, breaks, ColorBrewer.WHITE);
        //crebCoverage.plotTrack(svgGenerator, horizontalPosition, verticalPosition, width, height, color);
		        
        genePlotter.plot(800, 700, stock, svgGenerator);
		        
        // Finally, stream out SVG to the standard output using
        // UTF-8 encoding.
        boolean useCSS = true; // we want to use CSS style attributes
        //Writer out = new FileWriter(new File("/home/marpiech/Resources/Emd_85s.svg"));
        Writer out = new FileWriter(new File("plot.svg"));
        svgGenerator.stream(out, useCSS);
		
		//ImageIO.write(genePlotter.plot(800, 700, stock), "JPEG", new File("plot.jpg"));
		
		*/
		 
	}
}
