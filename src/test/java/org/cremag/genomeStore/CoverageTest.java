package org.cremag.genomeStore;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.cremag.dao.AnnotationDAO;
import org.cremag.dao.GenomicFeaturesDAO;
import org.cremag.genomic.BedItem;
import org.cremag.genomic.Position;
import org.cremag.utils.file.genomic.BigWig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"file:conf/application-context.xml"})
public class CoverageTest {

	private static final Logger logger = Logger.getLogger(CoverageTest.class);
	
	@Autowired
	GenomeStore genomeStore;
	
	@Before
	public void setUp() {
		logger.info("Setting up SqlDaoTest");
	}

	@Test
	public void coverageTest() throws Exception {

		Genome mm9 = genomeStore.getGenome("mm9");
		List <BigWig> tracks = mm9.getBigWigTracks();
		for (BigWig track : tracks) {
			System.out.println(track.getCoverage(new BedItem("chr14", 70476500, 70476750)).getSum());
		}
		
	}
	
	
}
