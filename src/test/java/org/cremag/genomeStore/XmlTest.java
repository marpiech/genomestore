package org.cremag.genomeStore;

import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XmlTest {
	private static final Logger logger = Logger.getLogger(XmlTest.class);

	@Before
	public void setUp() {
		logger.info("Setting up Xml Test");
	}

	@Test
	public void test() throws Exception {
					
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		Document doc = docBuilder.newDocument();
		
		Element rootElement = doc.createElement("genome");
		
		doc.appendChild(rootElement);
		
		
		Element staff = doc.createElement("track");
		staff.setAttribute("name", "qq");
		rootElement.appendChild(staff);
		
		Element off = doc.createElement("off");
		off.setTextContent("Content");
		staff.appendChild(off);
		
		System.out.println(staff.getElementsByTagName("off").item(0).getTextContent());
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		//transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
		DOMSource source = new DOMSource(doc);
		StringWriter sw = new StringWriter();
		StreamResult result = new StreamResult(sw);
		transformer.transform(source, result);
		String xmlString = sw.toString();
		
		System.out.println(xmlString);
		
	}
	
}
