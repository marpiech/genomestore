package org.cremag.genomeStore;

import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.log4j.Logger;
import org.cremag.utils.file.BedFile;
import org.cremag.utils.file.genomic.BigWig;
import org.cremag.utils.plot.ColorBrewer;
import org.cremag.utils.plot.PlotProperties;
import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
	"classpath:org/cremag/genomeStore/mock/mock-context.xml"})
public class GenomeStoreTest {
	
	private static final Logger logger = Logger.getLogger(GenomeStoreTest.class);
	
	@Autowired
	GenomeStore genomeStore;
	
	@Before
	public void setUp() {
		logger.info("Setting up Genome Store Test");
	}

	@Test
	public void test() throws Exception {
		logger.info("testing Genome Store");
		
		/* Genome humanGenome = genomeStore.getGenome("hg18");
		assertNotNull(humanGenome);
		genomeStore.deleteGenome("hg18"); */

		Genome genome = genomeStore.getGenome("mm9");
		assertNotNull(genome);

		BigWig track = (BigWig) genome.getTrackById("mm9_SRF_4.bw");
		assertNotNull(track);
		
		SVGGraphics2D svgGenerator = new SVGGraphics2D(
        		GenericDOMImplementation.getDOMImplementation().createDocument(
        				"http://www.w3.org/2000/svg", "svg", null)
        				);
		
		PlotProperties plot = new PlotProperties();
		plot.setX(30);
		plot.setY(30);
		plot.setWidth(300);
		plot.setHeight(100);
		
		QuerySet set1 = new QuerySet(new BedFile("src/test/resources/chr10.bgnd.bed").getBedItems());
		set1.compute(genome);
		set1.setName("Set1");
		QuerySet set2 = new QuerySet(new BedFile("src/test/resources/chr10.bgnd.bed").getBedItems());
		set2.setName("Set2");
		set2.compute(genome);
		List <QuerySet> querySets = new ArrayList<QuerySet>();
		querySets.add(set1);
		querySets.add(set2);
		
		
		track.plot(plot, svgGenerator, querySets);
        
		
        boolean useCSS = true;
        Writer out = new FileWriter(new File("/tmp/multi.svg"));
        svgGenerator.stream(out, useCSS);
		
	}
}
