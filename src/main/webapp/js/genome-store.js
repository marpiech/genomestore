// startWait($text); --- creates wait icon, requires css/images/loader.gif
// endWait(); --- closes wait icon
// overlay(); --- creates overlay or closes if exists
// function createDropDown($source, $container, $target_id);
// $.postJSON --- jQuery extension
// getURLParameter(name)

/* 
 * This function creates wait icon 
 */

function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
}

function startWait($text) {
	var $root = $(document.body);
	var $html = '<button id="dialog" class="darkbtn" style="padding:5 5 5 8px; display:none"><span class="label" style="height: 31px !important;"><img src="css/images/ajax-loader.gif" style="vertical-align:middle;"><span style="padding:40 5 20 5px">' + $text + '</span></span></button>';
	$root.append($html);
	
	$("#dialog").addClass("dialog-box")
       	.appendTo($root)
		.css({'position':'fixed', 'top':'40%', 'left':'45%'})
		.show();
	
	overlay();
}

function endWait() {
	if(!overlay()) {
		overlay();
	}
	$("#dialog").remove();
}

function overlay() {
	var $root = $(document.body);
	if ($("#overlay").length) {
		$("#overlay").remove();
		return true;
	} else {
		var $maskHeight = $(document).height();  
		var $maskWidth = $(window).width();
	
		$overlay = document.createElement("div");
		$($overlay).addClass("dialog-overlay")
	    	.appendTo($root)
			.attr('id', "overlay")
			.css({height:$maskHeight, width:$maskWidth})
			.show();
		return false;
	}
		
}
     
 

/* 
 * This function creates dropdown menu from select 
 * and requires jquery
 */

function createDropDown($source, $container, $target_id) {
	$container.html("");
	var $target = $target_id;
	var selected = $source.find("option[selected]"); // get selected <option>
	var options = $("option", $source); // get all <option> elements

	// create <dl> and <dt> with selected value inside it
	$container.append('<dl id="' + $target.substr(1)
			+ '" class="dropdown"></dl>')

	$($target).append(
			'<dt><a href="#">' + selected.text() + '<span class="value">'
					+ selected.val() + '</span></a></dt>')
	$($target).append('<dd><ul></ul></dd>')
	options.each(function() {
		$($target + " dd ul").append(
				'<li><a href="#">' + $(this).text() + '<span class="value">'
						+ $(this).val() + '</span></a></li>');
	});

	$($target_id + " dt a").click(function() {
		$($target_id + " dd ul").toggle();
	});

	$(document).bind('click', function(e) {
		var $clicked = $(e.target);
		if (!$clicked.parents().hasClass("dropdown"))
			$($target_id + " dd ul").hide();
	});

	$($target_id + " dd ul li a").click(function() {
		console.log("Clicked" + $(this).html());
		var text = $(this).html();
		$($target_id + " dt a").html(text);
		$($target_id + " dd ul").hide();
		$source.val($(this).find("span.value").html());
	});
}

jQuery.extend({
	   postJSON: function( url, data, callback) {
	      return jQuery.post(url, data, callback, "json");
	   }
	});

