<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<HTML>
<HEAD>
<TITLE>genomeStore</TITLE>
<LINK rel="stylesheet" type="text/css" href="css/genomeStore.css" />
<LINK rel="stylesheet" type="text/css" href="css/cremagOrgButtons.css" />
<LINK rel="stylesheet" type="text/css"
	href="css/cremagOrgModalDialog.css" />
<LINK rel="stylesheet" type="text/css" href="css/cremagOrgForm.css" />
</HEAD>
<BODY onload="load()">
	<DIV>GenomeStore :: hosted by cremag.org</DIV>
	<BR>
	<DIV>
		<button class="googlegray" onclick='describeSchema()'>Describe
			schema</button>
		<button class="googlegray" onclick='selectGenome()'>Select
			genome</button>
		<button class="googlegray" onclick='queryForBed()'>Query for
			bed</button>
		<button class="googlegray" onclick='queryForGraph()'>Query for
			graph</button>
	</DIV>
	<BR>
	<DIV id='response'></DIV>
	<script type="text/javascript">
		function load() {

			//HttpRequest
			xmlHttp = new XMLHttpRequest();
			if (xmlHttp == null) {
				alert("Your browser does not support XMLHTTP!");
				return;
			}

			document.getElementById("dialog-overlay").style.visibility = "hidden";

			pretag = "<pre style=\"background-color: #eeeeee; border: 1px dashed #999999; color: black; font-family: Andale Mono, Lucida Console, Monaco, fixed, monospace; font-size: 12px; line-height: 14px; overflow: auto; padding: 5px; width: 100%;\"><code>"
			postag = "</code></pre>";
		}

		function describeSchema() {
			var element = document.getElementById('response');
			element.innerHTML = '<p><em>Loading ...</em></p>';
			xmlHttp.open("GET", "schema.rest");
			xmlHttp.onreadystatechange = function() {
				element.innerHTML = '<p><em>Done ...</em></p>';
				if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
					element.innerHTML = pretag + xmlHttp.responseText + postag;
				}
			};
			xmlHttp.send(null);
		}
		function getGenome() {
			return document.getElementById("genomeSelector").options[document
				.getElementById("genomeSelector").selectedIndex].value;
		}
		function getPosition() {
			return document.getElementById("position").value;
		}
		function selectGenome() {
			document.getElementById('response').innerHTML = pretag
					+ "Selected Genome: "
					+ getGenome()
					+ postag;
			el = document.getElementById("dialog-overlay");
			el.style.visibility = (el.style.visibility == "visible") ? "hidden"
					: "visible";
			el = document.getElementById("dialog-selectGenome");
			el.style.visibility = (el.style.visibility == "visible") ? "hidden"
					: "visible";
		}
		function queryForBed() {
			//document.getElementById('response').innerHTML = pretag + "Selected Genome: " + document.getElementById("genomeSelector").options[document.getElementById("genomeSelector").selectedIndex].value + postag;
			el = document.getElementById("dialog-overlay");
			el.style.visibility = (el.style.visibility == "visible") ? "hidden"
					: "visible";
			el = document.getElementById("dialog-queryForBed");
			el.style.visibility = (el.style.visibility == "visible") ? "hidden"
					: "visible";
			//document.getElementById('response').innerHTML = '';
		}
		function queryForBedHtml() {
			el = document.getElementById("dialog-overlay");
			el.style.visibility = (el.style.visibility == "visible") ? "hidden"
					: "visible";
			el = document.getElementById("dialog-queryForBed");
			el.style.visibility = (el.style.visibility == "visible") ? "hidden"
					: "visible";
			var element = document.getElementById('response');
			element.innerHTML = '<p><em>Loading ...</em></p>';
			xmlHttp.open("GET", "bed.rest?genome=" + getGenome() + "&position=" + getPosition());
			xmlHttp.onreadystatechange = function() {
				element.innerHTML = '<p><em>Loading ...</em></p>';
				if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
					element.innerHTML = pretag + xmlHttp.responseText + postag;
				}
			};
			xmlHttp.send(null);
		}
		function queryForGraph() {
			//document.getElementById('response').innerHTML = pretag + "Selected Genome: " + document.getElementById("genomeSelector").options[document.getElementById("genomeSelector").selectedIndex].value + postag;
			el = document.getElementById("dialog-overlay");
			el.style.visibility = (el.style.visibility == "visible") ? "hidden"
					: "visible";
			el = document.getElementById("dialog-queryForBed");
			el.style.visibility = (el.style.visibility == "visible") ? "hidden"
					: "visible";
			//document.getElementById('response').innerHTML = '';
		}
		function queryForGraphHtml() {
			el = document.getElementById("dialog-overlay");
			el.style.visibility = (el.style.visibility == "visible") ? "hidden"
					: "visible";
			el = document.getElementById("dialog-queryForBed");
			el.style.visibility = (el.style.visibility == "visible") ? "hidden"
					: "visible";
			var element = document.getElementById('response');
			element.innerHTML = '<p><em>Loading ...</em></p>';
			xmlHttp.open("GET", "bed.rest?genome=" + getGenome() + "&position=" + getPosition());
			xmlHttp.onreadystatechange = function() {
				element.innerHTML = '<p><em>Loading ...</em></p>';
				if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
					element.innerHTML = pretag + xmlHttp.responseText + postag;
				}
			};
			xmlHttp.send(null);
		}
	</script>

	<div id="dialog-overlay" class="dialog-overlay"></div>
	<div id="dialog-selectGenome" class="dialog-box">
		<div class="dialog-content">
			<!--         	<label for="popupform">Enter text:</label> 
			<input id="popupform" type="text" /> -->
			<label for="genomeSelector">Select Genome:</label> <SELECT
				id="genomeSelector" name="genome">
				<c:forEach items="${genomeList}" var="line">
				<option value="<c:out value='${line}' />">
				<c:out value='${line}' /></OPTION>
				
				</c:forEach>
			</SELECT>
			<div id="dialog-message"></div>
			<div>
				<a href="#" class="button" onclick='selectGenome()'>Submit</a>
			</div>
		</div>
	</div>
	<div id="dialog-queryForBed" class="dialog-box">
		<div class="dialog-content">
			<label for="position">Range [Chromosome:Start-End]</label> 
			<input id="position" type="text" />
			<div id="dialog-message"></div>
			<div>
				<a href="#" class="button" onclick='queryForBedHtml()'>Submit</a>
			</div>
		</div>
	</div>
	<div id="dialog-queryForGraph" class="dialog-box">
		<div class="dialog-content">
			<label for="position">Range [Chromosome:Start-End]</label> 
			<input id="position" type="text" />
			<div id="dialog-message"></div>
			<div>
				<a href="#" class="button" onclick='queryForGraphHtml()'>Submit</a>
			</div>
		</div>
	</div>
</BODY>
</HTML>
