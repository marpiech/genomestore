<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>

	<head>
		<title>seqinterpreter</title>
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js" type="text/javascript"></script>
		<script src="http://code.jquery.com/ui/1.8.23/jquery-ui.min.js" type="text/javascript"></script>
		
		<link rel="shortcut icon" href="css/images/seqinterpreter-icon.png">
		
		<link rel="stylesheet" href="css/cremag-org.css" type="text/css" media="all">
		<link rel="stylesheet" href="css/genome-store.css" type="text/css" media="all">
		<link rel="stylesheet" href="css/genome-store-button.css" type="text/css" media="all">
		<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css" type="text/css" media="all">
		
		<script type="text/JavaScript" src="js/genome-store.js"></script>
		
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-1338631-5', 'cremag.org');
			ga('send', 'pageview');

		</script>
		
	</head>
	<body>
		
		<script type="text/javascript">
		
		var genome = "mm9";
		var referenceId;
		
		function onGenomeChange ($source, $target_id) {
            $($target_id + " dd ul li a").click(function() {
            	
            	var $val = $(this).find("span.value").html();

            	if($val == "mm9")
            		$("#database").val("mm9");
            	if($val == "mm10")
            		$("#database").val("mm9");
            	if($val == "hg19")
            		$("#database").val("hg19");
            	
            	setOptions();
            	
            	console.log("Clicked dropdown " + $target_id);
                var text = $(this).html();
                $($target_id + " dt a").html(text);
                $($target_id + " dd ul").hide();
                $source.val($(this).find("span.value").html());
            });
		}
		
		function populateBackground() {
			console.log("Populating background");
			$.getJSON("getbackgrounds.htm?" + 
					"genome=" + $( "#database").val(), function(result) {
				$("#background").replaceWith("<select id=\"background\"></select>");
			    var options = $("#background");
			    $.each(result, function(item) {
			        options.append($("<option />").val(this.id).text(this.value));				    
			    });
			    $("#background option:first").attr('selected','selected');
			    createDropDown($("#background"), $("#background-dropdown"), "#background-target");
			    setReference(getBackground());
			});
		}
		
		function progress() {
			//console.log("PROGRESS---");
			$.post("progress.htm", {}, function(result) {
				var prog = parseInt(result);
				console.log(prog + "% done");
				endWait();
				startWait("Computing: " + prog + "% done");
				if (prog < 100)
					setTimeout(function() {progress()}, 500);
				else {
					endWait();
					populateQuerySets();
				}
			}, "text").error(function() { alert("Error"); });
		}
		
		function addBedSet() {
            startWait("Computing...");
			$.post("add-queryset.htm", {"genome" : getGenome(), "database" : getDatabase(), "extend" : getExtend(), "query": getQuery(), "background" : getBackground()}, function(result) {
				console.log("ADD BEDSET SUCCESS: " + result);
				if(result != "FALSE") {}
					//populateQuerySets();
			}, "text");//.error(function() { alert("Error"); });
			setTimeout(function() {progress()}, 500);
		}
		
		function populateStats($id) {
			startWait("Calculating statistics");
			$.post("calculate-stat.htm", {"database" : getDatabase(), "query": $id, "background" : referenceId}, function(result) {
				
				$("#statsHolder").html("<table id=\"statsTable\" class=\"gs-table\"></table>");
			    var options = $("#statsTable");
			    options.append("<tr>" +
			    	"<th width=150><b>Track name</b></th>" +
			    	"<th width=95><b>Query</b></th>" +
			    	"<th width=95><b>Background</b></th>" +
			    	"<th width=95><b>Fold diff</b></th>" +
			    	"<th width=95><b>P value</b></th>" +
			    	"<th width=95><b>Bonferroni</b></th>" +
			    	"<th width=95><b>Stack plot</b></th>" +
			    	"<th width=95><b>Histogram</b></th>" +
			    	"<th width=105><b>Genes</b></th>" +
			    	"<th width=95><b>Description</b></th>" + "</tr>");
			    
			    $.each(result, function(item) {

			    	var row = new Array(), j = -1;

			    	row[++j] = "<td>" + this.name + "</td>";
			    	row[++j] = "<td>" + this.querymean + "</td>";
			    	row[++j] = "<td>" + this.backgroundmean + "</td>";
			    	row[++j] = "<td>" + this.foldchange + "</td>";
			    	row[++j] = "<td>" + this.pvalue + "</td>";
			    	row[++j] = "<td>" + this.bonferroni + "</td>";
			    	
			    	row[++j] = "<td><button class=stack style=\"padding:3px; height:40px;\">Stack plot</button></td>";
			    	row[++j] = "<td><button class=histogram style=\"padding:3px; height:40px;\">Histogram</button></td>";
			    	row[++j] = "<td><button class=genes style=\"padding:3px; height:40px;\">Show genes</button></td>";
			    	row[++j] = "<td><button class=desc style=\"padding:3px; height:40px;\">Description</button></td>";
			    				    	
			        options.append($("<tr id=\"" + this.id + "\"/>").html(row.join('')));
			        
			    });
			    
			    $('#csv').show();
			    				
			    var link = "calculate-stat-csv.htm" + 
				"?database=" + getDatabase() + 
				"&query=" + $id + 
				"&background=" + referenceId + 
				"";
			    $("#csv").html(
				    	"<span class=\"label\">Download Results</span>"
				);
			    
			    $("#csv").click(function() {window.open(link)});
			    
			    $('.desc').click(function() {
			    	 var track = $(this).parent().parent();
			    	 showDescription($(track).attr("id"));
			    });
			    
			    $('.stack').click(function() {
			    	 var track = $(this).parent().parent();
			    	 showStackPlot($(track).attr("id"));
			    });

			    $('.histogram').click(function() {
			    	 var track = $(this).parent().parent();
			    	 showHistogram($(track).attr("id"));
			    });

			    $('.genes').click(function() {
			    	 var track = $(this).parent().parent();
			    	 showGenes($id, $(track).attr("id"));
			    });
			    
			    $("#statsHolder").show();
			    endWait();
			}, "json");
		}

		function showDescription($trackid) {
			console.log("Track log: " + $trackid);
			populateDescription($trackid);
		}
		
		function showStackPlot($trackID) {
			var $height = 300;
			var $width = 100 + $('.showgenes').length * 100;
			
			$("#description-box").html("<embed src=\"get-stackplot.htm" + 
					"?trackID=" + $trackID + 
					"&backgroundID=" + getBackground() +
					"&height=" + $height + 
					"&width=" + $width +
					"&databaseID=" + getDatabase() +
					"\" type=\"image/svg+xml\" height=\""+($height + 50) +"\" width = \""+($width + 50)+"\"/>");
			
			$("#description-box").append('<br/>' +
				'<button id="closepopup" class="greenbtn" style="width: 120px;"><span class="label">Close</span></button>' +
				'<button id="download" class="greenbtn" style="width: 150px; margin-left: 10px;"><span class="label">Download SVG</span></button>' +
				'<br/><br/>' +
				'<span width=600>Stack plot presents distribution of coverages of chosen track in every query set. <br/>The more red the higher percentage of transcripts with high coverage on its promoter.<br/>' + 
				'Above each box query-set id is displayed. Below each plot P value of differnce to <br/>reference id displayed.</span>');
			    
			$('#closepopup').click(function () {$('#dialog-overlay, #dialog-box, #description-box').hide(); return false; });
			
			var link = "\"get-stackplot.htm" + 
			"?trackID=" + $trackID + 
			"&backgroundID=" + getBackground() +
			"&height=" + $height + 
			"&width=" + $width +
			"&databaseID=" + getDatabase() +
			"&download=true" +
			"\"";
	
	$("#download").html("<a  href=" + link + " >" +
	    	"<span class=\"label\">Download SVG</span>" + 
		    "</a>");
			
		    popup('description-box');

		}
		
		function rename ($id) {
  
			console.log("RENAME log: " + $id);
			
			$("#description-box").html("Old name: " + $id + "<BR /><BR /> New name: <input id=\"newname\" type=\"text\" name=\"newname\"><br />");

			$("#description-box").append(
					'<br/><button id="closepopup" class="greenbtn" style="width: 120px;"><span class="label">Cancel</span></button>' +
					'<button id="rename" class="greenbtn" style="width: 120px; margin-left: 10px;"><span class="label">Rename</span></button>' +
					'<br/>');
			
			var options = $("#description-table");

			$('#closepopup').click(function () {$('#dialog-overlay, #dialog-box, #description-box').hide(); return false; });

			
			$('#rename').click(function() {
		    	 //var row = $(this).parent().parent();
		    	 //var $range = row.find(".Interval").html();
		    	 //window.open("/seqinspector.htm?range=" + $range + "&track=" + $track + "&genome=" + getDatabase());
		    	 		    	 console.log("Renaming " + $id + " to " + $('#newname').val());
		    	$.get("rename-queryset.htm", {"queryid" : $id, "newname" : $('#newname').val()}, function(result) {
			    populateQuerySets();
				});

		    	 $('#dialog-overlay, #dialog-box, #description-box').hide(); return false;
		    	 //refresh();
		    });
			
			popup('description-box');			
		}
		
		function showHistogram($trackID) {
			var $height = 300;
			var $width = 600;
			
			$("#description-box").html("<embed src=\"get-histogram.htm" + 
					"?trackID=" + $trackID + 
					"&backgroundID=" + getBackground() +
					"&height=" + $height + 
					"&width=" + $width +
					"&databaseID=" + getDatabase() +
					"\" type=\"image/svg+xml\" height=\""+($height + 50) +"\" width = \""+($width + 50)+"\"/>");
			
			$("#description-box").append('<br/>' +
				'<button id="closepopup" class="greenbtn" style="width: 120px;"><span class="label">Close</span></button>' +
				'<button id="download" class="greenbtn" style="width: 150px; margin-left: 10px;"><span class="label">Download SVG</span></button>' +
				'<br/><br/>');
			    
			$('#closepopup').click(function () {$('#dialog-overlay, #dialog-box, #description-box').hide(); return false; });
			
			var link = "\"get-histogram.htm" + 
					"?trackID=" + $trackID + 
					"&backgroundID=" + getBackground() +
					"&height=" + $height + 
					"&width=" + $width +
					"&databaseID=" + getDatabase() +
					"&download=true" +
					"\"";
			
			$("#download").html("<a  href=" + link + " >" +
			    	"<span class=\"label\">Download SVG</span>" + 
				    "</a>");
					
		    popup('description-box');

		}
		
		function populateDescription($trackid) {
			$.getJSON("gettrackdescription.htm?" + 
					"trackId=" + $trackid +
					"&genome=" + $( "#database").val(), function(result) {
				
				$("#description-box").html(
						'<button id="closepopup" class="greenbtn closepop" style="width: 120px;"><span class="label">Close</span></button>' +
						'<br/><br/><br/>');
				
				$("#description-box").append("<table id=\"description-table\" class=\"gs-table\"></table>");


				
				var options = $("#description-table");
			    
				$.each(result, function(item) {
			    	var row = new Array(), j = -1;
			    	row[++j] = "<td width=160>" + this.key + "</td>";
			    	row[++j] = "<td width=160>" + this.value + "</td>";
			    	options.append($("<tr>").html(row.join('')));
			    });
			    
				$("#description-box").append('<br/>' +
				'<button id="closepopup" class="greenbtn closepop" style="width: 120px;"><span class="label">Close</span></button>' +
				'<br/><br/>');
			    
				$('.closepop').click(function () {$('#dialog-overlay, #dialog-box, #description-box').hide(); return false; });
			    popup('description-box');
			});
		}			
		
		function popup($box) {
		    // get the screen height and width  
		    var maskHeight = $(document).height();  
		    var maskWidth = $(window).width();
		     
		    // calculate the values for center alignment
		    var dialogTop = 75 + $(window).scrollTop();
		    var dialogLeft = $(window).width() / 4; 
		     
		    // assign values to the overlay and dialog box
		    $('#dialog-overlay').css({height:maskHeight, width:maskWidth}).show();
		    //$('#' + $box).css({'position':'fixed', 'top':'10%', 'left':'25%'}).show();
		    $('#' + $box).css({top:dialogTop, left:dialogLeft}).show();
		    
		    console.log("popup: " + $box);
         
		}

		/*
		* These functions are dealing with gene sets management
		*/
		
		function refresh() {
			setTimeout(function() {populateQuerySets();}, 10);
		}
		
		function populateQuerySets() {
			$.post("get-querysets.htm", {}, function(result) {
				var i = 1;
				$("#backgrounds").html("");
			    $.each(result, function(item) {
			    	addSetRow(this.id);
			    	if (i == 1) setBackground(this.id);
					i = 0;		        
			    });
				$('.showgenes').click(function() {
			    	 var row = $(this).parent();
			    	 showGenes($(row).attr("id"), null);
			    });
			}, "json");
		}
		
		function addSetRow($id) {
			var holder = $("#backgrounds");
			holder.append('<div style="clear: left;padding: 5px;" id="' + $id + '">' + 
				'<button class="buttonset buttonset-left buttonset-inactive" style="width: 120px;">' + $id + "</button>" +
				'<button class="stats buttonset buttonset-active">statistics</button>' +
				'<button class="remove buttonset buttonset-active">remove</button>' +
				'<button class="setreference buttonset buttonset-active">set as reference</button>' +
				'<button class="showgenes buttonset buttonset-active">showgenes</button>' +
				'<button class="rename buttonset buttonset-right buttonset-active">rename</button>' +
				'</div>');
			
			$('.stats').click(function() {
		    	 var row = $(this).parent();
		    	 populateStats($(row).attr("id"));
		    });
			$('.setreference').click(function() {
		    	 var row = $(this).parent();
		    	 setReference($(row).attr("id"));
		    });
			$('.remove').click(function() {
		    	 var row = $(this).parent();
		    	 removeQuerySet($(row).attr("id"));
		    });
			$('.rename').click(function() {
		    	 var row = $(this).parent();
		    	 rename($(row).attr("id"));
		    });
			/*$('.showgenes').click(function() {
				 console.log("SetRow show");
		    	 var row = $(this).parent();
		    	 showGenes($(row).attr("id"), null);
		    });*/
		}
		
		function showGenes($id, $track) {
			
			$.getJSON("show-genes.htm", {"queryid" : $id, "trackID" : $track, "genome" : getDatabase()} , function(result) {
				
				$("#description-box").html(
						'<button id="closepopup" class="greenbtn" style="width: 120px;"><span class="label">Close</span></button>' +
						'<br/><br/><br/>');
					    
				$('#closepopup').click(function () {$('#dialog-overlay, #dialog-box, #description-box').hide(); return false; });
				
				$("#description-box").append("<table id=\"description-table\" class=\"gs-table\"></table>");

				var options = $("#description-table");
			    
				if (!!$track) {
					options.append("<tr>" +
				    	"<th width=160><b>Gene Symbol</b></th>" +
				    	"<th width=250>Interval</th>" +
				    	"<th width=80><b>Maximal peak</b></th>" +
				    	"<th width=80><b>Average coverage</b></th>" +
				    	"<th width=110><b>Visualize</b></th>");
				} else {
					options.append("<tr>" +
					    	"<th width=160><b>Gene Symbol</b></th>" +
					    	"<th width=250>Interval</th>" +
					    	"<th width=110><b>Visualize</b></th>");
				}
				
				$.each(result, function(item) {
			    	var row = new Array(), j = -1;
			    	row[++j] = "<td>" + this.Name + "</td>";
			    	row[++j] = "<td class=Interval>" + this.Interval + "</td>";
			    	if (!!$track) row[++j] = "<td>" + this.Max + "</td>";
			    	if (!!$track) row[++j] = "<td>" + this.Avg + "</td>";
			    	row[++j] = "<td><button class=inspect style=\"padding:3px; height:40px;\">Show interval</button></td>";
			    	
			    	options.append($("<tr>").html(row.join('')));
			    });
			    
				$('.inspect').click(function() {
			    	 var row = $(this).parent().parent();
			    	 var $range = row.find(".Interval").html();
			    	 window.open("/seqinspector.htm?range=" + $range + "&track=" + $track + "&genome=" + getDatabase());
			    	 //console.log(row.find(".Interval").html());
			    });
				
				popup('description-box');
				
			});
			
			/*$.get("show-genes.htm", {"queryid" : $id}, function(result) {
				$("#statsHolder").html("<div id=\"genes\"></div>");
			    var options = $("#genes");
			    options.append(result);
			    $("#statsHolder").show();
			});*/
		}
		
		function setBackground($id) {
			console.log("setBackground: " + $id);
			var holder = $("#" + $id);
			console.log(holder.prop("tagName"));
			holder.html('<button class="buttonset buttonset-left buttonset-inactive" style="width: 120px;">' + $id + "</button>" +
					'<button class="buttonset buttonset-inactive">statistics</button>' +
					'<button class="buttonset buttonset-inactive">remove</button>' +
					'<button class="buttonset buttonset-inactive">set as reference</button>' +
					'<button class="showgenes buttonset buttonset-active">showgenes</button>' +
					'<button class="buttonset buttonset-right buttonset-inactive">rename</button>'
					); 
			/*$('.showgenes').click(function() {
				 console.log("Background show");
		    	 var row = $(this).parent();
		    	 showGenes($(row).attr("id"), null);
		    });*/
		} 
		
		function setReference($id) {
			referenceId = $id;
		    $.get("set-reference.htm", {"database" : getDatabase(), "background" : $id}, function(result) {
				console.log("ADD BACKGROUND SUCCES");
			    populateQuerySets();
			});
		}
		
		function removeQuerySet($id) {
			$.get("remove-queryset.htm", {"queryid" : $id}, function(result) {
			    populateQuerySets();
			});
		}
		
		function setOptions() {
			startWait("Recalculating...");
			$.get("set-options.htm", {"database" : getDatabase(), "filters" : getFilters()}, function(result) {
				console.log("SET OPTIONS SUCCESS");
				endWait();
				populateBackground();
			});
			setTimeout(function() {progress()}, 500);
		}
		
		function getFilters() {
			var filstr = "";
			var first = 1;
			var filters = $( ".filter:checked" );
			for (var i = 0 ;i < filters.length; i++) {
				if (first == 0) {filstr = filstr + "," + filters[i].value;}
				if (first == 1) {filstr = filstr + filters[i].value; first = 0;}
			}
			return filstr;
		}

		function getExtend() {
			return $( "#extendBy").val();
		}
		function getGenome() {
			return $( "#genome").val();
		}
		function getBackground() {
			return $( "#background").val();
		}
		function getDatabase() {
			return $( "#database").val();
		}
		function getQuery() {
			return $( "#query").val();
		}
		
		$(document).keyup(function(e) {
			if (e.keyCode == 27) { $('#dialog-overlay, #description-box').hide(); }   // esc
		});
		
		function parseGET() {
			if(!!getURLParameter("query")) {
            	$query = getURLParameter("query");
        		$("#query").val($query);
            }
			if(!!getURLParameter("genome")) {
            	$genome = getURLParameter("genome");
            	$("#genome").val($genome);
            	$("#database").val($genome);
            }
			if(!!getURLParameter("query")) {
				addBedSet();
			}
		}
		
		$(document).ready(function() {
				
			createDropDown($("#genome"), $("#genome-dropdown"), "#genome-target");
			onGenomeChange($("#genome"), "#genome-target");
			populateBackground();
			createDropDown($("#database"), $("#database-dropdown"), "#database-target");
			
			$('#closepopup').click(function () {$('#dialog-overlay, #dialog-box').hide(); setOptions(); return false; });
			$(window).resize(function () {if (!$('#dialog-box').is(':hidden')) popup('dialog-box'); });
            $(window).resize(function () {if (!$('#description-box').is(':hidden')) popup('description-box'); });
            
            $('#options').click(function () {popup('dialog-box'); });
            $('#example').click(function () {$("#query").val("chr10:66999617-67001617\nchr18:35019861-35021861\nchr14:70476252-70478252\nchr6:85462583-85464583");});
            $("#add").click(function() {addBedSet(); });
            $("#help").click(function() {window.open("http://seqinspector.readthedocs.org/en/latest/")});
            parseGET();
            $("#csv").hide();
		});
		
		</script>
		
		<header class="gs-header cremag-a">
			<img src="css/images/seqinspectorlogo.png">
			<b>seq</b>inspector
			<a class="cremag-a" href="http://www.cremag.org" style="margin-left: 200px; padding-right: 10px; border-right: 1px solid #AAA;">Home</a>
			<a class="cremag-a" href="http://seqone.cremag.org" style="padding-left: 5px; padding-right: 10px; border-right: 1px solid #AAA;">Seqinspector-one</a>
			<a class="cremag-a" href="http://seqinspector.readthedocs.org/en/latest/" style="padding-left: 5px; padding-right: 10px; border-right: 1px solid #AAA;">Help</a>
<!-- 			<a class="cremag-a" href="contact.htm" style="padding-left: 5px; padding-right: 10px; border-right: 1px solid #AAA;">Contact</a>-->
<!-- 			<a class="cremag-a" href="news.htm" style="padding-left: 5px;">News</a>-->
		</header>
		
		<table>
			<td id="column-left">
		<div id="genome-dropdown" style="float: left; padding: 0px;"></div>
		<div style="float: left;"><button id="add" class="greenbtn gs-in-table" style="width: 90px; padding:10 20 6 20px;"><span class="label gs-centerize">Submit</span></button></div>
		
		
		
		<div style="clear: left; float: left;">
			<textarea id="query" cols="40" rows="10" name="bed-items" 
				placeholder="
Input your query in one of the following format:                                 
gene symbols, ensembl transcript IDs, refseq mRNA IDs, .bed formatted lines or USCS genomic ranges"></textarea>
<!-- chr10:66999617-67001617
chr18:35019861-35021861
chr14:70476252-70478252
chr6:85462583-85464583 -->
		</div>

			<td id="column-set" style="vertical-align: top;">
				<div id="backgrounds" style="float: left;"></div>
			</td>
		</table>

		<div id="buttons">
			<button id="options" class="bluebtn gs-in-table" style="float: left"><span class="label">Options</span></button>
			<button id="example" class="bluebtn gs-in-table" style="float: left"><span class="label">Example</span></button>
			<button id="help" class="bluebtn gs-in-table" style="float: left"><span class="label">Help</span></button>
			<button id="csv" class="bluebtn gs-in-table" style="float: left"><span class="label">Download CSV</span></button>
		</div>
		
		<div id="statsHolder" style="margin:3 0 5 8px; display:none; clear: left; float: left;"></div>
		
		<div id="dialog-overlay" class="dialog-overlay"></div>
		<div id="description-box" class="dialog-box"></div>
		<div id="dialog-box" class="dialog-box" style="width:300px !important;">
			<div class="dialog-content">
				<div id="dialog-message" class="ui-widget">
					<div class="gs-in-table">Database:</div> 
					<!-- <div id="database-dropdown"></div> -->
					<div><select id="database">
						<option selected="selected" value="mm9">mm9</option>
						<option value="hg19">hg19</option>
					</select></div>
					<div style="height:4px;"></div>
					<!-- <div class="gs-in-table">Reference:</div>
					<div id="background-dropdown"></div> -->
					<div style="height:4px;"></div>
					<div class="gs-in-table"><input type="checkbox" id="extendBed" value="extend" />Extend bed ranges</div>
					<div style="height:4px;"></div>
					<div class="gs-in-table">Extend by (bp):</div>
					<div><input id="extendBy" class="gs-input-text gs-in-table" placeholder="extend by (bp)" value="1000" /></div>
					<div style="height:4px;"></div>
					<div class="gs-in-table">Filters:</div>
					<div class="gs-in-table"><input type="checkbox" class="filter" id="transcription" value="transcription" checked="yes"/>Transcription Factors</div>
					<div class="gs-in-table"><input type="checkbox" class="filter" id="histone" value="histone" />Histones</div>
					<div class="gs-in-table"><input type="checkbox" class="filter" id="polymerase" value="polymerase" />Polymerases</div>
					<div class="gs-in-table"><input type="checkbox" class="filter" id="dnase" value="dnase" />Dnases</div>
					<div class="gs-in-table"><input type="checkbox" class="filter" id="feature" value="feature" />Features</div>
					<div class="gs-in-table"><input type="checkbox" class="filter" id="input" value="input" />Controls</div>
					<div class="gs-in-table"><input type="checkbox" class="filter" id="other" value="other" />Others</div>
				</div>
				<br/>
				<button id="closepopup" class="greenbtn closeoptions" style="width: 120px;"><span class="label">Close</span></button>
				<br/><br/>
			</div>
		</div>
		
		<div id="hidden-elements" class="hidden">
			<select id="genome">
 				<option selected="selected" value="mm9">Mus Musculus (mm9)</option>
				<option value="mm10">Mus musculus (mm10)</option>
				<option value="hg19">Homo sapiens (hg19)</option>
			</select>
			<select id="background"></select>

		</div>
		
	</body>
	
	
	
</html>
