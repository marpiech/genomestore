<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>

	<head>
	
		<title>cremag.org - NEWS</title>
		
		<link rel="shortcut icon" href="css/images/cremag-small.png">
		
		<link rel="stylesheet" href="css/cremag-org.css" type="text/css" media="all">
		<link rel="stylesheet" href="css/genome-store.css" type="text/css" media="all">
		
	</head>
	
	<body class="cremag" style="">
	
		<header class="gs-header cremag-a">
			<img src="css/images/cremag-logo-black.png">
			<b>cremag</b>.org
			<a class="cremag-a" href="http://www.cremag.org" style="margin-left: 200px; padding-right: 10px; border-right: 1px solid #AAA;">Home</a>
			<a class="cremag-a" href="http://seqinspector.cremag.org" style="padding-left: 5px; padding-right: 10px; border-right: 1px solid #AAA;">Seqinspector</a>
			<a class="cremag-a" href="help.htm" style="padding-left: 5px; padding-right: 10px; border-right: 1px solid #AAA;">Help</a>
			<a class="cremag-a" href="contact.htm" style="padding-left: 5px; padding-right: 10px; border-right: 1px solid #AAA;">Contact</a>
			<a class="cremag-a" href="news.htm" style="padding-left: 5px;">News</a>
		</header>	
	
		<table>
			<tr style="height: 200px;">
				<td class="cremag-info">
					20-04-2013
					<h1>Meet us at The Biology Of Genomes May 7 - 11, 2013</h1>
					<a href="http://meetings.cshl.edu/meetings/2013/genome13.shtml">http://meetings.cshl.edu</a>
				</td>
			</tr>
			<tr style="height: 200px;">				
				<td class="cremag-info">
					17-04-2013
					<h1>Seqinspector released for beta testing!</h1>
					<a href="http://seqinspector.cremag.org">http://seqinspector.cremag.org</a>
				</td>

			</tr>
		</table>

<%@ include file="/staticContent/footer.html" %>

	</body>

</html>
