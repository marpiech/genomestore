<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>

	<head>
		<title>cremag.org</title>
		
		<link rel="stylesheet" href="css/cremag-org.css" type="text/css" media="all">
		<link rel="stylesheet" href="css/genome-store.css" type="text/css" media="all">
		<link rel="stylesheet" href="css/genome-store-button.css" type="text/css" media="all">

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-1338631-6', 'cremag.org');
			ga('send', 'pageview');

		</script>
		
	</head>
	<body class="cremag" style="">
		<div style="height:100px;"></div>
		<table>
			<tr style="height: 200px;">
				<td class="cremag-logo">
					<h1>cremag.org</h1>
					<h2>easy transcription regulation analysis</h2>
				</td>
				<td class="cremag-info">
					<h1>Seqinspector released for beta testing!</h1>
					<a href="http://seqinspector.cremag.org/">http://seqinspector.cremag.org</a>
				</td>
				
			</tr>	
		</table>
		<table style="padding-left:100px; padding-top:50px; border-spacing:30px;">
			<tr>
				<td class="cremag-button-frame">
					<a href="http://seqone.cremag.org" class="cremag-button" style="">
						<h1><span style="color:#82c554; font-weight: bold;">seq</span>inspector-one</h1>
						<h2>inspect gene promoters using available ChIP-seq data</h2>
					</a>
				</td>
				<td class="cremag-button-frame">
					<a href="http://seqinspector.cremag.org" class="cremag-button" style="">
						<h1><span style="color:#82c554; font-weight: bold;">seq</span>inspector</h1>
						<h2>interpret genomic intervals using available ChIP-seq data</h2>
					</a>
				</td>
				
				<td class="cremag-button-frame">
					<a href="http://cis.cremag.org" class="cremag-button" style="">
						<h1><span style="color:#82c554;; font-weight: bold;">cre</span>mag</h1>
						<h2>inspect gene promoters with computationally predicted TFBSs</h2>
					</a>
				</td>
			</tr>
		</table>
		
		
		
		<%@ include file="/staticContent/footer.html" %>
	</body>
	

	
</html>
