<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>

	<head>
	
		<title>seQinspector</title>
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js" type="text/javascript"></script>
		<script src="http://code.jquery.com/ui/1.8.23/jquery-ui.min.js" type="text/javascript"></script>
		
		<link rel="shortcut icon" href="css/images/seqinspector-icon.png">
		
		<link rel="stylesheet" href="css/cremag-org.css" type="text/css" media="all">
		<link rel="stylesheet" href="css/genome-store.css" type="text/css" media="all">
		<link rel="stylesheet" href="css/genome-store-button.css" type="text/css" media="all">
		<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css" type="text/css" media="all">
		
		<script type="text/JavaScript" src="js/genome-store.js"></script>

	</head>
	<body>
		
		<script type="text/javascript">
			
			/* Genomic range functions */
		
			var $chromosome = "chr1";
			var $start = 100000;
			var $end = 100000;
			var $upstream = 1000;
			var $downstream = 1000;
			var $position = 100000;
			
			function setUpstream(n) {
				$upstream = Math.round(n);
			}
			
			function setDownstream(n) {
				$downstream = Math.round(n);
			}
			
			function calculateStartEnd() {
				$start = $position - $upstream;
				$end = $position + $downstream;				
			}
			
			function calculateLength() {
				calculateStartEnd;
				return $end - $start;
			}
			
			function populateRange() {
				calculateStartEnd();
				$("#range").val($chromosome + ":" + $start + "-" + $end);
			}
			
			function populatePosition() {
				var $tss = $("#tss").val();
				$chromosome = $tss.split(":")[0];
				$position = parseInt($tss.split(":")[1]);
				populateRange();
			}
			
			function left() {
				$len = calculateLength() / 4;
				$position = $position - $len;
				populateRange();
			}
			
			function right() {
				$len = calculateLength() / 4;
				$position = $position + $len;
				populateRange();
			}
			
			function zoomin() {
				setUpstream($upstream / 2);
				setDownstream($downstream / 2);
				populateRange();
			}
			
			function zoomout() {
				setUpstream($upstream * 2);
				setDownstream($downstream * 2);
				populateRange();
			}
			
			function populateTss() {
				startWait("Parsing query"); 
				$.getJSON("gettss.htm?symbol=" + $( "#symbol").val() + 
						"&genome=" + $( "#genome").val(), function(result) {
					
					/* removes old tss selector */
					$("#tss").replaceWith("<select id=\"tss\"></select>");
					
					/* creates new selector */
				    var options = $("#tss");
				    var first = true;
				    
				    $.each(result, function(item) {
				    	if(first) {
			    			/* the first field is selected by default */
			        		options.append($("<option selected/>").val(this.id).text(this.value));
			        		first = false;
							if(!!this.extend) {
								setUpstream(parseInt(this.extend));
								setDownstream(parseInt(this.extend));
							}
				    	} else {
				    		options.append($("<option />").val(this.id).text(this.value));
				    	}
				    });

					$("#tss").change(function () {
						populatePosition();
					});
					
					populatePosition();
			
					var len = result.length;
				    if(len > 1) {
						$("#tss-info").html("<button id=\"tss-alert\" class=\"redbtn gs-in-table\"><span class=\"label\">" + len + " alternative start sites</span></button>");
				    	createDropDown($("#tss"), $("#tss-dropdown"), "#tss-target");
				    } else {
				    	$("#tss-info").html("");
				    	$("#tss-dropdown").html("");
				    }
				    $('#wait-tss').hide();
				    endWait();
				    populateStats();
				}).error(function() { alert("Not valid ID"); endWait();});				
			}

			function populateBackground() {
				$("#log").val("Populating background");
				$.getJSON("getbackgrounds.htm?" + 
						"genome=" + $( "#genome").val(), function(result) {
					$("#background").replaceWith("<select id=\"background\"></select>");
				    var options = $("#background");
				    $.each(result, function(item) {
				        options.append($("<option />").val(this.id).text(this.value));				    
				    });
				    
		            // show range specified in range parameter
		            if(!!getURLParameter("range")) {
		            	range = getURLParameter("range");
		            	chromosome = range.split(":")[0];
		            	start = parseInt(range.split(":")[1].split("-")[0]);
		            	end = parseInt(range.split(":")[1].split("-")[1]);
		            	$chromosome = chromosome;
		            	$position = Math.round((start + end) / 2);
		            	$upstream = Math.round((end - start) / 2);
		            	$downstream = Math.round((end - start) / 2);
		            	populateRange();
		            	populateStats();
		            }
				    
				});
			}

			function populateStats() {
				$('#wait-stat').show();
				console.log("getscoresforbed.htm?" + 
						"genome=" + $( "#genome").val() +
						"&database=" + $( "#database").val() +
						"&range=" + $( "#range").val() +
						"&background=" + $( "#background").val() +
						"&filters=" + getFilters());
				$.getJSON("getscoresforbed.htm?" + 
						"genome=" + $( "#genome").val() +
						"&database=" + $( "#database").val() +
						"&range=" + $( "#range").val() +
						"&background=" + $( "#background").val() +
						"&filters=" + getFilters(),
						function(result) {
					$("#statsHolder").html("<table id=\"statsTable\" class=\"gs-table\"></table>");
				    var options = $("#statsTable");
				    options.append("<tr><th width=30></th>" +
				    	"<th width=150><b>Track name</b></th>" +
				    	"<th width=95><b>Query</b></th>" +
				    	"<th width=95><b>Background</b></th>" +
				    	"<th width=95><b>Fold diff</b></th>" +
				    	"<th width=95><b>P value</b></th>" +
				    	"<th width=95><b>Bonferroni</b></th>" +
				    	"<th width=95><b>Description</b></th>" + "</tr>");
				    var i = 0, sig = 0, tend = 0;
				    var checked = "checked";
				    $.each(result, function(item) {
				    	var row = new Array(), j = -1;
				    	if (i++ >= 10) {checked = "";}
				    	row[++j] = "<td class=\"cb gs-table\"><input type=\"checkbox\" value=\"yes\" "+checked+"></td>";
				    	if(parseFloat(this.bonferroni) < 0.05) {sig++;}
				    	else if (parseFloat(this.pvalue) < 0.05) {tend++;}
				    	
				    	row[++j] = "<td>" + this.name + "</td>";
				    	row[++j] = "<td>" + this.querymean + "</td>";
				    	row[++j] = "<td>" + this.backgroundmean + "</td>";
				    	row[++j] = "<td>" + this.foldchange + "</td>";
				    	row[++j] = "<td>" + this.pvalue + "</td>";
				    	row[++j] = "<td>" + this.bonferroni + "</td>";
				    	var $descriptionCell = this.id + "cell"; 
				    	row[++j] = "<td><button class=desc style=\"padding:3px; height:40px;\">Description</button></td>";
				    	
				        options.append($("<tr id=\"" + this.id + "\"/>").html(row.join('')));

				    });
				    
				    $('.desc').click(function() {
				    	 var trRef=$(this).parent().parent();
				    	 showDescription($(trRef).attr("id"));
				    });
				    
				    $('#info-sig').html("<span class=\"label\">" + sig + " significant</span>");
				    $('#info-tend').html("<span class=\"label\">" + tend + " with tendency</span>");
				    $('#wait-stat').hide();
					getImage();
					showStatButtons();
					$('#show-stat').show();
					$('#info-sig').show();
					$('#info-tend').show();
					$('#info-show').show();
					//$('#spanner').hide();
				});
			}			

			function populateDescription($trackid) {
				$.getJSON("gettrackdescription.htm?" + 
						"trackId=" + $trackid +
						"&genome=" + $( "#database").val(), function(result) {
					
					$("#description-box").html(
							'<button id="closepopup" class="greenbtn" style="width: 120px;"><span class="label">Close</span></button>' +
							'<br/><br/><br/>');
					
					$("#description-box").append("<table id=\"description-table\" class=\"gs-table\"></table>");

					var options = $("#description-table");
				    
					$.each(result, function(item) {
				    	var row = new Array(), j = -1;
				    	row[++j] = "<td width=160>" + this.key + "</td>";
				    	row[++j] = "<td width=160>" + this.value + "</td>";
				    	options.append($("<tr>").html(row.join('')));
				    });
				    
					$("#description-box").append('<br/>' +
					'<button id="closepopup" class="greenbtn" style="width: 120px;"><span class="label">Close</span></button>' +
					'<br/><br/>');
				    
					$('#closepopup').click(function () {$('#dialog-overlay, #dialog-box, #description-box').hide(); return false; });
				    popup('description-box');
				});
			}	
			
			/*function populateDescription($trackid) {
				$.getJSON("gettrackdescription.htm?" + 
						"trackId=" + $trackid +
						"&genome=" + $( "#database").val(), function(result) {
					$("#description-box").html("<table id=\"description-table\" class=\"gs-table\"></table>");
				    var options = $("#description-table");
				    //options.append("<tr><th width=120>Feature</th>" +
				    //	"<th width=120><b>Track name</b></th>" + "</tr>");
				    $.each(result, function(item) {
				    	var row = new Array(), j = -1;
				    	row[++j] = "<td width=160>" + this.key + "</td>";
				    	row[++j] = "<td width=160>" + this.value + "</td>";
				    	options.append($("<tr>").html(row.join('')));
				    });
				    $("#description-box").append('<br/>' +
					'<button id="closepopup" class="greenbtn" style="width: 120px;"><span class="label">Close</span></button>' +
					'<br/><br/>');
				    $('#closepopup').click(function () {$('#dialog-overlay, #dialog-box, #description-box').hide(); return false; });
				    popup('description-box');
				});
			}			*/

			
			function showDescription($trackid) {
				console.log("Track log: " + $trackid);
				populateDescription($trackid);
			}
			
			function showStatButtons() {
				$('#info-sig').show();
				$('#info-tend').show();
				$('#info-show').show();
				$('#show-stat').show();
				$('#statsButtons').show();
				$('#statsHolder').show();
			}

			function hideStatButtons() {
				$('#info-sig').hide();
				$('#info-tend').hide();
				$('#info-show').hide();
				$('#show-stat').hide();
				$('#statsButtons').hide();
				$('#statsHolder').hide();
			}
			
			function getImage() {
				var $height = 0;
				$height = 110 + 45 * getNumberOfSelectedTracks() + 20;
				$('#wait-image').show();
				$("#imageHolder").show();
				$("#imageHolder").html("<embed src=\"getrangehist.htm" + 
						"?genome=" + $( "#genome").val() + 
						"&range=" + $( "#range").val() + 
						"&database=" + $( "#database").val() + 
						"&tracks=" + getListOfSelectedTracks() +
						//"\" type=\"image/svg+xml\" height=\""+$height+"\" width = \"800\"/>");
						"\" type=\"image/svg+xml\" width = \"800\"/>");
				$('#wait-image').hide();
				var linkjpg = "\"getrangehistjpeg.htm" + 
				"?genome=" + $( "#genome").val() + 
				"&range=" + $( "#range").val() + 
				"&database=" + $( "#database").val() + 
				"&tracks=" + getListOfSelectedTracks() +
				"&jpg=true" + "\"";
				var link = "\"getrangehist.htm" + 
				"?genome=" + $( "#genome").val() + 
				"&range=" + $( "#range").val() + 
				"&database=" + $( "#database").val() + 
				"&tracks=" + getListOfSelectedTracks() +
				"&jpg=true" + "\"";
				$("#download").html("<a  href=" + linkjpg + " >" +
			    	"<span class=\"label\">Download PNG</span>" + 
			    "</a>");
				$("#downloadsvg").html("<a  href=" + link + " >" +
				    	"<span class=\"label\">Download SVG</span>" + 
				    "</a>");
				var csvlink = "\"getscoresforbedcsv.htm?" + 
						"genome=" + $( "#genome").val() +
						"&database=" + $( "#database").val() +
						"&range=" + $( "#range").val() +
						"&background=" + $( "#background").val() +
						"&filters=" + getFilters() + "\"";
				$("#downloadtab").html("<a  href=" + csvlink + " >" +
				    	"<span class=\"label\">Download Results</span>" + 
				    "</a>");
			}
			
			function getNumberOfSelectedTracks() {
				var $number = 0;
				$('#statsHolder tr').filter(':has(:checkbox:checked)').each(function() {
					$number++;
			    });
				return $number;
			}
			
			function redraw() {
				if($('#imageHolder').is(":visible")) {
					getImage();}
			}
						
			function getListOfSelectedTracks() {
				$("#log").val("Selecting tracks");
				var $tracks = "";
				$('#statsHolder tr').filter(':has(:checkbox:checked)').each(function() {
					$tracks = $tracks + "," + this.id;
			    });				
				$("#log").val($tracks.substr(1));
				return $tracks.substr(1);
			}

			function createDropDown($source, $container, $target_id){
				$container.html("");
				var $target = $target_id;
			    var selected = $source.find("option[selected]");  // get selected <option>
			    var options = $("option", $source);  // get all <option> elements
			    
			    // create <dl> and <dt> with selected value inside it
			    $container.append('<dl id="' + $target.substr(1) + '" class="dropdown"></dl>')

			    $($target).append('<dt><a id="'+selected.val()+'" href="#">' + selected.text() + 
			        '<span class="value">' + selected.val() + 
			        '</span></a></dt>')
			    $($target).append('<dd><ul></ul></dd>')
			    options.each(function(){
			        $($target + " dd ul").append('<li><a href="#">' + 
			            $(this).text() + '<span class="value">' + 
			            $(this).val() + '</span></a></li>');
			    });
			    
			    $($target_id + " dt a").click(function() {
	                $($target_id + " dd ul").toggle();
	            });
			    
	            $(document).bind('click', function(e) {
	                var $clicked = $(e.target);
	                if (! $clicked.parents().hasClass("dropdown"))
	                    $($target_id + " dd ul").hide();
	            });
	            
	            $($target_id + " dd ul li a").click(function() {
	            	var $val = $(this).find("span").html();
	            	if($val == "mm9")
	            		$("#database").val("mm9");
	            	if($val == "mm10")
	            		$("#database").val("mm9");
	            	if($val == "hg19")
	            		$("#database").val("hg19");
	            	console.log("Clicked " + $val);
	                var text = $(this).html();
	                $($target_id + " dt a").html(text);
	                $($target_id + " dd ul").hide();
	                $source.val($(this).find("span.value").html());
	                populatePosition();
	                populateStats();
	            });
			}
			
			function popup($box) {
		         
			    // get the screen height and width  
			    var maskHeight = $(document).height();  
			    var maskWidth = $(window).width();
			     
			    // calculate the values for center alignment
			    var dialogTop =  (maskHeight/3) - ($('#' + $box).height());  
			    var dialogLeft = (maskWidth/2) - ($('#' + $box).width()/2); 
			     
			    // assign values to the overlay and dialog box
			    $('#dialog-overlay').css({height:maskHeight, width:maskWidth}).show();
			    $('#' + $box).css({'position':'fixed', 'top':'10%', 'left':'25%'}).show();
			    //$('#' + $box).css({top:dialogTop, left:dialogLeft}).show();
			    
			    console.log("popup: " + $box);
             
			}

			function getFilters() {
				var filstr = "";
				var first = 1;
				var filters = $( ".filter:checked" );
				for (var i = 0 ;i < filters.length; i++) {
					if (first == 0) {filstr = filstr + "," + filters[i].value;}
					if (first == 1) {filstr = filstr + filters[i].value; first = 0;}
				}
				return filstr;
			}
			
			function parseGET() {
				if(!!getURLParameter("genome")) {
	            	$genome = getURLParameter("genome");
	            	if($genome == "mm9") $text = "Mus musculus (mm9)";
	            	if($genome == "hg19") $text = "Home sapiens (hg19)";
	            	console.log($genome + " " + $text);
            		$("#database").val($genome);
	                $("#genome-target dt a").html($text);
	                $("#genome-target dd ul").hide();
	                $("#genome").val($genome);
	            }
				populateBackground();
			}
			
			$(document).keyup(function(e) {
				if (e.keyCode == 27) { $('#dialog-overlay, #description-box').hide(); }   // esc
			});
			
			$(document).ready(function() {
				
				$("#range").attr('disabled', ''); /* turn off range input */			
	            createDropDown($("#genome"), $("#genome-dropdown"), "#genome-target");
				parseGET();
				
	            /* options popup window control */
	            $('#closepopup').click(function () {$('#dialog-overlay, #dialog-box, #description-box').hide(); return false; });
	            $('#options').click(function () {popup('dialog-box'); });
	            $(window).resize(function () {if (!$('#dialog-box').is(':hidden')) popup('dialog-box'); });
	            $(window).resize(function () {if (!$('#description-box').is(':hidden')) popup('description-box'); });
	            
				$("#symbol").autocomplete({
	                source: function( request, response ) {
	                    url = "getsymbols.htm?";
	                    $.getJSON(url + "start=" + $( "#symbol").val() +
	                    		"&genome=" + $( "#genome").val(), function(data) {
	                        response(data);
	                    });
	                },
					minLength: 2,
					select: function( event, ui ) {
						$( "#log").val("Done");
						populateTss();
					}
				});
				
				$("#getimage").click(function() {populateStats(); });
				$("#options").click(function() {popup('dialog-box'); });
				$("#go-left").click(function() {left(); redraw();});
				$("#go-right").click(function() {right(); redraw();});
				$("#zoom-in").click(function() {zoomin(); redraw();});
				$("#zoom-out").click(function() {zoomout(); redraw();});
				$("#example").click(function(){
					$("#symbol").val("Fos");
					populateTss();
				});
	            $("#show-stat").click(function() {
	            	showStatButtons(); 
	            	var new_position = $("#statsButtons").offset();
	            	window.scrollTo(0,new_position.top);
	            });
	            $("#redraw").click(function() {redraw();});
				
	            $("#symbol").keypress(
	            		function (e) {if (e.which == 13) {populateTss();} });
	            
	        });
			
		</script>
		
		<header class="gs-header cremag-a">
			<img src="css/images/seqinspectorlogo.png">
			<b>seq</b>inspector-one
			<a class="cremag-a" href="http://www.cremag.org" style="margin-left: 200px; padding-right: 10px; border-right: 1px solid #AAA;">Home</a>
			<a class="cremag-a" href="http://seqinspector.cremag.org" style="padding-left: 5px; padding-right: 10px; border-right: 1px solid #AAA;">Seqinspector</a>
			<a class="cremag-a" href="http://seqinspector.readthedocs.org/en/latest/" style="padding-left: 5px; padding-right: 10px; border-right: 1px solid #AAA;">Help</a>
			<!-- <a class="cremag-a" href="contact.htm" style="padding-left: 5px; padding-right: 10px; border-right: 1px solid #AAA;">Contact</a> -->
			<!-- <a class="cremag-a" href="news.htm" style="padding-left: 5px;">News</a> -->
		</header>
		
		<table>
			<tr style="height: 37px;">
				<td><div id="genome-dropdown"></div></td>
				<td><input id="symbol" class="gs-input-text gs-in-table" placeholder="Gene symbol"/></td>
				<td><div id="tss-info"></div></td>
				<td><div id="tss-dropdown"></div></td></tr>
		</table>
		<table>
			<tr style="height: 37px;">
				<td><input id="range" class="gs-input-text gs-in-table" placeholder="Range chr:start-end" style="width: 465px; margin:0 0 5 8px; background:#F5F5F5"/></td>
				<td>
					<button id="info-sig" class="greybtn gs-in-table" style="display:none"><span class="label">significant</span></button>
					<button id="info-tend" class="greybtn gs-in-table" style="display:none"><span class="label">with tendency</span></button>
					<button id="info-show" class="greybtn gs-in-table" style="display:none"><span class="label">Showing top 10</span></button>
					<button id="wait-tss" class="darkbtn gs-in-table" style="padding:5 5 5 8px; display:none"><span class="label" style="height: 31px !important;"><img src="css/images/ajax-loader.gif" style="vertical-align:middle;"><span style="padding:40 5 20 5px">Retrieving transcription start sites</span></span></button>			
					<button id="wait-stat" class="darkbtn gs-in-table" style="padding:5 5 5 8px; display:none"><span class="label" style="height: 31px !important;"><img src="css/images/ajax-loader.gif" style="vertical-align:middle;"><span style="padding:40 5 20 5px">Calculating statistics</span></span></button>
					<button id="wait-image" class="darkbtn gs-in-table" style="padding:5 5 5 8px; display:none"><span class="label" style="height: 31px !important;"><img src="css/images/ajax-loader.gif" style="vertical-align:middle;"><span style="padding:40 5 20 5px">Drawing image</span></span></button>
				</td>
			</tr>
		</table>
		
		<div style="margin:3 0 5 4px; height: 40px;">
			<button id="go-left" class="bluebtn gs-in-table"><span class="label"> << </span></button>
			<button id="zoom-in" class="bluebtn gs-in-table" style="padding:5 5 5 5px;"><span class="label" style="height: 37px !important;"><img src="css/images/zoomin.png"></span></button>
			<button id="zoom-out" class="bluebtn gs-in-table" style="padding:5 5 5 5px;"><span class="label" style="height: 37px !important;"><img src="css/images/zoomout.png"></span></button>
			<button id="go-right" class="bluebtn gs-in-table"><span class="label"> >> </span></button>
			<button id="options" class="bluebtn gs-in-table"><span class="label">Options</span></button>
			<button id="getimage" class="greenbtn gs-in-table" style="width: 203px; padding:10 50 6 70px;"><span class="label gs-centerize">Calculate</span></button>
			<button id="example" class="bluebtn gs-in-table" style="width: 103px; padding:10 20 6 20px;"><span class="label gs-centerize">Example</span></button>
			<button id="show-stat" class="orangebtn gs-in-table" style="display:none"><span class="label">Show statistics</span></button>
			
		</div>

		<div id="imageHolder" style="margin:3 0 5 8px; display:none; width: 760px; background:#F8F8F8;"></div>
		<div id="statsButtons" style="margin:3 0 5 8px; display:none; height: 40px;">
			<button id="redraw" class="bluebtn gs-in-table"><span class="label">Redraw</span></button>
			<button id="download" class="bluebtn gs-in-table"><span class="label">Download</span></button>
			<button id="downloadsvg" class="bluebtn gs-in-table"><span class="label">Download</span></button>
			<button id="downloadtab" class="bluebtn gs-in-table"><span class="label">Download results</span></button>
		</div>
		<div id="statsHolder" style="margin:3 0 5 8px; display:none;"></div>
		
		<div id="hidden-elements" class="hidden">
			<select id="genome">
 				<option selected="selected" value="mm9">Mus Musculus (mm9)</option>
 				<option value="mm10">Mus musculus (mm10)</option>
 				<option value="hg19">Homo sapiens (hg19)</option>
			</select>
			<select id="tss"></select>
			<input id="chromosome" placeholder="chromosome"/>
			<input id="position" placeholder="position"/>
			<textarea id = "log" rows="4" cols="60" class="ui-widget-content">Error logger:</textarea>
		</div>

		<div id="dialog-overlay" class="dialog-overlay"></div>
		<div id="description-box" class="dialog-box"></div>
		<div id="dialog-box" class="dialog-box">
			<div class="dialog-content">
				<div id="dialog-message" class="ui-widget">
					Database:<select id="database"><option value="mm9">mm9</option><option value="hg19">hg19</option></select><br/>
					Background:<select id="background"></select><br/>
					Upstream:<input id="upstream" placeholder="Upstream (bp)" value="1000" /><br/>
					Downstream:<input id="downstream" placeholder="Downstream (bp)" value="1000" /><br/>
				</div>
					<div class="gs-in-table">Filters:</div>
					<div class="gs-in-table"><input type="checkbox" class="filter" id="transcription" value="transcription" checked="yes"/>Transcription Factors</div>
					<div class="gs-in-table"><input type="checkbox" class="filter" id="histone" value="histone" />Histones</div>
					<div class="gs-in-table"><input type="checkbox" class="filter" id="polymerase" value="polymerase" />Polymerases</div>
					<div class="gs-in-table"><input type="checkbox" class="filter" id="dnase" value="dnase" />Dnases</div>
					<div class="gs-in-table"><input type="checkbox" class="filter" id="feature" value="feature" />Features</div>
					<div class="gs-in-table"><input type="checkbox" class="filter" id="input" value="input" />Controls</div>
					<div class="gs-in-table"><input type="checkbox" class="filter" id="other" value="other" />Others</div>
				
				<br/>
				<button id="closepopup" class="greenbtn" style="width: 120px;"><span class="label">Close</span></button>
				<br/><br/>
			</div>
		</div>
		<div id="spanner" style="height:500px;"></div>
		<%@ include file="/staticContent/footer.html" %>
		
	</body>
	
	
	
</html>
