SET IGNORECASE TRUE;

CREATE TABLE mm9position (
	id CHAR(18) NOT NULL,
	chromosome VARCHAR(12) NOT NULL,
	transcript_start INT(9) NOT NULL,
	transcript_end INT(9) NOT NULL,
	strand INT(1) NOT NULL,
	PRIMARY KEY (id)
) as select * from CSVREAD('classpath:sql-tables/mm9positions.txt');

CREATE TABLE mm9refseq (
	id CHAR(18) NOT NULL,
	refseq VARCHAR(15) NOT NULL
) as select * from CSVREAD('classpath:sql-tables/mm9refseqs.txt');

CREATE TABLE mm9symbol (
	id CHAR(18) NOT NULL,
	symbol VARCHAR(25) NOT NULL,
) as select * from CSVREAD('classpath:sql-tables/mm9symbols.txt');

CREATE TABLE mm10position (
	id CHAR(18) NOT NULL,
	chromosome VARCHAR(12) NOT NULL,
	transcript_start INT(9) NOT NULL,
	transcript_end INT(9) NOT NULL,
	strand INT(1) NOT NULL,
	PRIMARY KEY (id)
) as select * from CSVREAD('classpath:sql-tables/mm10positions.txt');

CREATE TABLE mm10refseq (
	id CHAR(18) NOT NULL,
	refseq VARCHAR(15) NOT NULL
) as select * from CSVREAD('classpath:sql-tables/mm10refseqs.txt');

CREATE TABLE mm10symbol (
	id CHAR(18) NOT NULL,
	symbol VARCHAR(25) NOT NULL,
) as select * from CSVREAD('classpath:sql-tables/mm10symbols.txt');

CREATE TABLE hg19position (
	id CHAR(18) NOT NULL,
	chromosome VARCHAR(30) NOT NULL,
	transcript_start INT(9) NOT NULL,
	transcript_end INT(9) NOT NULL,
	strand INT(1) NOT NULL,
	PRIMARY KEY (id)
) as select * from CSVREAD('classpath:sql-tables/hg19positions.txt');

CREATE TABLE hg19refseq (
	id CHAR(18) NOT NULL,
	refseq VARCHAR(15) NOT NULL
) as select * from CSVREAD('classpath:sql-tables/hg19refseqs.txt');

CREATE TABLE hg19symbol (
	id CHAR(18) NOT NULL,
	symbol VARCHAR(25) NOT NULL,
) as select * from CSVREAD('classpath:sql-tables/hg19symbols.txt');
