package org.cremag.genomeStore;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.cremag.utils.GenomeStoreFileUtils;
import org.cremag.utils.XmlUtils;
import org.cremag.utils.file.genomic.BigWig;
import org.cremag.utils.file.genomic.GenomicFileFactory;
import org.cremag.utils.file.genomic.GenomicTrack;
import org.cremag.utils.file.genomic.GenomicTrack.TrackClass;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

public class GenomeSchema {

	File schemaFile;
	Document doc;
	String descToString;
	
	private static final Logger logger = Logger
			.getLogger(GenomeSchema.class);
	
	public GenomeSchema(String schemaFile, String name) throws Exception {
		
		this.schemaFile = new File(schemaFile);
		//logger.debug("Schema exists: " + this.schemaFile.exists());
		logger.debug("Reading schema: " + this.schemaFile.getAbsoluteFile());
		if (!this.schemaFile.exists()) {
			this.schemaFile.createNewFile();
			this.createNewDocument(name);
			this.saveDocumentToFile();
		}
		this.readDocumentFromFile();
		if(!name.equals(this.getGenomeName()))
			throw new Exception("GenomeSchema does not match Genome name");
		
	}
	
	public Set <String> selectTrackIdsByKeyAndElement(String key, String element) {

		//logger.trace("Start: " + key + " " + element);
		
		Set <String> ids = new HashSet<String>();
		
		NodeList list = this.doc.getElementsByTagName("track");
		for(int i = 0; i < list.getLength(); i++) {
			String id = ((Element) list.item(i)).getAttribute("id");

			NodeList nodeList = ((Element) list.item(i)).getElementsByTagName(key);
			if(nodeList.getLength() == 1) {
				Element node = (Element) nodeList.item(0);
				if(node.getTagName().equals(key) && node.getAttribute("value").equals(element) ) {//value.getNodeValue().equals(element)) {
					ids.add(id);
				}
			}
		}

		return ids;
	}
	
	public Map<String, GenomicTrack> getTracks() {
		Map <String, GenomicTrack> trackList = new TreeMap <String, GenomicTrack>();
		
		/* reading tracks */
		NodeList list  = this.doc.getElementsByTagName("track");
		logger.info("Loading " + list.getLength() + " tracks");
		
		GenomicFileFactory factory = new GenomicFileFactory();
		
		for(int i = 0; i < list.getLength(); i++) {
			
			Element trackXmlElement = (Element) list.item(i);
			String id = trackXmlElement.getAttribute("id");
			String path = trackXmlElement.getAttribute("path");
			String name = id;
			String trackClass = TrackClass.OTHER.toString();
			//logger.trace("Loading track: " + id);
			try {
				name = ((Element) trackXmlElement.getElementsByTagName("name").item(0)).getAttribute("value");
				trackClass = ((Element) trackXmlElement.getElementsByTagName("class").item(0)).getAttribute("value");
				//logger.info("Loading track: \"" + name + "\" " + id + " Type: " + trackClass);
			} catch (Exception e) {
				logger.error("Loading track: " + id + " does not have a name or class");
			}
			GenomicTrack genomicFile = factory.createGenomicFile(path);
			genomicFile.setName(name);
			genomicFile.setTrackClass(TrackClass.getTrackClass(trackClass));
			//logger.trace("Name:" + name + " " + genomicFile.getName());
			trackList.put(id, genomicFile);
		}
		return trackList;
	}
	
	public Map<String, QuerySet> getBackgrounds() {
		Map <String, QuerySet> backgroundList = new TreeMap <String, QuerySet>();
		
		//logger.trace("Get Backgrounds");
		
		/* reading querySets */
		NodeList list  = this.doc.getElementsByTagName("background");
		logger.info("Loading " + list.getLength() + " querySets");
		
		for(int i = 0; i < list.getLength(); i++) {
			String id = ((Element) list.item(i)).getAttribute("id");
			String path = ((Element) list.item(i)).getAttribute("path");
			// logger.trace("Loading background: " + id);
			QuerySet querySet = new QuerySet(path);
			try {
				querySet.load();
				backgroundList.put(id, querySet);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return backgroundList;
	}	
	
	public String addTrack(String path) throws Exception {
		String name = GenomeStoreFileUtils.getFileName(path);
		Map <String, String> description = new TreeMap <String, String>();
		description.put("name", name);
		return this.addTrack(path, description);

	}
	
	public String addTrack(String path, Map <String, String> description) throws Exception {

		String id = path.substring(path.lastIndexOf("/") + 1);
		
		Element root = this.doc.getDocumentElement();
		Element track = this.doc.createElement("track");
		root.appendChild(track);
		
		track.setAttribute("id", id);
		track.setAttribute("path", path);
		track.setAttribute("type", "bigwig");

		logger.trace("Adding track to genome " + id);
		
		for(Entry <String, String> entry : description.entrySet()) {
			try {
				Element child = this.doc.createElement(entry.getKey().replace(" ", "_"));
				child.setAttribute("value", entry.getValue().trim());
				//child.setTextContent(GenomeStoreXmlUtils.stripNonValidXMLCharacters(entry.getValue()));
				track.appendChild(child);
			} catch (Exception e) {
				logger.error(e);
				logger.error(entry.getKey() + "\t" + entry.getValue());
			}
		}
		this.saveDocumentToFile();
		return path;
	}
	
	public void addBackground(String path) throws IOException {
		String id = path.substring(path.lastIndexOf("/") + 1);
		id = id.substring(0, id.indexOf('.'));
		
		Element root = this.doc.getDocumentElement();
		Element track = this.doc.createElement("background");
		root.appendChild(track);
		
		track.setAttribute("id", id);
		track.setAttribute("path", path);		
		
		this.saveDocumentToFile();		
	}
	
	private File getRootDirectory() {
		return this.schemaFile.getParentFile();
	}
	
	public String getNameOfTrack(GenomicTrack track) {
		String[] elements = track.getId().split("/");
		String trackId = elements[elements.length - 1];
		
		NodeList list  = this.doc.getElementsByTagName("track");
		for(int i = 0; i < list.getLength(); i++) {
			String id = ((Element) list.item(i)).getAttribute("id");
			if (id.equals(trackId)) {
				for (int j = 0; j < list.item(i).getChildNodes().getLength(); j++) {
					if(list.item(i).getChildNodes().item(j).getNodeName().equals("name"))
					return (list.item(i).getChildNodes().item(j).getChildNodes().item(0).getNodeValue());
				}
				
				return ((Element) list.item(i)).getAttribute("name");
			}
		}
		return "";
	}
	
	public String getGenomeName() {
		return doc.getDocumentElement().getAttribute("name");
	}
	
	public void saveDocumentToFile() {
		
		//logger.info("Saving schema to file");
		
		/* setting BufferedWriter from schemaFile */
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(this.schemaFile));
			writer.write(this.toString());
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Could not save schema to file");
		}
		
	}
	
	public void readDocumentFromFile() throws ParserConfigurationException, SAXException, IOException {
		
		/* Reading XML schema description file */
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		this.doc = dBuilder.parse(this.schemaFile);
		this.doc.getDocumentElement().normalize();
		
	}
	
	@Override
	public String toString() {
		return XmlUtils.documentToString(doc);
	}
	
	private void createNewDocument(String name) throws ParserConfigurationException {
		
		/* using DocumentBuilder to set up new document */
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		this.doc = docBuilder.newDocument();
		
		/* creating root element */
		Element rootElement = doc.createElement("genome");
		rootElement.setAttribute("name", name);
		doc.appendChild(rootElement);
		
	}

	public Map <String, String> getDescription(String trackId) {

		Map <String, String> description = new LinkedHashMap <String, String>();
		
		NodeList list  = this.doc.getElementsByTagName("track");
		for(int i = 0; i < list.getLength(); i++) {
			String id = ((Element) list.item(i)).getAttribute("id");
			if (id.equals(trackId)) {
				for (int j = 0; j < list.item(i).getChildNodes().getLength(); j++) {
					try {
						Element element = (Element) list.item(i).getChildNodes().item(j);
						//logger.info(element.getNodeName() + " " + element.getAttribute("value"));
						description.put(element.getNodeName(), element.getAttribute("value"));
						//logger.info(element.getNodeName() + " " + element.getChildNodes().item(0).getNodeValue());
					} catch (ClassCastException e) {} /* If not element do nothing */
					
					//if(list.item(i).getChildNodes().item(j).getNodeName().equals("name"))
					//return (list.item(i).getChildNodes().item(j).getChildNodes().item(0).getNodeValue());
				}
			}
		}
		
		return description;
		
	}

	public void remove(String trackId) {
		
		NodeList list  = this.doc.getElementsByTagName("track");
				
		for(int i = 0; i < list.getLength(); i++) {
			
			Element trackXmlElement = (Element) list.item(i);
			String id = trackXmlElement.getAttribute("id");
			String path = trackXmlElement.getAttribute("path");
			
			if(id.equals(trackId))
				trackXmlElement.getParentNode().removeChild(trackXmlElement);
			if (id.equals(trackId) && path.startsWith(this.getRootDirectory().getAbsolutePath()))
				new File(path).delete();
		}
		
		saveDocumentToFile();

		
	}
}
