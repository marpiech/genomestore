package org.cremag.genomeStore.web;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.log4j.Logger;
import org.cremag.dao.GenomicFeaturesDAO;
import org.cremag.dataSource.biomart.BiomartDAO;
import org.cremag.dataSource.flatFile.FlatFileDAO;
import org.cremag.genomeStore.Genome;
import org.cremag.genomeStore.GenomeStore;
import org.cremag.genomeStore.QuerySet;
import org.cremag.genomic.Position;
import org.cremag.utils.file.genomic.BigWig;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

@Controller
public class RestController {

	private static final Logger logger = Logger.getLogger(RestController.class);
	
	@Autowired 
	GenomeStore genomeStore;

	@Autowired
	GenomicFeaturesDAO genesDao;

	@RequestMapping(value = "/getsymbols.htm", method = RequestMethod.GET)
	public @ResponseBody
	String getsymbols(HttpServletRequest request, @RequestParam String start, @RequestParam String genome) {
		
		try {
			Set<String> symbols = new TreeSet<String>(genesDao.getSymbolsByBeginningString(start, genome));

			JSONArray jsonArray = new JSONArray();
			for(String symbol : symbols) {
				JSONObject json = new JSONObject();
				json.put("value", symbol);
				json.put("id", symbol);
				jsonArray.put(json);
			}
			return jsonArray.toString();
		} catch (Exception e) {
			logger.error("Could not retrieve symbol information " + e.getMessage());
			//e.printStackTrace();
			return "";
		}
    }
	
	@RequestMapping(value = "/getbackgrounds.htm", method = RequestMethod.GET)
	public @ResponseBody
	String getbackgrounds(@RequestParam String genome, HttpServletRequest request) {
		
		Set <QuerySet> backgroundSet = this.genomeStore.getGenome(genome).getBackgrounds();
		
		JSONArray jsonArray = new JSONArray();
		for(QuerySet querySet : backgroundSet) {
			JSONObject json = new JSONObject();
			json.put("value", querySet.getId());
			json.put("id", querySet.getId());
			jsonArray.put(json);
		}
        return jsonArray.toString();
    }

	@RequestMapping(value = "/refresh.htm", method = RequestMethod.GET)
	public @ResponseBody
	String refresh(HttpServletRequest request, @RequestParam String genome) {
		
		Set <QuerySet> backgroundSet = this.genomeStore.getGenome(genome).getBackgrounds();
		for(QuerySet querySet : backgroundSet) {
			logger.trace("Computing background:" + querySet.getId());
			querySet.compute(this.genomeStore.getGenome(genome));
			try {
				querySet.save();
			} catch (ParserConfigurationException e) {
				logger.error(e.getMessage());
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		}
		
		return "Done";
	
    }

	@RequestMapping(value = "/gettrackdescription.htm", method = RequestMethod.GET)
	public @ResponseBody
	String getTrackDescription(HttpServletRequest request, @RequestParam String trackId, @RequestParam String genome) {
		
		JSONArray jsonArray = new JSONArray();
		Map <String, String> description = genomeStore.getGenome(genome).getDescription(trackId);
		for(Entry <String, String> entry : description.entrySet()) {
			JSONObject json = new JSONObject();
			json.put("key", entry.getKey());
			json.put("value", entry.getValue());
			jsonArray.put(json);
		}
		return jsonArray.toString();
	}
	
	public GenomeStore getGenomeStore() {
		return genomeStore;
	}

	public void setGenomeStore(GenomeStore genomeStore) {
		this.genomeStore = genomeStore;
	}

	public GenomicFeaturesDAO getGenesDao() {
		return genesDao;
	}

	public void setGenesDao(GenomicFeaturesDAO genesDao) {
		this.genesDao = genesDao;
	}
}