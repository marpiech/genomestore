package org.cremag.genomeStore.web;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HtmController {

	private static final Logger logger = Logger.getLogger(HtmController.class);

	

	@RequestMapping("/home.htm")
	public ModelAndView homeRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		logger.info("Requested /home.htm from " + request.getRemoteAddr());
	
		if (request.getServerName().equals("seqone.cremag.org"))
			return new ModelAndView("seqinspector");
		if (request.getServerName().equals("seqinspector.cremag.org"))
			return new ModelAndView("seqinterpreter");
		
		ModelAndView mav = new ModelAndView("home");
		return mav;
	}

	@RequestMapping("/funding.htm")
	public ModelAndView newsRequest() {
		ModelAndView mav = new ModelAndView("funding");
		return mav;
	}

	@RequestMapping("/contact.htm")
	public ModelAndView contactRequest() {
		ModelAndView mav = new ModelAndView("contact");
		return mav;
	}

	@RequestMapping("/help.htm")
	public ModelAndView helpRequest() {
		ModelAndView mav = new ModelAndView("help");
		return mav;
	}
	
	@RequestMapping("/server.htm")
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String[][] variables = {
				{ "AUTH_TYPE", request.getAuthType() },
				{ "CONTENT_LENGTH", String.valueOf(request.getContentLength()) },
				{ "CONTENT_TYPE", request.getContentType() },
				{
						"DOCUMENT_ROOT",
						request.getSession().getServletContext()
								.getRealPath("/") },
				{ "PATH_INFO", request.getPathInfo() },
				{ "PATH_TRANSLATED", request.getPathTranslated() },
				{ "QUERY_STRING", request.getQueryString() },
				{ "REMOTE_ADDR", request.getRemoteAddr() },
				{ "REMOTE_HOST", request.getRemoteHost() },
				{ "REMOTE_USER", request.getRemoteUser() },
				{ "REQUEST_METHOD", request.getMethod() },
				{ "SCRIPT_NAME", request.getServletPath() },
				{ "SERVER_NAME", request.getServerName() },
				{ "SERVER_PORT", String.valueOf(request.getServerPort()) },
				{ "SERVER_PROTOCOL", request.getProtocol() },
				{
						"SERVER_SOFTWARE",
						request.getSession().getServletContext()
								.getServerInfo() } };
		String title = "CGI Variables";
		out.println("<html><head><title>seqinterpreter</title>"
				+ "<BODY BGCOLOR=\"#FDF5E6\">\n" + "<H1 ALIGN=CENTER>" + title
				+ "</H1>\n" + "<TABLE BORDER=1 ALIGN=CENTER>\n"
				+ "<TR BGCOLOR=\"#FFAD00\">\n"
				+ "<TH>CGI Variable Name<TH>Value");
		for (int i = 0; i < variables.length; i++) {
			String varName = variables[i][0];
			String varValue = variables[i][1];
			if (varValue == null)
				varValue = "<I>Not specified</I>";
			out.println("<TR><TD>" + varName + "<TD>" + varValue);
		}
		out.println("</TABLE></BODY></HTML>");
	}

	
	
}