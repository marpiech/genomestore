package org.cremag.genomeStore.exec;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class Run {
	private static final Logger logger = Logger.getLogger(Run.class);

	private static final ApplicationContext context = new FileSystemXmlApplicationContext(new String[] {
	"conf/exec-context.xml"});
	
	public static void main (String[] args) throws Exception {
		
		logger.info("Run was launched with the following arguments:");
		for(String arg : args)
			logger.info("Argument: " + arg);
		
		if (args[0].equals("add-dir"))
			addDir(args);

		if (args[0].equals("check-dir"))
			checkDir(args);
		
	}
	
	private static void addDir(String[] args) throws Exception {
		AddDirectory addDirectory = (AddDirectory) context.getBean("addDirectory");
		addDirectory.addDir(args);
	}

	private static void checkDir(String[] args) throws Exception {
		CheckDirectory checkDirectory = (CheckDirectory) context.getBean("checkDirectory");
		checkDirectory.checkDir(args);
	}
	
}
