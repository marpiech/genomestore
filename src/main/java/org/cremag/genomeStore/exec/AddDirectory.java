package org.cremag.genomeStore.exec;

import java.io.File;

import org.apache.log4j.Logger;
import org.cremag.genomeStore.Genome;
import org.cremag.genomeStore.GenomeStore;
import org.cremag.utils.GenomeStoreFileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class AddDirectory {
	
	private static final Logger logger = Logger.getLogger(AddDirectory.class);
	
	@Autowired
	private GenomeStore genomeStore;
	
	public void addDir (String[] args) throws Exception {
		
		if (args.length < 3) {
			System.out.println("Try to run with the following arguments: add-dir mm9 /path/to/dir");
			System.exit(1);
		}
		
		logger.info("Getting genome " + args[1]);
		
		Genome genome = genomeStore.getGenome(args[1]);
		
		/* Directory to be added */
		File dir = new File(args[2]);
		
		if(genome != null && dir.exists() && dir.isDirectory()) {
			File[] files = dir.listFiles();
			for(File file : files) {
				if(file.isFile() && (GenomeStoreFileUtils.getFileType(file.getPath()).equals("bigwig") || GenomeStoreFileUtils.getFileType(file.getAbsolutePath()).equals("bigbed")))
					genome.addTrack(file.getAbsolutePath(), false);
			}
		} else {
			logger.error("Failed to add directory");
		}
		
		logger.info("Finished adding directory");
		
	}
}
