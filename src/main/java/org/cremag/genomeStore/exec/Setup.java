package org.cremag.genomeStore.exec;

import org.apache.log4j.Logger;
import org.cremag.genomeStore.Genome;
import org.cremag.genomeStore.GenomeStore;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class Setup {
	private static final Logger logger = Logger.getLogger(Setup.class);
	
	ApplicationContext context = new FileSystemXmlApplicationContext(new String[] {
	"conf/application-context.xml"});
	GenomeStore genomeStore = (GenomeStore) context.getBean("genomeStore");
	
	public static void main(String args[]) {
		Setup mock = new Setup();
		mock.setup();
	}
	
	private void setup() {
		logger.info("Setting mock genomeStore");

		/* creating mock schema */

		Genome genome = genomeStore.getGenome("mm9");

		try {
			genome.addTrack("http://edison.cremag.org/resources/genomeStore/mm9/cremag-anno/mousensembl.bb", true);
		} catch (Exception e) {
			logger.error("Add Bigbed failed " + e.getMessage());
		}
		
		try {
			genome.addBackground("src/main/resources/mm9/reference.bed", "reference", "desc");
		} catch (Exception e) {
			logger.error("Add Reference failed " + e.getMessage());
		}
		
		/*try {
			genome.addLiftOverFile("http://hgdownload.cse.ucsc.edu/goldenPath/mm10/liftOver/mm10ToMm9.over.chain.gz");
		} catch (Exception e) {
			logger.error("Add LiftOver failed " + e.getMessage());
		}*/
		
		genome = genomeStore.getGenome("hg19");

		try {
			genome.addTrack("http://edison.cremag.org/resources/genomeStore/hg19/cremag-anno/humanensembl.bb", true);
		} catch (Exception e) {
			logger.error("Add Bigbed failed " + e.getMessage());
		}
		
		try {
			genome.addBackground("src/main/resources/hg19/reference.bed", "reference", "desc");
		} catch (Exception e) {
			logger.error("Add Reference failed " + e.getMessage());
		}		
		
	}
}
