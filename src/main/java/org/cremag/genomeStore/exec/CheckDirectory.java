package org.cremag.genomeStore.exec;

import java.io.File;

import org.apache.log4j.Logger;
import org.broad.igv.bbfile.BBFileReader;
import org.cremag.utils.GenomeStoreFileUtils;
import org.cremag.utils.file.genomic.BigWig;
import org.springframework.stereotype.Component;

@Component
public class CheckDirectory {
	
	private static final Logger logger = Logger.getLogger(CheckDirectory.class);
	
	public void checkDir (String[] args) throws Exception {
		
		File dir = new File(args[1]);
		
		if(dir.exists() && dir.isDirectory()) {
			File[] files = dir.listFiles();
			for(File file : files) {
				if(file.isFile() && (GenomeStoreFileUtils.getFileType(file.getPath()).equals("bigwig") || GenomeStoreFileUtils.getFileType(file.getAbsolutePath()).equals("bigbed"))) {
					try {
						BBFileReader reader = new BBFileReader(file.getAbsolutePath());
						new BigWig(reader);
						logger.info("" + file.getName() + " OK...");
					} catch (Exception e) {logger.error("" + file.getName() + " is corrupted");}
				}
			} /* for file : files */
		} /* if exists and isDirectory */
	}
}
