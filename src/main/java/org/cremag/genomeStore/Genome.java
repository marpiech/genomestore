/*
 * Copyright (C) 2012 - Marcin Piechota
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 Unported (CC BY-NC 3.0)
 * You may obtain a copy of the License at http://creativecommons.org/licenses/by-nc/3.0/legalcode
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package org.cremag.genomeStore;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.cremag.genomic.BedItem;
import org.cremag.genomic.Coverage;
import org.cremag.utils.GenomeStoreFileUtils;
import org.cremag.utils.NetUtils;
import org.cremag.utils.file.BedFile;
import org.cremag.utils.file.genomic.BigWig;
import org.cremag.utils.file.genomic.GenomicFileFactory;
import org.cremag.utils.file.genomic.GenomicTrack;
import org.cremag.utils.file.genomic.GenomicTrack.TrackType;
import org.cremag.utils.file.genomic.TrackDescription;
import org.cremag.utils.file.genomic.GenomicTrack.TrackClass;

/**
 * @author marpiech
 */
public class Genome implements Comparable<Genome> {
	
	/**
	 * root directory in which schema is stored
	 */
	File rootDirectory;
	
	/**
	 * XML description schema 
	 */
	GenomeSchema schema;
	
	/**
	 *  set of tracks with their ids
	 */
	Map <String, GenomicTrack> tracks;
	
	/**
	 *  set of querySets with their ids
	 */
	Map <String, QuerySet> querySets;
	
	
	private static final Logger logger = Logger
			.getLogger(Genome.class);
	
	
	/**
	 * @param directory
	 * @throws Exception
	 * constructor for allready existing genome
	 */
	public Genome(File directory) throws Exception {
		
		if (!directory.exists())
			throw new Exception ("Genome does not exist. Please create new genome by Genome(String path)");
		
		this.rootDirectory = directory;
		this.schema = new GenomeSchema(this.rootDirectory + "/description.xml", this.getName());
		
		/* Reading tracks */
		this.tracks = this.schema.getTracks();
		this.querySets = this.schema.getBackgrounds();
		for(QuerySet querySet : querySets.values())
			querySet.setGenome(this);
	}
	
	/**
	 * @param path
	 * @throws Exception
	 * constructor for nonexisting genome
	 */
	public Genome(String path) throws Exception {
		logger.trace("Creating new genome schema: " + path);
		
		this.rootDirectory = new File(path);
		if (this.rootDirectory.exists()) throw new Exception("Genome allready exists");
		this.rootDirectory.mkdir();
		this.schema = new GenomeSchema(this.rootDirectory + "/description.xml", this.getName());
		this.tracks = this.schema.getTracks();
		this.querySets = this.schema.getBackgrounds();
	}
	
	public String getNameOfTrack(GenomicTrack track) {
		return this.schema.getNameOfTrack(track);
	}
	
	public Set <GenomicTrack> selectTracksByKeyAndElement(String key, String element) {
		Set <GenomicTrack> output = new TreeSet <GenomicTrack> ();
		for(String id : schema.selectTrackIdsByKeyAndElement(key, element)) {
			output.add(tracks.get(id));
		}
		return output;
	}
	
	public Map <String, Float> getCoverageSum(BedItem bedItem) {
		Map <String, Float> output = new TreeMap <String, Float>();
		for(BigWig genomicFile : this.getBigWigTracks()) {
			Coverage coverage = genomicFile.getCoverage(bedItem);
			output.put(genomicFile.getId(), coverage.getSum());
		}
		return output;
	}
	
	public void addTrack(String path) throws Exception {
		addTrack(path, false);
	}
	
	public void addTrack(String path, boolean toCopy) throws Exception {
		String fileType = GenomeStoreFileUtils.getFileType(path);
		addTrack(path, fileType, toCopy);
		for(QuerySet querySet : this.querySets.values()) {
			querySet.recompute();
			querySet.save();
		}
	}
	
	public void addTrack(String path, String type, boolean toCopy) throws Exception {
		
		/* setting track id */
		String id = path.substring(path.lastIndexOf("/") + 1);
		logger.info("Adding to genome: " + this.getName() + 
				"; track: " + id + 
				"; type: " + type +
				"; copy: " + toCopy);
		
		if (tracks != null && tracks.get(id) != null)
			throw new Exception("Destination File allready exists");
		
		boolean isFile = false;
		boolean isUrl = false;
		
		/* checking if the file is local or remote */
		if(new File(path).exists())
			isFile = true;
		else
			isUrl = NetUtils.exists(path);
		
		if(!isFile && !isUrl)
			throw new Exception("Path is not valid");			
		
		/* if the file is remote we have to copy it */
		if(!isFile && !toCopy && isUrl) 
			throw new Exception("Valid remote Url should be copied. Use addTrack(path, true);");
		
		String destPath = path;
		TrackDescription description = null;
		
		/* copy local file */		
		if(isFile) {
			
			if(toCopy) {
				File source = new File(path);
				File dest = new File(this.rootDirectory.getAbsolutePath() + "/" + id);
				if(dest.exists())
					throw new Exception("Destination File allready exists");
				FileUtils.copyFile(source, dest);
				logger.trace("Local track copied");
				destPath = dest.getAbsolutePath();
			}
			description = new TrackDescription(new File(path));
		}
		
		/* copy remote file */
		if(!isFile && isUrl) {
			//logger.trace("Copying URL track");
			logger.trace("Copying URL:" + path);
			URL source = new URL(path);
			File dest = new File(this.rootDirectory.getAbsolutePath() + "/" + id);
			if(dest.exists())
				throw new Exception("Destination File allready exists");
			FileUtils.copyURLToFile(source, dest);
			
			logger.trace("Remote track copied");
			
			destPath = dest.getAbsolutePath();
			
			//logger.trace("Path:" + path);
			
			description = new TrackDescription(new URL(path));
		}
		
		try {
			if(description != null)
				schema.addTrack(destPath, description.getDescriptionMap());
			else
				schema.addTrack(destPath);
				
			GenomicTrack track = new GenomicFileFactory().createGenomicFile(destPath);
			track.setTrackClass(TrackClass.getTrackClass(description.getDescriptionMap().get("class")));
			tracks.put(destPath.substring(destPath.lastIndexOf("/") + 1), track);
			
		} catch (InvocationTargetException e) {
			e.getCause().printStackTrace();
		} 		
		
	}

//	public void addLiftOverFile(String path) throws IOException {
//		logger.trace("Downloading chain file: " + path);
//		String id = path.substring(path.lastIndexOf("/") + 1);
//		URL source = new URL(path);
//		File dest = new File(this.rootDirectory.getAbsolutePath() + "/" + id);
//		if(dest.exists())
//			logger.error("Destination exists! :: " + id);
//		else
//			FileUtils.copyURLToFile(source, dest);
//	}
	
	public void addBackground(String backgroundPath, String name, String desc) throws Exception {
		
		String backgroundXmlPath = this.rootDirectory.getAbsolutePath() + "/" + name + ".xml";
		
		logger.trace("Adding background: " + backgroundXmlPath);
		
		String id = backgroundXmlPath.substring(backgroundXmlPath.lastIndexOf("/") + 1);
		if(querySets.get(id) == null) {
		
			List<BedItem> bedItems = new BedFile(backgroundPath).getBedItems();
				
			/* computing background */
			QuerySet querySet = new QuerySet(bedItems, backgroundXmlPath);
			querySet.compute(this);
			querySet.save();
		
			schema.addBackground(backgroundXmlPath);
			
		} else {
			throw new RuntimeException("QuerySet allready exists");
		}
		
	}
	
	public void printDescription() {
		logger.info(this.schema);
	}
	
	public String getName() {
		return rootDirectory.getName();
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Genome)) return false;
		if (((Genome) o).getName().equals(this.getName()))
			return true;
		else
			return false;
	}

	public int compareTo(Genome arg) {
		return this.getName().compareTo(arg.getName());
	}

	public Set<GenomicTrack> getGenomicFiles() {
		return new TreeSet <GenomicTrack> (this.tracks.values());
	}	

	public Set<QuerySet> getBackgrounds() {
		return new HashSet <QuerySet> (this.querySets.values());
	}
	
//	public BedItem liftFrom(String originGenome, BedItem bed) {
//		logger.trace("lifting " + bed + " from " + originGenome + " to " + this.getName());
//		LiftOver liftOver = new LiftOver(new File(this.rootDirectory + "/" +
//				originGenome + "To" + this.getName().substring(0,1).toUpperCase() + this.getName().substring(1) + ".over.chain.gz"));
//		Interval out = liftOver.liftOver(new Interval(bed.getChromosome(), bed.getStart(), bed.getEnd()));
//		return new BedItem(out.getSequence(), out.getStart(), out.getEnd());
//	}

	public List <BigWig> getBigWigTracks() {
		List <BigWig> bigWigList = new ArrayList <BigWig> ();
		for(GenomicTrack track : this.tracks.values()) {
			if (track instanceof BigWig)
				bigWigList.add((BigWig) track);
		}
		//logger.info("BigWig tracks: " + bigWigList.size());
		return bigWigList;
	}

	public List <BigWig> getBigWigTracks(Set <TrackClass> filters) {
		List <BigWig> bigWigList = new ArrayList <BigWig> ();
		for(GenomicTrack track : this.tracks.values()) {
			if (track.getTrackType().equals(TrackType.BIGWIG)) {
				TrackClass trackClass = track.getTrackClass();
				for(TrackClass filter : filters)
					if (trackClass.equals(filter))
						bigWigList.add((BigWig) track);
			}
		}
		//logger.info("BigWig tracks with filters: " + bigWigList.size());
		return bigWigList;
	}
	
	public QuerySet getBackgroundById(String id) {
		return querySets.get(id);
	}

	public GenomicTrack getTrackById(String trackId) {
		return tracks.get(trackId);
	}

	public Map<String, String> getDescription(String trackId) {
		return schema.getDescription(trackId);
		
	}

	public void removeTrack(String trackId) {
		this.schema.remove(trackId);		
	}
}
