package org.cremag.genomeStore;

import java.io.File;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.cremag.utils.file.FileUtils;
import org.w3c.dom.Document;

public class GenomeStore {

	File rootDirectory;
	Document configuration;
	Set <Genome> genomes;
	private static final Logger logger = Logger
			.getLogger(GenomeStore.class);
	
	public GenomeStore() throws Exception {
		//logger.trace("Constructor() invoked");
		
		File fXmlFile = new File("conf/genomeStore.conf");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		this.configuration = dBuilder.parse(fXmlFile);
		this.configuration.getDocumentElement().normalize();
		
		String rootDirectoryPath = this.configuration.getDocumentElement().getElementsByTagName("directory").item(0).getFirstChild().getNodeValue();
		setRootDirectory(rootDirectoryPath);
		setGenomes();
	}
	
	public Set <Genome> getGenomes() {
		return genomes;
	}
	
	@Deprecated
	public Genome createNewGenome(String name) {
		try {
			Genome genome = new Genome(rootDirectory.getAbsolutePath() + "/" + name);
			genomes.add(genome);
			return genome;
		} catch (Exception e) {
			logger.error("Could not add genome to genome store: " + e.getMessage());
			return null;
		}
	}
	

	/**
	 * This method returns genome of given name or 
	 * creates new if the genome does not exists.
	 * If any error occurs method will return null
	 * @param name Genome name. For example <code>"mm9"</code>.
	 */
	public Genome getGenome(String name) {
		for(Genome genome : genomes)
			if(genome.getName().equals(name))
				return genome;
		return createNewGenome(name);
	}
	
	public void deleteGenome(String name) throws Exception {
		logger.info("deleting genome " + name);
		File genomeDirectory = new File(rootDirectory.getAbsolutePath() + "/" + name);
		if(genomeDirectory.exists())
			logger.info("genome directory exists");
		if(genomeDirectory.isDirectory())
			logger.info("genome directory is directory");
		if (genomeDirectory.exists() && genomeDirectory.isDirectory()) {
			genomes.remove(new Genome(genomeDirectory));
			FileUtils.recursiveDelete(genomeDirectory);
		} else {
			throw new Exception("There is no such genome to delete");
		}
	}
	
	private void setGenomes() throws Exception {
		this.genomes = new TreeSet <Genome>();
		File[] directories = rootDirectory.listFiles();
		for(File directory : directories) {
			if(directory.isDirectory())
				this.genomes.add(new Genome(directory));
		}
	}
	
	private void setRootDirectory(String path) {
		File directory = new File(path);
		if(directory.exists()) {
			logger.info("Genome Store location: " + directory.getAbsolutePath());
			this.rootDirectory = directory;
			return;
		} else {
			logger.info("Creating Genome Store in " + directory.getAbsolutePath());
			logger.info(directory.mkdirs());
			this.rootDirectory = directory;
			return;
		}
	}

	public boolean genomeExists(String name) {
		for(Genome genome : genomes)
			if(genome.getName().equals(name))
				return true;
		return false;
	}
}
