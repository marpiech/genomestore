package org.cremag.genomeStore;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.mortbay.jetty.Server;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;

public class RunGenomeStore {
	
	private static final Logger logger = Logger
			.getLogger(RunGenomeStore.class);
	private static File rootDirectory;
	
	public static void main (String[] args) throws ParserConfigurationException, SAXException, IOException {
		
		logger.info("Starting...");
		
		ApplicationContext context =
			    new FileSystemXmlApplicationContext(new String[] {"conf/jetty-context.xml"});
		Server server = (Server) context.getBean("server");
		
		ApplicationContext ctx 
		= new FileSystemXmlApplicationContext("conf/logging-context.xml");
		
		//logger.info("Genome Store is being initialized");
		
		File fXmlFile = new File("conf/genomeStore.conf");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		doc.getDocumentElement().normalize();
		
		String rootDirectoryPath = doc.getDocumentElement().getElementsByTagName("directory").item(0).getFirstChild().getNodeValue();
		rootDirectory = getRootDirectory(rootDirectoryPath);
		
	}
	
	private static File getRootDirectory(String path) {
		File directory = new File(path);
		if(directory.exists()) {
			//logger.info("The root directory is " + directory.getAbsolutePath());
			return directory;
		}
		logger.info("Creating root directory in " + directory.getAbsolutePath());
		logger.info(directory.mkdirs());
		
		return directory;
	}
	
}
