package org.cremag.externalProcess;

import java.io.IOException;

import org.cremag.genomic.BedItem;
import org.cremag.genomic.Coverage;

import pl.intelliseq.utils.BashExecutor;

/**
 * 
 * @author marpiech
 * this class calculates coverage for bigwigFile using Kent tools
 * 
 */
public class BigWigSummaryExec {

	BashExecutor exec = new BashExecutor();

	public Coverage getCoverageForBigWig(String path, BedItem bed) throws IOException, InterruptedException {
		return getCoverageForBigWig(path, bed, bed.getLength());
	}
	
	public Coverage getCoverageForBigWig(String path, BedItem bed, int n) throws IOException, InterruptedException {
	
		String process = 
			"executables/bigWigSummary " + path + " " + bed.getChromosome() + " " + bed.getStart() + " " + bed.getEnd() + " " + n;
	
		String[] out = exec.execute(process).split("\t");
		
		Coverage coverage = new Coverage(bed);
		
		float len = (float) bed.getLength(); /* length of genomic region */
		float nFloat = (float) n; /* float representation of number of divisions */
		float dist = nFloat / len; /* size of division */
		int iter = 0; /* iteration counter */
		
		for(String strVal : out) {
			float val = Float.parseFloat(strVal);
			float iterFloat = (float) iter;
			int start = (int) (iterFloat * dist);
			int end = (int) ((iterFloat + 1f) * dist);
			if (end <= start) end = start + 1;
			for (int i = start; i < end; i++)
				coverage.put(i, val);
				
		}
		
		return coverage;
	}
	
}
