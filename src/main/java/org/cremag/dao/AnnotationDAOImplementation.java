package org.cremag.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.httpclient.log.Log;
import org.apache.log4j.Logger;
import org.cremag.genomic.BedItem;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

public class AnnotationDAOImplementation implements AnnotationDAO {

	private static Logger logger = Logger.getLogger(AnnotationDAOImplementation.class);
	
	@Override
	public List<String> annotate(List<BedItem> bedItems, String genome) {
		
		List <String> names = new ArrayList <String> ();
		/*for(BedItem bedItem : bedItems) {
			names.add("Gene symbol");
		}*/
		
		Map <BedItem, String> symbols = new TreeMap <BedItem, String> ();
		try {
			symbols = post(bedItems, genome);
		} catch(Exception e) {e.printStackTrace();}
		
		for(BedItem item : bedItems) {
			String symbol = "-";
			if( symbols.containsKey(item))
				symbol = symbols.get(item);
			names.add(symbol);
		}
		
		return names;
		
	}
	
	public Map <BedItem, String> post(List <BedItem> bedItems, String genome) throws Exception {
		
		Map <BedItem, String> output = new TreeMap <BedItem, String> ();
		
		String bedString = "";
		for(BedItem item : bedItems)
			bedString += item.toString() + "\n";
		bedString = bedString.trim();
		
		String parameters = "Data=" + bedString + 
					"&Accuracy=" + "0" + 
					"&Genome=" + genome;
        
		// TODO: annotation url hack, all urls should be placed in one conf file
        URL url = new URL("http://bedanno.cremag.org");
        URLConnection conn = url.openConnection();
        conn.setDoOutput(true);
        OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
            
        System.out.println(parameters);
        
        //write parameters
        writer.write(parameters);
        writer.flush();
            
        // Get the response
        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {

        	String[] strSplit = line.split("\t");
        	BedItem item = new BedItem(strSplit[1], new Integer(strSplit[2]), new Integer(strSplit[3]));
        	String symbol = "-";
        	try {symbol = strSplit[8];} catch (Exception e) {logger.error(e.getMessage());};
        	output.put(item, symbol);
        	
        }
        writer.close();
        reader.close();
            
		return output;
	}
}
