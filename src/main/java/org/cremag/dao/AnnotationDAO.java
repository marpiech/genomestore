package org.cremag.dao;

import java.util.List;

import org.cremag.genomic.BedItem;

public interface AnnotationDAO {
	public List<String> annotate (List <BedItem> bedItems, String genome);
}
