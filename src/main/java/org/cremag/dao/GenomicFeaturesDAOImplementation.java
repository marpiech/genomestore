package org.cremag.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.cremag.genomic.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class GenomicFeaturesDAOImplementation implements GenomicFeaturesDAO {

	private JdbcTemplate jdbcTemplate;

	private final static String SYMBOLS_LIMIT = "5";
	private final static Logger logger = Logger.getLogger(GenomicFeaturesDAOImplementation.class);
	
	@Autowired
    public void setDataSource(DataSource dataSource) {
    	//logger.trace("Invoked construcor");
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

	@Override
	public List<String> getSymbolsByBeginningString(String wordBegin,
			String genome) {
		List <String> symbols = new LinkedList<String>();
		SqlRowSet rowSet = jdbcTemplate.queryForRowSet(
				"SELECT DISTINCT symbol FROM " + genome + "symbol " + 
				"WHERE symbol LIKE '" + wordBegin + "%' " + 
				"ORDER BY symbol " + 
				"LIMIT " + SYMBOLS_LIMIT);
		while(rowSet.next())
			symbols.add(rowSet.getString("symbol"));
		return symbols;
	}

	@Override
	public List<Position> getPositionsBySymbol(List <String> symbols, String genome) {
		
		List <Position> positions = this.getStartPositionsByEnsemblID(
				this.getEnsemblIDsBySymbol(symbols, genome), genome);
		return positions;
	}

	@Override
	public List<Position> getPositionsByRefseq(List <String> symbols, String genome) {
		List <Position> positions = this.getStartPositionsByEnsemblID(
				this.getEnsemblIDsByRefseq(symbols, genome), genome);
		return positions;
	}
	
	@Override
	public List<Position> getPositionsBySymbol(String symbol, String genome) {
		List <String> symbols = new ArrayList <String>();
		symbols.add(symbol);
		return this.getPositionsBySymbol(symbols, genome);
	}
	
	@Override
	public List<Position> getPositionsBySymbol(String symbol, String genome, int reduceDistance) {
		List <String> symbols = new ArrayList <String>();
		symbols.add(symbol);
		List <Position> positions = this.getPositionsBySymbol(symbols, genome);
		Collections.sort(positions);
		List <Position> reducedPositions = new ArrayList<Position>();
		Position previousPosition = positions.get(0);
		for(Position position : positions) {
			if(previousPosition.getDistanceTo(position) > reduceDistance) {
				reducedPositions.add(previousPosition);
				previousPosition = position;
			}
		}
		reducedPositions.add(previousPosition);
		return reducedPositions;
	}
	
	@Override
	public List<Position> getStartPositionsByEnsemblID(List <String> ensemblIDs, String genome) {
		List <Position> positions = new ArrayList<Position>();
		SqlRowSet rowSet = jdbcTemplate.queryForRowSet(
				"SELECT DISTINCT chromosome, transcript_start, transcript_end, strand " +
				"FROM " + genome + "position " + 
				"WHERE (id IN (" + this.convertListToComaSeparatedString(ensemblIDs) + "))" +
				"ORDER BY transcript_start");
		while(rowSet.next()) {
			if(rowSet.getString("chromosome").length() <= 5) {
				if(rowSet.getInt("strand") == 1)
					positions.add(new Position("chr" + rowSet.getString("chromosome"), rowSet.getInt("transcript_start")));
				else
					positions.add(new Position("chr" + rowSet.getString("chromosome"), rowSet.getInt("transcript_end")));
			}
		}
		return positions;
	}

	@Override
	public List<String> getEnsemblIDsBySymbol(List <String> symbols, String genome) {
		
		if(genome.equals("hg19")) {
			List <String> upperCaseSymbols = new ArrayList <String> ();
			for(String line : symbols)
				upperCaseSymbols.add(line.toUpperCase());
			symbols = upperCaseSymbols;
		}

		
		//logger.trace("DAO: " + genome);
		//logger.trace("DAO symbols: " + this.convertListToComaSeparatedString(symbols));
		
		List <String> ensemblIds = new LinkedList<String>();
		SqlRowSet rowSet = jdbcTemplate.queryForRowSet(
				"SELECT DISTINCT id FROM " + genome + "symbol " + 
				"WHERE (symbol IN (" + this.convertListToComaSeparatedString(symbols) + "))");
		while(rowSet.next())
			ensemblIds.add(rowSet.getString("id"));
		
		//logger.trace("TOTAL: " + ensemblIds.size());
		
		return ensemblIds;
	}

	@Override
	public List<String> getEnsemblIDsByRefseq(List <String> symbols, String genome) {
		List <String> ensemblIds = new LinkedList<String>();
		SqlRowSet rowSet = jdbcTemplate.queryForRowSet(
				"SELECT DISTINCT id FROM " + genome + "refseq " + 
				"WHERE (refseq IN (" + this.convertListToComaSeparatedString(symbols) + "))");
		while(rowSet.next())
			ensemblIds.add(rowSet.getString("id"));
		return ensemblIds;
	}
	
	private String convertListToComaSeparatedString(List<String> strings) {
		return join(strings.iterator(), ",");
	}
	
	private static String join(Iterator<String> iterator, String separator) {
		
	    /* handle null, zero and one elements before building a buffer */ 
	    Object first = iterator.next();
	    if (!iterator.hasNext()) {
	        return "'" + first.toString() + "'";
	    }
	    
	    /* two or more elements */ 
	    StringBuffer buf = 
	        new StringBuffer(256); 
	    if (first != null) {
	        buf.append("'" + first + "'");
	    }
	    while (iterator.hasNext()) {
	        if (separator != null) {
	            buf.append(separator);
	        }
	        Object obj = iterator.next();
	        if (obj != null) {
	            buf.append("'" + obj + "'");
	        }
	    }
	    return buf.toString();
	}


}
