package org.cremag.dao;

import java.util.List;

import org.cremag.genomic.Position;

public interface GenomicFeaturesDAO {
	
	List<String> getSymbolsByBeginningString(String wordBegin, String genome);
	List<Position> getPositionsBySymbol(List<String> symbol, String genome);
	List<Position> getPositionsBySymbol(String symbol, String genome);
	List<Position> getPositionsBySymbol(String symbol, String genome, int reduceDistance);
	List<Position> getPositionsByRefseq(List<String> symbol, String genome);
	List<Position> getStartPositionsByEnsemblID(List<String> ensembl, String genome);
	List<String> getEnsemblIDsBySymbol(List<String> symbol, String genome);
	List<String> getEnsemblIDsByRefseq(List<String> symbol, String genome);
	
}
