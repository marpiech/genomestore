/*
 * Copyright (C) 2012 - Marcin Piechota
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 Unported (CC BY-NC 3.0)
 * You may obtain a copy of the License at http://creativecommons.org/licenses/by-nc/3.0/legalcode
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package org.cremag.seqinterpreter;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.log4j.Logger;
import org.cremag.dao.AnnotationDAO;
import org.cremag.dao.GenomicFeaturesDAO;
import org.cremag.genomeStore.Genome;
import org.cremag.genomeStore.GenomeStore;
import org.cremag.genomeStore.QuerySet;
import org.cremag.genomic.BedItem;
import org.cremag.genomic.Coverage;
import org.cremag.genomic.Position;
import org.cremag.genomic.id.GeneIDRecognizer;
import org.cremag.genomic.id.GeneIDType;
import org.cremag.tools.LiftOver;
import org.cremag.utils.StringUtils;
import org.cremag.utils.file.genomic.BigWig;
import org.cremag.utils.plot.PlotProperties;
import org.cremag.utils.stats.StatisticalResultSet;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author marpiech
 */
@Controller
@Scope("session")
public class SeqinterpreterController {

	private static final Logger logger = Logger.getLogger(SeqinterpreterController.class);
	ApplicationContext context;
	
	@Autowired
	GenomeStore genomeStore;
	
	@Autowired
	GenomicFeaturesDAO genomicDAO;
	
	@Autowired
	AnnotationDAO annotationDAO;
	
	@Autowired
	GeneIDRecognizer geneIDRecognizer;
	
	SeqinterpreterSession seqinterpreterSession = new SeqinterpreterSession();
	
	@RequestMapping("/seqinterpreter.htm")
	public ModelAndView seqinspectorRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		ModelAndView mav = new ModelAndView("seqinterpreter");
		return mav;
	}
	
	@RequestMapping("/set-reference")
	public void setReference(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		/* 
		 * if there is no query-set with the given name
		 * it is probable that this genome background-set
		 */
		
		//logger.info("SET GENOME" + request.getParameter("database"));
		logger.info("SET REFERENCE" + request.getParameter("background"));
		
		if(!seqinterpreterSession.setReference(request.getParameter("background"))) {
			//logger.info("SET-IN");
			System.out.println("DATABASE" + request.getParameter("database") + " " + request.getParameter("background"));
			Genome genome = genomeStore.getGenome(request.getParameter("database"));
			QuerySet querySet = genome.getBackgroundById(request.getParameter("background")).copy();
			this.seqinterpreterSession.addQuerySet(querySet.getId(), querySet);
			this.seqinterpreterSession.setReference(querySet.getId());
		}
		
		//logger.info("REF GENOME: " + this.seqinterpreterSession.getQuerySets().get(0).getGenome().getName());
		
	}

	@RequestMapping("/remove-queryset")
	public void removeQuerySet(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		seqinterpreterSession.removeQuerySet(request.getParameter("queryid"));
		
	}

	@RequestMapping("/rename-queryset")
	public void renameQuerySet(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		seqinterpreterSession.renameQuerySet(request.getParameter("queryid"), request.getParameter("newname"));
		
	}
	
	@RequestMapping("/set-options.htm")
	public void setOptions(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		if(!seqinterpreterSession.setDatabase(request.getParameter("database"))) {
			seqinterpreterSession = new SeqinterpreterSession();
			seqinterpreterSession.setDatabase(request.getParameter("database"));
		}
		if (seqinterpreterSession.getProgress() < 100)
			throw new RuntimeException("Computation in progress");
		seqinterpreterSession.setProgress(0);
		seqinterpreterSession.setFilters(request.getParameter("filters"));
		seqinterpreterSession.setProgress(100);
		
	}

	
	@RequestMapping("/get-querysets")
	public @ResponseBody
	String getQuerySets(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		return this.seqinterpreterSession.getJSON();
		
	}	
	
	@RequestMapping("/show-genes")
	public @ResponseBody
	String showGenes(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		QuerySet querySet = seqinterpreterSession.getQuerySet(request.getParameter("queryid"));
		String genome = request.getParameter("genome");
		
		BigWig track = null;
		try {
			track = (BigWig) seqinterpreterSession.getQuerySets().get(0).getGenome().getTrackById(request.getParameter("trackID"));
		} catch (Exception e) { /* null is normal */ }
		
		//System.out.println("TRACKID: " + );
		
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "no-cache");
		
		
		logger.trace("Genome " + genome);
		List <String> names = annotationDAO.annotate(querySet.getBedItems(), genome);
		
		List <JSONObject> jsonList = new ArrayList <JSONObject> (); 
		
		
		
		int counter = 0;
		for(BedItem item : querySet.getBedItems()) {
			JSONObject json = new JSONObject();
			json.put("Interval", item.toString());
			json.put("Name", names.get(counter++));
			if (track != null) {
				Coverage coverage = track.getCoverage(item);
				json.put("Max", StringUtils.roundToSignificantFigures(new Double(coverage.getMax()), 4));
				json.put("Avg", StringUtils.roundToSignificantFigures(new Double(coverage.getAverage()), 4));
			}
			jsonList.add(json);
		}
	
		Collections.sort(jsonList, new JSONComparator());
		JSONArray jsonArray = new JSONArray();
		for(JSONObject jsonObject : jsonList)
			jsonArray.put(jsonObject);
		
		return jsonArray.toString();
	}
	
	
	@RequestMapping("/progress.htm")
	public @ResponseBody
	String progress(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		//System.out.println("Progress " + seqinterpreterSession.getProgress());
		return "" + seqinterpreterSession.getProgress();
		
	}
	
	@RequestMapping("/add-queryset.htm")
	public void addQuerySet(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		if (seqinterpreterSession.getProgress() < 100)
			throw new RuntimeException("Computation in progress");
		seqinterpreterSession.setProgress(0);
		
		try {
		
		String originGenome = request.getParameter("genome");
		String query = request.getParameter("query");
		int extend = 1000;
		try {
			extend = Integer.parseInt(request.getParameter("extend"));
		} finally {
			
		}
		Genome genome = genomeStore.getGenome(request.getParameter("database"));
		//QuerySet background = genome.getBackgroundById(request.getParameter("background"));
		
		/*
		 * Controller will try to detect query type in identifyQueryType method
		 * 1) First it will check if line is bed item
		 * 2) Then it will check if it is ucsc-type genomic range
		 * 3) Next it will check if it is gene symbol
		 * 4) Finally it will check if it is refseq or ensembl
		 */

		List <String> queryLines = Arrays.asList(query.split("\n"));
				
		/* TODO: constant extend hack */
		List <BedItem> rawBedItems = this.getBedItems(queryLines, originGenome, extend);
		List <BedItem> bedItems = new ArrayList <BedItem> ();
		if(!originGenome.equals(genome.getName())) {
			for (BedItem bed : rawBedItems) {
				System.out.println("lifting");
				bedItems.add(LiftOver.lift(originGenome, genome.getName(), bed));
				System.out.println("finished lifting");
			}
		} else {
			bedItems = rawBedItems;
		}
		
		QuerySet positionSet = new QuerySet(bedItems);
		
		List <BigWig> tracks = genome.getBigWigTracks(seqinterpreterSession.getFilters());
		int size = tracks.size();
		int BATCH_SIZE = 10;
		for(int i = 0; i < size; i = i + BATCH_SIZE) {
			List <BigWig> batch = new ArrayList <BigWig>();
			for(int which = 0; which < BATCH_SIZE && (i + which) < size; which++)
				batch.add(tracks.get(i + which));
			seqinterpreterSession.setProgress(((int) ((double) 100 * (double) i / (double) size)));
			positionSet.batchCompute(batch);
		}
		seqinterpreterSession.setProgress(100);
		positionSet.setGenome(genome);
		//positionSet.compute(genome, seqinterpreterSession.getFilters());
		String name = seqinterpreterSession.addQuerySet(positionSet);
		ServletOutputStream out = response.getOutputStream();
		out.print(name);
		
		} catch (Exception e) {
			e.printStackTrace();
			response.getOutputStream().print("FALSE");
		}
		
	}
	
	@RequestMapping("/calculate-stat.htm")
	public void calculateStatRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		try {
		
		QuerySet reference = seqinterpreterSession.getQuerySet(request.getParameter("background"));	
		QuerySet query = seqinterpreterSession.getQuerySet(request.getParameter("query"));

		/*logger.info(request.getParameter("background"));
		logger.info(request.getParameter("query"));
		logger.info("QuerySet " + background != null);
		logger.info("Query " + query != null);*/
		
		StatisticalResultSet results = reference.tTest(query);
		
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "no-cache");
		
		ServletOutputStream out = response.getOutputStream();
		out.print(results.getJSON().toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@RequestMapping("/calculate-stat-csv.htm")
	public void calculateStatCSVRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		try {
		
		QuerySet reference = seqinterpreterSession.getQuerySet(request.getParameter("background"));	
		QuerySet query = seqinterpreterSession.getQuerySet(request.getParameter("query"));

		StatisticalResultSet results = reference.tTest(query);
		
		response.setContentType("application/json");
		response.setHeader("Content-Disposition", "attachment; filename=statistics.tsv");
		response.setHeader("Cache-Control", "no-cache");
		
		ServletOutputStream out = response.getOutputStream();
		out.print(results.getCSV());

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * @param request
	 * @param response
	 * @throws Exception
	 * plots svg histogram
	 */
	@RequestMapping("/get-stackplot.htm")
	public void stackPlotRequest(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam String height, @RequestParam String width,
			@RequestParam String trackID, @RequestParam String backgroundID,
			@RequestParam String databaseID) throws Exception {
		
		Genome genome = genomeStore.getGenome(databaseID);

		BigWig track = (BigWig) genome.getTrackById(trackID);
		
		SVGGraphics2D svgGenerator = new SVGGraphics2D(
        		GenericDOMImplementation.getDOMImplementation().createDocument(
        				"http://www.w3.org/2000/svg", "svg", null)
        				);
		
		PlotProperties plot = new PlotProperties();
		plot.setX(30);
		plot.setY(30);
		plot.setWidth(new Integer(width) - 10);
		plot.setHeight(new Integer(height) - 30);

		track.plot(plot, svgGenerator, seqinterpreterSession.getQuerySets());
		
		response.setContentType("image/svg+xml");
		if (request.getParameter("download") != null)
			response.setHeader("Content-Disposition", "attachment; filename=histogram.svg");
		
		TransformerFactory transfac = TransformerFactory.newInstance();
		Transformer trans = transfac.newTransformer();
		trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		trans.setOutputProperty(OutputKeys.INDENT, "yes");

		//create string from xml tree
		StringWriter sw = new StringWriter();
		svgGenerator.stream(sw);
		String xmlString = sw.toString();
		
		ServletOutputStream out = response.getOutputStream();
		out.print(xmlString);
		out.close();
		
	}

	/**
	 * @param request
	 * @param response
	 * @throws Exception
	 * plots svg histogram
	 */
	@RequestMapping("/get-histogram.htm")
	public void histogramRequest(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam String height, @RequestParam String width,
			@RequestParam String trackID, @RequestParam String backgroundID,
			@RequestParam String databaseID) throws Exception {
		
		Genome genome = genomeStore.getGenome(databaseID);

		BigWig track = (BigWig) genome.getTrackById(trackID);
		
		SVGGraphics2D svgGenerator = new SVGGraphics2D(
        		GenericDOMImplementation.getDOMImplementation().createDocument(
        				"http://www.w3.org/2000/svg", "svg", null)
        				);
		
		PlotProperties plot = new PlotProperties();
		plot.setX(30);
		plot.setY(30);
		plot.setWidth(new Integer(width) - 10);
		plot.setHeight(new Integer(height) - 50);

		int RANGE = 1000;
		
		track.histogram(plot, svgGenerator, seqinterpreterSession.getQuerySets(), RANGE);
		
		response.setContentType("image/svg+xml");
		if (request.getParameter("download") != null)
			response.setHeader("Content-Disposition", "attachment; filename=histogram.svg");
		
		TransformerFactory transfac = TransformerFactory.newInstance();
		Transformer trans = transfac.newTransformer();
		trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		trans.setOutputProperty(OutputKeys.INDENT, "yes");

		//create string from xml tree
		StringWriter sw = new StringWriter();
		svgGenerator.stream(sw);
		String xmlString = sw.toString();
		
		ServletOutputStream out = response.getOutputStream();
		out.print(xmlString);
		out.close();
		
	}	
	
	
	private List<BedItem> getBedItems(List<String> nonTrimmedQueryLines,
			String originGenome, int extend) {
		
		//logger.trace("GENOME: " + originGenome);
		//for(String str : nonTrimmedQueryLines)
		//	logger.trace("------ " + str);
		
		/* trim lines */
		List<String> queryLines = new ArrayList <String> ();
		for(String line : nonTrimmedQueryLines) {
			queryLines.add(line.trim());
		}
			
		
		GeneIDType queryType = this.geneIDRecognizer.identifyQueryType(queryLines, originGenome);
		
		logger.trace("QUERY TYPE: " + queryType);
		
		List <BedItem> bedItems = new ArrayList <BedItem> ();
		
		if (queryType == null) return null;
		if (queryType == GeneIDType.BED)
			for(String line : queryLines) {
				try {bedItems.add(new BedItem(line));} catch (Exception e) {}
			}
		if (queryType == GeneIDType.SYMBOL) {
			for(Position position : this.genomicDAO.getPositionsBySymbol(queryLines, originGenome))
				bedItems.add(position.getBedItem(extend));
		}
		if (queryType == GeneIDType.REFSEQ) {
			for(Position position : this.genomicDAO.getPositionsByRefseq(queryLines, originGenome))
				bedItems.add(position.getBedItem(extend));
		}
		if (queryType == GeneIDType.ENSEMBL) {
			for(Position position : this.genomicDAO.getStartPositionsByEnsemblID(queryLines, originGenome))
				bedItems.add(position.getBedItem(extend));
		}
		
		Set <BedItem> reducedItems = new TreeSet <BedItem> (bedItems);
		bedItems = new ArrayList <BedItem> (reducedItems);
		Collections.sort(bedItems);
		
		List <BedItem> mergedItems = new ArrayList <BedItem> ();
		BedItem previousBed = bedItems.get(0);
		for(BedItem item : bedItems) {
			if(item.overlaps(previousBed, false))
				previousBed = new BedItem(previousBed.getChromosome(), previousBed.getStart(), item.getEnd());
			else {
				mergedItems.add(previousBed);
				previousBed = item;
			}
		}
		mergedItems.add(previousBed);
		return mergedItems;
	}

	public GenomeStore getGenomeStore() {
		return genomeStore;
	}
	public void setGenomeStore(GenomeStore genomeStore) {
		this.genomeStore = genomeStore;
	}	
}