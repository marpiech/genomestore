package org.cremag.seqinterpreter;

import java.util.Comparator;

import org.cremag.genomic.BedItem;
import org.json.JSONObject;

public class JSONComparator implements Comparator <JSONObject> {

	@Override
	public int compare(JSONObject o1, JSONObject o2) {
		try {
			return (new Double(o2.get("Avg").toString()).compareTo(new Double(o1.getString("Avg").toString())));
		} catch(Exception e) {
			try {
				return (new BedItem(o1.get("Interval").toString()).compareTo(new BedItem(o2.get("Interval").toString())));
			} catch (Exception error) {
				return 0;
			}
		}

	}
}
