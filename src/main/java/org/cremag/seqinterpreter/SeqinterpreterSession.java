package org.cremag.seqinterpreter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.cremag.genomeStore.QuerySet;
import org.cremag.utils.file.genomic.GenomicTrack.TrackClass;
import org.json.JSONArray;
import org.json.JSONObject;

public class SeqinterpreterSession {

	private static int sessionCount = 0;
	private final static Logger logger = Logger.getLogger(SeqinterpreterSession.class);
	
	private Map <String, QuerySet> querySets = new TreeMap <String, QuerySet>();
	private String reference = "";
	private String database = "";
	private Set <TrackClass> filters = new HashSet <TrackClass> ();
	
	int count = 1;
	
	int progress = 100;
	
	public SeqinterpreterSession () {
		sessionCount++;
		logger.info("Starting session number: " + sessionCount);
		filters.add(TrackClass.TRANSCRIPTION);
		database = "mm9";
	}

	public String addQuerySet (String name, QuerySet querySet) {
		
		logger.info("Adding background to session: " + name);
		
		querySets.put(name, querySet);
		querySet.recompute(this.filters);
		if (querySets.size() == 1)
			this.setReference(name);
		return name;
	}
	
	public boolean setReference (String name) {
		if(querySets.get(name) != null) {
			reference = name;
			return true;
		}
		return false;
	}
	
	public String addQuerySet (QuerySet querySet) {
		String name = "set_" + count++;
		return addQuerySet (name, querySet);
	}
	
	public QuerySet removeQuerySet (String name) {
		return querySets.remove(name);
	}

	public void renameQuerySet(String name, String newname) {
		QuerySet query = querySets.remove(name);
		querySets.put(newname, query);
	}
	
	public QuerySet getQuerySet (String name) {
		return querySets.get(name);
	}
	
	public List <QuerySet> getQuerySets () {
		List <QuerySet> out = new ArrayList <QuerySet> ();
		out.add(querySets.get(reference));
		for(Entry <String, QuerySet> entry : querySets.entrySet()) {
			entry.getValue().setName(entry.getKey());
			if (!entry.getKey().equals(reference))
				out.add(entry.getValue());
		}
		return out;
	}

	public String getJSON() {
		JSONArray json = new JSONArray();
		for(QuerySet set : this.getQuerySets()) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("id", set.getName());
			json.put(jsonObject);
		}
		return json.toString();
	}

	public void setFilters(String filters) {
		Set <TrackClass> newFilters = TrackClass.getFilters(filters);
		if (!this.filters.equals(newFilters)) {
			this.filters = newFilters;
			recalculate();
		}
	}
	
	public void recalculate() {
		int counter = 1;
		int len = querySets.values().size();
		for(QuerySet querySet : querySets.values()) {
			setProgress((100 * counter++) / len);
			querySet.recompute(this.filters);
		}
	}
	
	public Set <TrackClass> getFilters() {
		return this.filters;
	}
	
	public boolean setDatabase(String database) {
		
		System.out.println("SET DATABASE " + database);
		
		if (!this.database.equals("") && !this.database.equals(database)) {
			this.database = database;
			return false;
		} else {
			this.database = database;
			return true;
		}
	}
	public String getDatabase() {
		return this.getDatabase();
	}

	public int getProgress() {
		return progress;
	}
	
	public void setProgress(int progress) {
		this.progress = progress;
	}


	
}
