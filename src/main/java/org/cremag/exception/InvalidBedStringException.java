package org.cremag.exception;

@SuppressWarnings("serial")
public class InvalidBedStringException extends RuntimeException {
	public InvalidBedStringException(String message) {
		super(message);
	}
}
