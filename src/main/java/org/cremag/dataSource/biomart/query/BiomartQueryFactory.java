package org.cremag.dataSource.biomart.query;

import org.cremag.dataSource.biomart.query.builder.AttributeBuilder;
import org.cremag.dataSource.biomart.query.builder.DatasetBuilder;
import org.cremag.dataSource.biomart.query.builder.FilterBuilder;


public class BiomartQueryFactory {
	
	private String martserviceURL;

	public BiomartQueryFactory(String martserviceURL) {
		this.martserviceURL = martserviceURL;
	}
	
	public BiomartQuery getSkeletonQuery() {
		return new BiomartQuery(this.martserviceURL);
	}
	
	public BiomartQuery getQueryForTssFromGeneName(String geneName, String dataset) {
		return this.getSkeletonQuery()
				.setDataset(new DatasetBuilder().setName(dataset).setConfig("gene_ensembl_config"))
				.addFilter(new FilterBuilder().setName("wikigene_name").setValue(geneName))
				.addAttribute(AttributeBuilder.ChrmosomeNameAttributeBuilder)
				.addAttribute(AttributeBuilder.StrandAttributeBuilder)
				.addAttribute(AttributeBuilder.TranscriptStartAttributeBuilder)
				.addAttribute(AttributeBuilder.TranscriptEndAttributeBuilder);
	}
}
