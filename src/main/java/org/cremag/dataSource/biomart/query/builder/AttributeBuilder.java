package org.cremag.dataSource.biomart.query.builder;

public class AttributeBuilder extends AbstractBuilder {

	public static final AttributeBuilder TranscriptStartAttributeBuilder = new AttributeBuilder().setName("transcript_start");
	public static final AttributeBuilder ChrmosomeNameAttributeBuilder = new AttributeBuilder().setName("chromosome_name");
	public static final AttributeBuilder TranscriptEndAttributeBuilder = new AttributeBuilder().setName("transcript_end");
	public static final AttributeBuilder StrandAttributeBuilder = new AttributeBuilder().setName("strand");
	
	public AttributeBuilder() {
		this.tag = "Attribute";
	}
	
	public AttributeBuilder setName(String name) {
		return (AttributeBuilder) this.setAttribute("name", name);
	}

	// TODO: Implement proper validation
	protected Boolean validate() {
		return Boolean.TRUE;
	}

}
