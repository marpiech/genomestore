package org.cremag.dataSource.biomart.query.result;

import java.util.ArrayList;
import java.util.List;

public class ResultRow extends ArrayList<String> {

	private static final long serialVersionUID = 1L;
	
	protected ResultRow(List<String> rowData) {
		super(rowData);
	}
	
}
