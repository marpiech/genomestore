package org.cremag.dataSource.biomart.query.result;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;


public class ResultMapper<T> {
	
	ArgumentProvider<?>[] argumentProviders;
	
	public ResultMapper(ArgumentProvider<?>[] argumentProviders) {
		this.argumentProviders = argumentProviders;
	}
	
	public List<T> apply(Result result, Class<T> klass) {
		try {
			Object[] constructorArguments = new Object[this.argumentProviders.length];
			Class<?>[] constructorSignature = new Class[this.argumentProviders.length];
			List<T> mappings = new ArrayList<T>();
			for (ResultRow resultRow : result) {
				for (int i = 0; i < this.argumentProviders.length; i++) {
					constructorArguments[i] = this.argumentProviders[i].get(resultRow);
					constructorSignature[i] = constructorArguments[i].getClass();
				}
				Constructor<T> constructor = klass.getConstructor(constructorSignature);
				T mapping = constructor.newInstance(constructorArguments);
				mappings.add(mapping);
			}
			return mappings;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static class ColumnReference {
		
		protected Integer i;
		
		public ColumnReference(Integer i) {
			this.i = i;
		}
		
	}
	
	public static class ArgumentProvider<S> {
		
		Object[] constructorArguments;
		Class<S> klass;
		
		public ArgumentProvider(Object[] constructorArgs, Class<S> klass) {
			this.constructorArguments = constructorArgs;
			this.klass = klass;
		}
		
		protected S get(ResultRow resultRow){
			Class<?>[] constructorSignature = new Class[this.constructorArguments.length];
			Object[] constructorArgs = new Object[this.constructorArguments.length];
			for (int i = 0; i < this.constructorArguments.length; i++) {
				if (ColumnReference.class.isInstance(this.constructorArguments[i])) {
					Object referencedValue = resultRow.get(((ColumnReference) this.constructorArguments[i]).i.intValue());
					constructorSignature[i] = referencedValue.getClass();
					constructorArgs[i] = referencedValue;
				} else {
					constructorSignature[i] = this.constructorArguments[i].getClass();
					constructorArgs[i] = this.constructorArguments[i];
				}
			}
			try {
				Constructor<S> constructor = this.klass.getConstructor(constructorSignature);
				return constructor.newInstance(constructorArgs);
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
	}
	
 }
