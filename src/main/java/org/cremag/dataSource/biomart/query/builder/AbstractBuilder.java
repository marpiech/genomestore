package org.cremag.dataSource.biomart.query.builder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.cremag.utils.StringUtils;


abstract class AbstractBuilder {
	
	protected String tag;
	protected HashMap<String, String> attributes = new HashMap<String, String>();
	protected String body;
	
	protected AbstractBuilder() {};

	abstract protected Boolean validate();
	
	// TODO: Implement validations check
	public String render(){
		if (this.body != null) {
			return StringUtils.renderXMLElement(this.tag, this.renderAttributes(), this.body);
		} else {
			return StringUtils.renderXMLElement(this.tag, this.renderAttributes());
		}
	}
	
	public String render(AbstractBuilder abstractBuilder) {
		this.setBody(abstractBuilder.render());
		return this.render();
	}
	
	public String render(String flesh) {
		this.setBody(flesh);
		return this.render();
	}
	
	public AbstractBuilder setAttribute(String name, String value) {
		this.attributes.put(name, value);
		return this;
	}

	public String getAttribute(String name) {
		return this.attributes.get(name);
	}
	
	public AbstractBuilder setBody(String value) {
		this.body = value;
		return this;
	}
	
	// TODO: Implement validations check
	private String renderAttributes() {
		ArrayList<String> fleshBites = new ArrayList<String>();
		Set<Map.Entry<String,String>> attributesEntrySet = this.attributes.entrySet();
		Iterator<Map.Entry<String,String>> attributesEntrySetIterator = attributesEntrySet.iterator();
		while (attributesEntrySetIterator.hasNext()) {
			Map.Entry<String,String> attributesEntry = attributesEntrySetIterator.next();
			fleshBites.add(StringUtils.renderXMLAttribute(attributesEntry.getKey(), attributesEntry.getValue()));
		}
		return StringUtils.join(fleshBites, " ");
	}

}
