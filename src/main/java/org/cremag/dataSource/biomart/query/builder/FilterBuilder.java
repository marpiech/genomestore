package org.cremag.dataSource.biomart.query.builder;

public class FilterBuilder extends AbstractBuilder {

	public static final FilterBuilder HGNCSymbolFilterBuilder = new FilterBuilder().setName("hgnc_symbol");
	public static final FilterBuilder MGIIDFilterBuilder = new FilterBuilder().setName("mgi_symbol");

	public FilterBuilder() {
		this.tag = "Filter";
	}

	public FilterBuilder setName(String name) {
		return (FilterBuilder) this.setAttribute("name", name);
	}

	public FilterBuilder setValue(String name) {
		return (FilterBuilder) this.setAttribute("value", name);
	}
	
	// TODO: Implement proper validation
	protected Boolean validate() {
		return Boolean.TRUE;
	}

}
