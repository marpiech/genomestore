package org.cremag.dataSource.biomart.query.builder;

public class QueryBuilder extends AbstractBuilder {

	public static final QueryBuilder TSVProcessorQueryBuilder = new QueryBuilder().setClient("seQinspector").setProcessor("TSV").setLimit("-1").setHeader("1");

	public QueryBuilder() {
		this.tag = "Query";
	}

	public QueryBuilder setClient(String client) {
		return (QueryBuilder) this.setAttribute("client", client);
	}

	public QueryBuilder setProcessor(String processor) {
		return (QueryBuilder) this.setAttribute("processor", processor);
	}

	public QueryBuilder setLimit(String limit) {
		return (QueryBuilder) this.setAttribute("limit", limit);
	}

	public QueryBuilder setHeader(String header) {
		return (QueryBuilder) this.setAttribute("header", header);
	}

	// TODO: Implement proper validation
	protected Boolean validate() {
		return Boolean.TRUE;
	}

}
