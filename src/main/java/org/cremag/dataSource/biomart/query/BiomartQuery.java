package org.cremag.dataSource.biomart.query;

import org.cremag.dataSource.biomart.query.builder.AttributeBuilder;
import org.cremag.dataSource.biomart.query.builder.DatasetBuilder;
import org.cremag.dataSource.biomart.query.builder.FilterBuilder;
import org.cremag.dataSource.biomart.query.builder.QueryBuilder;
import org.cremag.dataSource.biomart.query.result.Result;
import org.springframework.web.client.RestTemplate;
import java.util.ArrayList;
import java.util.Iterator;


public class BiomartQuery {

	private static final String header = new String("<!DOCTYPE Query>");
	private String martserviceURL;
	private QueryBuilder queryBuilder = QueryBuilder.TSVProcessorQueryBuilder;
	private DatasetBuilder datasetBuilder;
	private ArrayList<FilterBuilder> filterBuilderList = new ArrayList<FilterBuilder>();
	private ArrayList<AttributeBuilder> attributeBuilderList = new ArrayList<AttributeBuilder>();
	
	public BiomartQuery(String martserviceURL) {
		this.martserviceURL = martserviceURL;
	}

	public BiomartQuery setQuery(QueryBuilder queryBuilder) {
		this.queryBuilder = queryBuilder;
		return this;	
	}
	
	public BiomartQuery setDataset(DatasetBuilder datasetBuilder) {
		this.datasetBuilder = datasetBuilder;
		return this;
	}
	
	public BiomartQuery setDataset(String name, String config) {
		return this.setDataset(new DatasetBuilder().setName(name).setConfig(config));
	}
	
	public BiomartQuery addFilter(FilterBuilder filterBuilder) {
		this.filterBuilderList.add(filterBuilder);
		return this;
	}
	
	public BiomartQuery addFilter(String name, String value) {
		return this.addFilter(new FilterBuilder().setName(name).setValue(value));
	}

	public BiomartQuery clearFilters() {
		this.filterBuilderList.clear();
		return this;
	}
		
	public BiomartQuery addAttribute(AttributeBuilder attributeBuilder) {
		this.attributeBuilderList.add(attributeBuilder);
		return this;
	}
	
	public BiomartQuery addAttribute(String name) {
		return this.addAttribute(new AttributeBuilder().setName(name));
	}
	
	public BiomartQuery clearAttributes() {
		this.attributeBuilderList.clear();
		return this;
	}

	public Result execute() {
		RestTemplate restTemplate = new RestTemplate();
		String restTemplateResult = restTemplate.getForObject(this.getURL(), String.class);
		return new Result(restTemplateResult, this.queryBuilder.getAttribute("processor"));
	}
	
	public String getURL() {
		return this.martserviceURL + "?query=" + this.getXML();
	}
	
	public String getXML() {
		StringBuilder fleshBuilder = new StringBuilder();
		Iterator<FilterBuilder> filterBuilderIterator = this.filterBuilderList.iterator();
		while (filterBuilderIterator.hasNext()) { fleshBuilder.append(filterBuilderIterator.next().render()); }
		Iterator<AttributeBuilder> attributeBuilderIterator = this.attributeBuilderList.iterator();
		while (attributeBuilderIterator.hasNext()) { fleshBuilder.append(attributeBuilderIterator.next().render()); }
		return BiomartQuery.header + this.queryBuilder.render(this.datasetBuilder.render(fleshBuilder.toString()));
	}
	
}
