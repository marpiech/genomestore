package org.cremag.dataSource.biomart.query.builder;

public class DatasetBuilder extends AbstractBuilder {

	public static final DatasetBuilder HomoSapiensGenesDatasetBuilder = new DatasetBuilder().setName("hsapiens_gene_ensembl").setConfig("gene_ensembl_config");
	public static final DatasetBuilder MusMusculusGenesDatasetBuilder = new DatasetBuilder().setName("mmusculus_gene_ensembl").setConfig("gene_ensembl_config");
	
	public DatasetBuilder() {
		this.tag = "Dataset";
	}
	
	public DatasetBuilder setName(String name) {
		return (DatasetBuilder) this.setAttribute("name", name);
	}
	
	public DatasetBuilder setConfig(String config) {
		return (DatasetBuilder) this.setAttribute("config", config);
	}
	
	// TODO: Implement proper validation
	protected Boolean validate() {
		return Boolean.TRUE;
	}
	
}
