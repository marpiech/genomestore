package org.cremag.dataSource.biomart.query.result;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


public class Result extends ArrayList<ResultRow> {

	private static final long serialVersionUID = 1L;
	private List<String> columns;
	
	
	public Result(String rawResult, String processorName) {
		if (processorName.equals("TSV")) {
			this.parseTSV(rawResult);
		}
	}
	
	public List<String> getColumns(){
		return this.columns;
	}
		
	private void parseTSV(String rawResult) {
		String[] rawResultRows = rawResult.split("\n");
		Iterator<String> iter = Arrays.asList(rawResultRows).iterator();
		this.columns = Arrays.asList(iter.next().split("\t"));
		while (iter.hasNext()) {
			this.add(new ResultRow(Arrays.asList(iter.next().split("\t"))));
		}
	}
	
}
