package org.cremag.dataSource.biomart;


import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.cremag.dataSource.biomart.query.BiomartQuery;
import org.cremag.dataSource.biomart.query.BiomartQueryFactory;
import org.cremag.genomic.Position;
import org.springframework.web.client.RestTemplate;

public class BiomartDAO {
	
	private static final Logger logger = Logger.getLogger(BiomartDAO.class);	
	
	private String url = "http://central.biomart.org/biomart/martservice";
	// http://www.ensembl.org/biomart/martservice
	private static String paramName = "?query=";
	private static String docType = "%3C!DOCTYPE%20Query%3E";
	private static String header = "%3CQuery%20client=%22true%22%20processor=%22TSV%22%20limit=%22-1%22%20header=%221%22%3E"; 
	private static String chromosomeAttribute = "%3CAttribute%20name=%22chromosome_name%22/%3E";
	private static String strandAttribute = "%3CAttribute%20name=%22strand%22/%3E";
	private static String startAttribute = "%3CAttribute%20name=%22transcript_start%22/%3E";
	private static String endAttribute = "%3CAttribute%20name=%22transcript_end%22/%3E";
	private static String footer = "%3C/Dataset%3E%3C/Query%3E";
	
	public BiomartDAO() {
		//this.url = martserviceURL;
		logger.debug("BiomartDAO constructor");
	}

	private String getDatasetForURI(String dataset) {
		return "%3CDataset%20name=%22" + dataset + "%22%20config=%22gene_ensembl_config%22%3E";
	}

	private String getFilter(String filterName, String filterValue) {
		return "%3CFilter%20name=%22" + filterName + "%22%20value=%22" + filterValue + "%22/%3E";
	}
	
	public Set<Position> getListOfTranscriptionStartSitesForGene(String gene, String genome) throws URISyntaxException {
		Set<Position> tss = new TreeSet<Position>();
		logger.info("getListOfTSSForGeneNameAndGenome called");
		
		// matching dataset to genome name
		String dataset = "hsapiens_gene_ensembl";
		this.url = "http://www.ensembl.org/biomart/martservice";
		if (genome.equals("mm9")) {
			this.url = "http://may2012.archive.ensembl.org/biomart/martservice";
			dataset = "mmusculus_gene_ensembl";
		}
		if (genome.equals("mm10")) {
			dataset = "mmusculus_gene_ensembl";
		}
		
		logger.trace("Biomart for Genome: " + genome + " " + genome.getClass() + " Dataset: " + dataset + " url: " + this.url);
		
		@SuppressWarnings("static-access")
		String query = this.docType + this.header + this.getDatasetForURI(dataset) +
				this.getFilter("wikigene_name", gene) + this.chromosomeAttribute +
				this.strandAttribute + this.startAttribute + this.endAttribute + this.footer;
		logger.trace("Tss Query:" + query);
		RestTemplate restTemplate = new RestTemplate();
		
		@SuppressWarnings("static-access")
		URI uri = new URI(this.url + this.paramName + query);
		String restTemplateResult = restTemplate.getForObject(uri, String.class);
		String[] rawResultRows = restTemplateResult.split("\n");
		for(String rawResultRow : rawResultRows) {
			String[] rawResult = rawResultRow.split("\t");
			try {
				if(rawResult[1].equals("1"))
					tss.add(new Position("chr" + rawResult[0], new Integer(rawResult[2]), rawResult[1]));
				else
					tss.add(new Position("chr" + rawResult[0], new Integer(rawResult[3]), rawResult[1]));
			} catch(Exception e) {
				logger.warn("Could not parse biomart response. Probably first line: " + rawResultRow);
			}
		}
		return tss;
	}
}