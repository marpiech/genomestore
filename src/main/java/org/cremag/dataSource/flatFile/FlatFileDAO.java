package org.cremag.dataSource.flatFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class FlatFileDAO {
	
	List <String> mgiSymbols;
	
	public FlatFileDAO() throws IOException {
		mgiSymbols = new LinkedList<String>();
		BufferedReader reader = new BufferedReader(
				new FileReader(
						new File("src/main/resources/mm10symbols.txt")));
		String line;
		while((line = reader.readLine()) != null) {
			mgiSymbols.add(line);
		}
		reader.close();
	}
	
	public Set<String> getGeneSymbols(String firstLetters, int max) {
		Set<String> out = new TreeSet<String>();
		int counter = 0;
		for(String symbol : mgiSymbols) {
			if(symbol.toLowerCase().startsWith(firstLetters.toLowerCase())) {
				out.add(symbol);
				if(++counter == max)
					return out;
			}
		}
		return out;
	}
}
