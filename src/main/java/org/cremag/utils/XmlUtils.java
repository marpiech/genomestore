/*
 * Copyright (C) 2012 - Marcin Piechota
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 Unported (CC BY-NC 3.0)
 * You may obtain a copy of the License at http://creativecommons.org/licenses/by-nc/3.0/legalcode
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package org.cremag.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.cremag.utils.file.genomic.BigWig;
import org.w3c.dom.Document;

/**
 * @author marpiech
 * this class contains some static methods useful for xml management
 */
public class XmlUtils {

	private static final Logger logger = Logger
			.getLogger(XmlUtils.class);
	
	private static final String xmlForbiddenPattern = "[^" 
            + "\u0001-\uD7FF" 
            + "\uE000-\uFFFD"
            + "\ud800\udc00-\udbff\udfff" 
            + "]+";
	
	public static String documentToString(Document doc) {
		try {
			
			/* using transformer to create xml document*/
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer;
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			DOMSource source = new DOMSource(doc);
				
			/* writing document to string */
			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			transformer.transform(source, result);
			String xmlString = sw.toString();
			
			return xmlString;
				
		} catch (Exception e) {
			logger.error(e.getMessage());
			return e.getMessage();
		}
	}
	
	public static void saveDocumentToFile(String path, Document doc) throws IOException {
		
		/* setting BufferedWriter from schemaFile */
		BufferedWriter writer = new BufferedWriter(new FileWriter(new File(path)));
		writer.write(XmlUtils.documentToString(doc));
		writer.close();
		
	}
	
	
	/**
	 * This method ensures that the output String has only
	 * valid XML unicode characters as specified by the
	 * XML 1.0 standard. For reference, please see
	 * <a href="http://www.w3.org/TR/2000/REC-xml-20001006#NT-Char">the
	 * standard</a>. This method will return an empty
	 * String if the input is null or empty.
	 *
	 * @param in The String whose non-valid characters we want to remove.
	 * @return The in String, stripped of non-valid characters.
	 */
	public static String stripNonValidXMLCharacters(String in) {
		return in.replaceAll(xmlForbiddenPattern, "_");
	}
	
}
