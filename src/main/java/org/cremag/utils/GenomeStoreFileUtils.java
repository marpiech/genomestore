/*
 * Copyright (C) 2012 - Marcin Piechota
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 Unported (CC BY-NC 3.0)
 * You may obtain a copy of the License at http://creativecommons.org/licenses/by-nc/3.0/legalcode
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package org.cremag.utils;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author marpiech
 * this class contains some static methods usefull for file management jobs
 */
public class GenomeStoreFileUtils {
	
	public static String getFileExtension (String path) {
		try {
			String extension = path.substring(path.lastIndexOf(".") + 1);
			return extension;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String getFileName (String path) {
		try {
			String extension = path.substring(path.lastIndexOf("/") + 1);
			return extension;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public static String getFileType (String path) {
		String extension = GenomeStoreFileUtils.getFileExtension(path).toLowerCase();
		if(extension.equals("bigwig")) return "bigwig";
		if(extension.equals("bw")) return "bigwig";
		if(extension.equals("bigbed")) return "bigbed";
		if(extension.equals("bb")) return "bigbed";
		if(extension.equals("bed")) return "bed";
		return "txt";
	}
	
}
