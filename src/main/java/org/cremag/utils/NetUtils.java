/*
 * Copyright (C) 2012 - Marcin Piechota
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 Unported (CC BY-NC 3.0)
 * You may obtain a copy of the License at http://creativecommons.org/licenses/by-nc/3.0/legalcode
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package org.cremag.utils;

import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

/**
 * @author marpiech
 * this class contains some static methods usefull for network jobs
 */
public class NetUtils {

	/**
	 * @param URLName
	 * @return boolean if URL resource exists
	 */
	public static boolean exists(String URLName) {
		try {
			HttpURLConnection.setFollowRedirects(false);
			HttpURLConnection con = (HttpURLConnection) new URL(URLName)
					.openConnection();
			con.setRequestMethod("HEAD");
			return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * @param request
	 * @return client ip even when the client is behind apache proxypass
	 */
	public static String getIP(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");
		if (ip != null) return ip;
		return request.getRemoteAddr();
	}
}
