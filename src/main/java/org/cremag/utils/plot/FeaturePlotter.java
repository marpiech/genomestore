package org.cremag.utils.plot;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.List;

public class FeaturePlotter extends Plotter {
	
	public void plotFeatures(Graphics2D image, List <Feature> features, int verticalPosition, int margin) {
		setVerticalPosition(verticalPosition);
		setMargin(margin);
		plotFeatures(image, features);
	}
	
	public void plotFeatures(Graphics2D image, List <Feature> features) {
		setWidth(image);
		image.setPaint(Color.BLACK);
		for(Feature feature : features) {
			int start = (int) (((float) WIDTH) * feature.getRelativeStart());
			int end = (int) (((float) WIDTH) * feature.getRelativeEnd());
			int featureWidth = end - start;
			if (featureWidth < 1)
				featureWidth = 1;
			int heightStart;
			if(feature.isCenter())
				heightStart = VERTICAL_POSITION - (int) ((feature.getHeight() - 1) / 2);
			else
				heightStart = VERTICAL_POSITION - (int) feature.getHeight();
			image.fillRect(MARGIN + start, heightStart, featureWidth, (int) feature.getHeight());
		}
	}
}
