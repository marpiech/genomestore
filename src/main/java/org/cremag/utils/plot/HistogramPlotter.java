package org.cremag.utils.plot;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class HistogramPlotter extends Plotter {
	int MIN_VALUE = 10;
	public void plotFeatures(Graphics2D image, Map <Float, Float> data, int verticalPosition, int margin, int height, Color color) {
		
		
		Float max = (float) MIN_VALUE; // set max value
		for(Float val : data.values())
			if (val > max)
				max = val;
		
		plotFeatures(image, data, verticalPosition, margin, height, color, max);

	}
	
	public void plotFeatures(Graphics2D image, Map <Float, Float> data, int verticalPosition, int margin, int height, Color color, float max) {
		image.setPaint(color);

		setVerticalPosition(verticalPosition); // set Plotter fields
		setMargin(margin);
		setWidth(image);
		
		int THICKNESS = 2;
		
		for(Entry <Float, Float> entry : data.entrySet()) {
			int start = (int) (((float) WIDTH) * entry.getKey());
			int breakLineWidth = THICKNESS;
			int breakLineHeight = (int) (((float) height) * (entry.getValue() / max));
			int heightStart = VERTICAL_POSITION + height - breakLineHeight;
			image.fillRect(MARGIN + start, heightStart, breakLineWidth, breakLineHeight);
		}		
	}
}
