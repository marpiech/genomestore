package org.cremag.utils.plot;

import java.awt.Graphics2D;
import java.awt.Rectangle;

public class Plotter {
	int MARGIN = 20;
	int VERTICAL_POSITION = 20;
	int HEIGHT = 20;
	int WIDTH = 100;
	Graphics2D image;
	
	public void setVerticalPosition(int height) {
		this.VERTICAL_POSITION = height;
	}
	public void setMargin (int margin) {
		this.MARGIN = margin;
	}
	public void setHeight (int height) {
		this.HEIGHT = height;
	}
	public void setWidth (int width) {
		this.WIDTH = width;
	}
	public void setWidth (Graphics2D image) {
		try {
			Rectangle imageBorders = image.getDeviceConfiguration().getBounds();
			int width = (int) imageBorders.getWidth() - 2 * MARGIN;
			this.WIDTH = width;
		} catch (Exception e) {
			this.WIDTH = 800;
		}
	}
}
