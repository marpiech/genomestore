package org.cremag.utils.plot;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class GraphPlotter {
	int HORIZONTAL_POSITION = 0;
	int VERTICAL_POSITION = 0;
	int HEIGHT = 50;
	int WIDTH = 50;
	Graphics2D image;
	
	public GraphPlotter(Graphics2D image, int horizontalPosition, int width, int verticalPosition, int height) {
		this.image = image;
		setHorizontalPosition(horizontalPosition);
		setWidth(width);
		setVerticalPosition(verticalPosition);
		setHeight(height);
	}
	
	public void plotBackground(Color color) {
		image.setPaint(color);
		image.fillRect(HORIZONTAL_POSITION, VERTICAL_POSITION, WIDTH, HEIGHT);
	}
	
	public void plotLinedBackground(Color color, int breaks, Color breakColor) {
		image.setPaint(color);
		image.fillRect(HORIZONTAL_POSITION, VERTICAL_POSITION, WIDTH, HEIGHT);
		float breakSize = ((float) WIDTH) / ((float) breaks);
		image.setPaint(breakColor);
		for(int i = 0; i < breaks; i++) {
			int position = (int) (i * breakSize);
			image.drawLine(HORIZONTAL_POSITION + position, VERTICAL_POSITION, 
					HORIZONTAL_POSITION + position, VERTICAL_POSITION + HEIGHT);
		}
	}
	
	
	
	public void setVerticalPosition(int height) {
		this.VERTICAL_POSITION = height;
	}
	public void setHorizontalPosition (int horizontalPosition) {
		this.HORIZONTAL_POSITION = horizontalPosition;
	}
	public void setHeight (int height) {
		this.HEIGHT = height;
	}
	public void setWidth (int width) {
		this.WIDTH = width;
	}
	
}
