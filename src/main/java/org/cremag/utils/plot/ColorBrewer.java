package org.cremag.utils.plot;

import java.awt.BasicStroke;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorBrewer {
	final static BasicStroke thinStroke = new BasicStroke(0.5f);
	final static BasicStroke stroke = new BasicStroke(1.0f);
    final static BasicStroke wideStroke = new BasicStroke(1.5f);
	
	public final static Color WHITE = Color.WHITE;
	public final static Color LIGHT_GREY = new Color(235,235,235);
	public final static Color GREY_90 = new Color(25,25,25);
	public final static Color GREY_80 = new Color(50,50,50);
	
	public final static Color PLOT_RED = new Color(227,26,28);
	public final static Color PLOT_BLUE = new Color(55,126,184);
	public final static Color PLOT_GREEN = new Color(77,175,74);
	public final static Color PLOT_VIOLET = new Color(147,81,164);
	public final static Color PLOT_ORANGE = new Color(255,127,0);
	public final static Color PLOT_YELLOW = new Color(255,255,51);
	public final static Color PLOT_BROWN = new Color(169,87,40);
	public final static Color PLOT_PINK = new Color(247,104,161);
	
	public final static Color PLOT_LIGHT_GREEN = new Color(107,205,104);
	public final static Color PLOT_LIGHT_ORANGE = new Color(255,157,30);
	public final static Color PLOT_LIGHT_BLUE = new Color(85,156,214);
	
	public final static Color PALLETTE_WEDDING_CURRY = new Color(231, 182, 53);
	public final static Color PALLETTE_WEDDING_CHARTREUSE = new Color(202,195,89);
	public final static Color PALLETTE_WEDDING_LEAF = new Color(141, 180, 123);
	public final static Color PALLETTE_WEDDING_MOSS = new Color(121, 132, 54);
	public final static Color PALLETTE_WEDDING_SAGE = new Color(209, 218, 175);
	public final static Color PALLETTE_WEDDING_POPPY = new Color(248, 149, 66);
	public final static Color PALLETTE_WEDDING_PAPAYA = new Color(240, 104, 78);
	public final static Color PALLETTE_WEDDING_STRAWBERRY = new Color(232, 61, 95);
	public final static Color PALLETTE_WEDDING_RHUBARB = new Color(179, 62, 84);
	public final static Color PALLETTE_WEDDING_RED = new Color(205, 0, 35);
	public final static Color PALLETTE_WEDDING_FUCHSIA = new Color(211, 0, 109);
	public final static Color PALLETTE_WEDDING_BEET = new Color(127, 51, 116);
	public final static Color PALLETTE_WEDDING_BLOSSOM = new Color(244, 178, 182);
	public final static Color PALLETTE_WEDDING_PLUM = new Color(190, 176, 202);
	public final static Color PALLETTE_WEDDING_VIOLET = new Color(139, 154, 197);
	public final static Color PALLETTE_WEDDING_GRAPE = new Color(85, 73, 145);
	public final static Color PALLETTE_WEDDING_NIGHT = new Color(0, 26, 49);
	public final static Color PALLETTE_WEDDING_LAKE = new Color(0, 109, 138);
	public final static Color PALLETTE_WEDDING_POOL = new Color(130, 198, 185);
	public final static Color PALLETTE_WEDDING_BLUEBELL = new Color(145, 195, 220);
	public final static Color PALLETTE_WEDDING_CHOCOLATTE = new Color(56, 29, 34);
	public final static Color PALLETTE_WEDDING_KHAKI = new Color(187, 163, 129);
	public final static Color PALLETTE_WEDDING_GRAVEL = new Color(156, 157, 139);
	public final static Color PALLETTE_WEDDING_CEMENT = new Color(227, 219, 200);
	public final static Color PALLETTE_WEDDING_BLACK = new Color(20, 20, 20);

	public static List <Color> getBlues(int n) {
		List <Color> set = new ArrayList <Color> ();
		set.add(Color.decode("#F7FBFF"));
		set.add(Color.decode("#DEEBF7"));
		set.add(Color.decode("#C6DBEF"));
		set.add(Color.decode("#9ECAE1"));
		set.add(Color.decode("#6BAED6"));
		set.add(Color.decode("#4292C6"));
		set.add(Color.decode("#2171B5"));
		set.add(Color.decode("#08519C"));
		set.add(Color.decode("#08306B"));
		return set;
	}
	
	public static List <Color> getDChipColors(int n) {
		List <Color> set = new ArrayList <Color> ();
		set.add(Color.decode("#0000FF"));
		set.add(Color.decode("#3838FF"));
		set.add(Color.decode("#7171FF"));
		set.add(Color.decode("#AAAAFF"));
		set.add(Color.decode("#E2E2FF"));
		set.add(Color.decode("#FFE2E2"));
		set.add(Color.decode("#FFAAAA"));
		set.add(Color.decode("#FF7171"));
		set.add(Color.decode("#FF3838"));      
		set.add(Color.decode("#FF0000"));
		return set;
	}
	
	public static List <Color> getGraphPalette() {
		List <Color> set = new ArrayList <Color> ();
		set.add(ColorBrewer.PLOT_RED);
		set.add(ColorBrewer.PLOT_BLUE);
		set.add(ColorBrewer.PLOT_GREEN);
		set.add(ColorBrewer.PLOT_VIOLET);
		set.add(ColorBrewer.PLOT_ORANGE);
		set.add(ColorBrewer.PLOT_YELLOW);
		set.add(ColorBrewer.PLOT_BROWN);
		set.add(ColorBrewer.PLOT_PINK);
		return set;
	}
	
}
