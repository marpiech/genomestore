package org.cremag.utils.plot;

import java.awt.Color;

import org.cremag.genomic.BedItem;
import org.cremag.genomic.Position;

public class PlotProperties {
	int x;
	int y;
	int width;
	int height;
	Color color = ColorBrewer.PLOT_GREEN;
	int colorIndex = -1;
	String legend = "";
	
	public PlotProperties() {
		this.setX(20);
		this.setY(20);
		this.setWidth(600);
		this.setHeight(100);
		this.setColor(ColorBrewer.GREY_80);
	}
	
	public int convertHorizontal(Position pos, BedItem range) {
		int horiz = x + (int) ((double) width * ((((double) (pos.getPosition() - range.getStart())) / ((double) range.getLength())))); 
		return horiz;
	}
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public Color getColor() {
		return color;
	}
	public void setColor(Color color) {
		this.color = color;
	}
	public void changeColor() {
		this.colorIndex++;
		if(this.colorIndex > 7)
			colorIndex = 0;
		switch(colorIndex) {
		case 0: this.color = ColorBrewer.PLOT_RED; break;
		case 1: this.color = ColorBrewer.PLOT_BLUE; break;
		case 2: this.color = ColorBrewer.PLOT_GREEN; break;
		case 3: this.color = ColorBrewer.PLOT_VIOLET; break;
		case 4: this.color = ColorBrewer.PLOT_ORANGE; break;
		case 5: this.color = ColorBrewer.PALLETTE_WEDDING_CURRY; break;
		case 6: this.color = ColorBrewer.PLOT_BROWN; break;
		case 7: this.color = ColorBrewer.PALLETTE_WEDDING_FUCHSIA; break;
		default: this.color = ColorBrewer.GREY_80;
		}
	}
	public String getLegend() {
		return legend;
	}
	public void setLegend(String legend) {
		this.legend = legend;
	}
	
}
