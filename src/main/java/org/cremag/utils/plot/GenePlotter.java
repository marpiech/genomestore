package org.cremag.utils.plot;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.cremag.genomic.BedItem;
import org.cremag.genomic.Position;
import org.cremag.utils.file.BamStock;
import org.cremag.utils.genome.Gene;
import org.cremag.utils.genome.Transcript;



public class GenePlotter {
	private static final Logger logger = Logger.getLogger(GenePlotter.class);
	private int MARGIN = 30;
	private int EXON_HEIGHT = 3;
	private int CDS_HEIGHT = 7;
	private int TRANSCRIPT_HEIGHT = CDS_HEIGHT + 1;
	private int PANEL_HEIGHT = 30;
	private int BREAK_BETWEEN_PANELS = 10;
	
	Gene gene;
	SortedSet <Position> shrinkedBreakPoints = new TreeSet <Position> ();
	
	public GenePlotter(Gene gene) throws Exception {
		logger.trace("GenePlotter constructor()");
		this.gene = gene;
		this.calculateShrinkedPositions(0.5f);
	}
	
	public void plot(int width, int height, BamStock bamStock, Graphics2D image) {

		// White QuerySet
		new BackgroundPlotter().plotBackground(image, 0, height, 0, ColorBrewer.WHITE);
		
		// Gene Panel
		int vPosition = MARGIN;
		new BackgroundPlotter().plotLinedBackground(image, vPosition, PANEL_HEIGHT, MARGIN, ColorBrewer.LIGHT_GREY, 10, ColorBrewer.WHITE);
		new FeaturePlotter().plotFeatures(image, getGenePlotFeatures(), MARGIN + PANEL_HEIGHT / 2, MARGIN);
		
		// Get Coverage
		Map <String, Map <Integer, Float>> coverageData = bamStock.getHistogram(gene.getRange(), 10);
		Map <String, List <Map <Integer, Float>>> samplesData = bamStock.getSamples(gene.getRange(), 10);
		float max = this.getMaxFromCoverageList(coverageData.values());
		for(List <Map <Integer, Float>> samplesList : samplesData.values()) {
			float newmax = this.getMaxFromCoverageList(samplesList);
			if (newmax > max) max = newmax; 
		}
		logger.info("MAX: " + max);
//		// Gene expression
//		vPosition += PANEL_HEIGHT + BREAK_BETWEEN_PANELS;
//		new BackgroundPlotter().plotLinedBackground(image, vPosition, PANEL_HEIGHT, MARGIN, ColorBrewer.LIGHT_GREY, 10, ColorBrewer.WHITE);
//		new HistogramPlotter().plotFeatures(image, 
//				prepareHistogram(coverageData.get("Control"), false), 
//				vPosition, MARGIN, PANEL_HEIGHT, Color.ORANGE);
				
		// ExtendedExons Gene Panel
		vPosition += PANEL_HEIGHT + BREAK_BETWEEN_PANELS * 2;
		new BackgroundPlotter().plotLinedBackground(image, vPosition, PANEL_HEIGHT, MARGIN, ColorBrewer.LIGHT_GREY, 10, ColorBrewer.WHITE);
		new FeaturePlotter().plotFeatures(image, getGenePlotFeatures(true), vPosition + PANEL_HEIGHT / 2, MARGIN);
		
		// Extended gene expression
		vPosition += BREAK_BETWEEN_PANELS;
		vPosition -= PANEL_HEIGHT;
		for(String groupName : coverageData.keySet()) {
			vPosition += PANEL_HEIGHT * 2 + BREAK_BETWEEN_PANELS;
			new BackgroundPlotter().plotLinedBackground(image, vPosition, PANEL_HEIGHT * 2, MARGIN, ColorBrewer.LIGHT_GREY, 10, ColorBrewer.WHITE);
			
			Color color = Color.ORANGE;
			logger.trace("Group: " + groupName);
			if(groupName.equals("Control"))
				color = ColorBrewer.PLOT_LIGHT_BLUE;
			if(groupName.equals("Mianserine"))
				color = ColorBrewer.PLOT_LIGHT_GREEN;
			if(groupName.equals("Tranylcypromine"))
				color = ColorBrewer.PLOT_LIGHT_ORANGE;
			image.setStroke(ColorBrewer.thinStroke);
			for(Map<Integer, Float> sample : samplesData.get(groupName)) {
				new LinePlotter().plotFeatures(image, 
						prepareHistogram(sample, true), 
						vPosition, MARGIN, PANEL_HEIGHT * 2, color, max);				
			}
			image.setStroke(ColorBrewer.wideStroke);
			if(groupName.equals("Control"))
				color = ColorBrewer.PLOT_BLUE;
			if(groupName.equals("Mianserine"))
				color = ColorBrewer.PLOT_GREEN;
			if(groupName.equals("Tranylcypromine"))
				color = ColorBrewer.PLOT_ORANGE;
			new LinePlotter().plotFeatures(image, 
					prepareHistogram(coverageData.get(groupName), true), 
					vPosition, MARGIN, PANEL_HEIGHT * 2, color, max);

			image.setStroke(ColorBrewer.stroke);
		}
		

				
		// TransciptsPanel
		vPosition += PANEL_HEIGHT * 2 + BREAK_BETWEEN_PANELS * 2;
		int TRANSCRIPT_PANEL_HEIGHT = TRANSCRIPT_HEIGHT * this.gene.getNumberOfTtanscripts();
		logger.trace("TranscriptPlotter");
		logger.trace("Number of transcripts: " + this.gene.getNumberOfTtanscripts());
		new BackgroundPlotter().plotLinedBackground(image, vPosition, TRANSCRIPT_PANEL_HEIGHT, MARGIN, ColorBrewer.LIGHT_GREY, 10, ColorBrewer.WHITE);
		for(List<Feature> features : getTranscriptPlotFeatures()) {
			new FeaturePlotter().plotFeatures(image, features, vPosition + TRANSCRIPT_HEIGHT / 2, MARGIN);
			vPosition += TRANSCRIPT_HEIGHT;
		}

	}
	public BufferedImage plot(int width, int height, BamStock bamStock) {
		BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D image = bufferedImage.createGraphics();
		
		// White QuerySet
		new BackgroundPlotter().plotBackground(image, 0, height, 0, ColorBrewer.WHITE);
		
		// Gene Panel
		int vPosition = MARGIN;
		new BackgroundPlotter().plotLinedBackground(image, vPosition, PANEL_HEIGHT, MARGIN, ColorBrewer.LIGHT_GREY, 10, ColorBrewer.WHITE);
		new FeaturePlotter().plotFeatures(image, getGenePlotFeatures(), MARGIN + PANEL_HEIGHT / 2, MARGIN);
		
		Map <String, Map <Integer, Float>> coverageData = bamStock.getHistogram(gene.getRange(), 10);
		float max = this.getMaxFromCoverageList(coverageData.values());
//		// Gene expression
//		vPosition += PANEL_HEIGHT + BREAK_BETWEEN_PANELS;
//		new BackgroundPlotter().plotLinedBackground(image, vPosition, PANEL_HEIGHT, MARGIN, ColorBrewer.LIGHT_GREY, 10, ColorBrewer.WHITE);
//		new HistogramPlotter().plotFeatures(image, 
//				prepareHistogram(coverageData.get("Control"), false), 
//				vPosition, MARGIN, PANEL_HEIGHT, Color.ORANGE);
				
		// ExtendedExons Gene Panel
		vPosition += PANEL_HEIGHT + BREAK_BETWEEN_PANELS * 3;
		new BackgroundPlotter().plotLinedBackground(image, vPosition, PANEL_HEIGHT, MARGIN, ColorBrewer.LIGHT_GREY, 10, ColorBrewer.WHITE);
		new FeaturePlotter().plotFeatures(image, getGenePlotFeatures(true), vPosition + PANEL_HEIGHT / 2, MARGIN);
		
		// Extended gene expression
		vPosition += BREAK_BETWEEN_PANELS;
		for(String groupName : coverageData.keySet()) {
			vPosition += PANEL_HEIGHT + BREAK_BETWEEN_PANELS;
			new BackgroundPlotter().plotLinedBackground(image, vPosition, PANEL_HEIGHT + BREAK_BETWEEN_PANELS, MARGIN, ColorBrewer.LIGHT_GREY, 10, ColorBrewer.WHITE);
			new HistogramPlotter().plotFeatures(image, 
					prepareHistogram(coverageData.get(groupName), true), 
					vPosition, MARGIN, PANEL_HEIGHT, Color.ORANGE, max);
		}
		

				
		// TransciptsPanel
		vPosition += PANEL_HEIGHT + BREAK_BETWEEN_PANELS;
		int TRANSCRIPT_PANEL_HEIGHT = TRANSCRIPT_HEIGHT * this.gene.getNumberOfTtanscripts();
		logger.trace("TranscriptPlotter");
		logger.trace("Number of transcripts: " + this.gene.getNumberOfTtanscripts());
		new BackgroundPlotter().plotLinedBackground(image, vPosition, TRANSCRIPT_PANEL_HEIGHT, MARGIN, ColorBrewer.LIGHT_GREY, 10, ColorBrewer.WHITE);
		for(List<Feature> features : getTranscriptPlotFeatures()) {
			new FeaturePlotter().plotFeatures(image, features, vPosition + TRANSCRIPT_HEIGHT / 2, MARGIN);
			vPosition += TRANSCRIPT_HEIGHT;
		}
		
		vPosition += BREAK_BETWEEN_PANELS;
		new BackgroundPlotter().plotLinedBackground(image, vPosition, TRANSCRIPT_PANEL_HEIGHT, MARGIN, ColorBrewer.LIGHT_GREY, 10, ColorBrewer.WHITE);
		new FeaturePlotter().plotFeatures(image, prepareSpecificityIndex(), vPosition + PANEL_HEIGHT, MARGIN);
		
		return bufferedImage;
	}

	private List<Feature> prepareSpecificityIndex() {
		List<Feature> features = new ArrayList <Feature>();
		int size = gene.getTranscripts().size();
		for(BedItem item : gene.getSpecificityIndex())
			features.add(new Feature(getShrinkedPosition(item.getStart()),
					getShrinkedPosition(item.getEnd()),
					(float) PANEL_HEIGHT * new Float(item.getScore()) / (float) size,
					false));
		return features;
	}
	
	private Map <Float, Float> prepareHistogram(Map <Integer, Float> coverage, boolean shrinked) {
		Map <Float, Float> histogram = new TreeMap <Float, Float> ();
		for(Entry <Integer, Float> entry : coverage.entrySet()) {
			Float key;
			if (shrinked)
				key = getShrinkedPosition(entry.getKey());
			else
				key = gene.getRelativePosition(entry.getKey());
			histogram.put(key, entry.getValue());
		}
		return histogram;
	}
	

	
 	private List<List<Feature>> getTranscriptPlotFeatures() {
		List <List<Feature>> transcriptFeatures = new ArrayList <List<Feature>> ();
		for(Transcript transcript : gene.getTranscripts()) {
			transcriptFeatures.add(getGenePlotFeatures(true, transcript));
		}
		return transcriptFeatures;
	}
	
	private List<Feature> getGenePlotFeatures() {
		return getGenePlotFeatures(false);
	}
	
	private List<Feature> getGenePlotFeatures(boolean shrinked) {
		return getGenePlotFeatures(shrinked, this.gene);
	}
	
	private List<Feature> getGenePlotFeatures(boolean shrinked, Gene geneOrTrancript) {
		List <BedItem> items = geneOrTrancript.getBedItems();
		List<Feature> features = new ArrayList <Feature>();
		float geneStart;
		if (shrinked)
			geneStart = getShrinkedPosition(geneOrTrancript.getRange().getStart());
		else
			geneStart = this.gene.getRelativePosition(geneOrTrancript.getRange().getStart());
		float geneEnd;
		if (shrinked)
			geneEnd = getShrinkedPosition(geneOrTrancript.getRange().getEnd());
		else
			geneEnd = this.gene.getRelativePosition(geneOrTrancript.getRange().getEnd());
		features.add(new Feature(geneStart, geneEnd, 1.0f, true));
		for(BedItem item : items) {
			float start;
			if (shrinked)
				start = getShrinkedPosition(item.getStart());
			else
				start = geneOrTrancript.getRelativePosition(item.getStart());
			float end;
			if (shrinked)
				end = getShrinkedPosition(item.getEnd());
			else
				end = geneOrTrancript.getRelativePosition(item.getEnd());
			
			if (item.getScore().equals("exon")) {
				features.add(new Feature(start, end, EXON_HEIGHT, true));
			}
			
			if (item.getScore().equals("CDS")) {
				features.add(new Feature(start, end, CDS_HEIGHT, true));
			}

		}
		return features;
	}
	
	private float getShrinkedPosition(int position) {
		Position startBorder = null;
		
		for(Position point : shrinkedBreakPoints) {
			if (position == point.getPosition())
				return point.getShrinkedPosition();
			if (point.getPosition() < position)
				startBorder = point;
			if (point.getPosition() > position) {
				float relativePositionOnExon = ((float) (position - startBorder.getPosition())) / ((float) (point.getPosition() - startBorder.getPosition()));
				float shrinkedLengthOfExon = point.getShrinkedPosition() - startBorder.getShrinkedPosition();
				float shrinkedPosition = startBorder.getShrinkedPosition() + relativePositionOnExon * shrinkedLengthOfExon;
				return shrinkedPosition;
			}
		}
		logger.error("FATAL. getShrinkedPosition calculation failure.");
		return 1.0f;
	}
	
	private void calculateShrinkedPositions(float intronProportion) throws Exception {
		this.calculateShrinkedPositions();
		int numberOfExons = shrinkedBreakPoints.size() / 2;
		double intronLength = ((double) intronProportion) / ((double) (numberOfExons - 1));
		float exonProportion = ((float) getTotalExonLength()) / ((float) gene.getLength());
		float currentShrinkedPosition = 0.0f;
		Position exonStartPoint = null;
		for (Position point : shrinkedBreakPoints) {
			if(point.isStart()) {
				point.setShrinkedPosition(currentShrinkedPosition);
				exonStartPoint = point;
			} else {
				float exonLength = point.getRelativePosition() - exonStartPoint.getRelativePosition();
				float exonProportionalLength = (1.0f - intronProportion) * exonLength / exonProportion;
				currentShrinkedPosition += exonProportionalLength;
				point.setShrinkedPosition(currentShrinkedPosition);
				currentShrinkedPosition += intronLength;
			}
		}
	}
	
	private void calculateShrinkedPositions() throws Exception {
		SortedSet <Position> breakPoints = this.gene.getBreakPoints();
		boolean wasStart = false;
		boolean wasEnd = false;
		Position breakPointToAdd = null;
		for(Position point : breakPoints) {
			if(!wasStart) {
				if(!point.isStart()) throw new Exception("FATAL. Assertion error. First BreakPoint is not start");
				breakPointToAdd = point;
				this.shrinkedBreakPoints.add(breakPointToAdd);
				wasStart = true;
			} else {
				if(!wasEnd) {
					if(!point.isStart()) {
						breakPointToAdd = point;
						wasEnd = true;
					}
				} else {
					if(point.getPosition() - breakPointToAdd.getPosition() == 1) {
					} else {
						// there was end and new start is not immediately after end
						if(point.isStart()) {
							this.shrinkedBreakPoints.add(breakPointToAdd);
							this.shrinkedBreakPoints.add(point);
							breakPointToAdd = point;
							wasEnd = false;
						} else {
							breakPointToAdd = point;
						}
					}
				}
			}
		}
		this.shrinkedBreakPoints.add(breakPointToAdd);
	}
	
	private int getTotalExonLength() {
		int sum = 0;
		int start = 0;
		for(Position point : shrinkedBreakPoints) {
			if(point.isStart())
				start = point.getPosition();
			else
				sum += point.getPosition() - start;
		}
		return sum;
	}
	
	private float getMaxFromCoverageList(Collection <Map <Integer, Float>> coverageData) {
		int MIN_VALUE = 10;
		Float max = (float) MIN_VALUE;
		for(Map <Integer, Float> coverage : coverageData)
			for(Float val : coverage.values())
				if (val > max)
					max = val;
		return max;
	}
	
}
