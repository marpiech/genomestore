package org.cremag.utils.plot;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class LinePlotter extends Plotter {
	int MIN_VALUE = 10;
	public void plotFeatures(Graphics2D image, Map <Float, Float> data, int verticalPosition, int margin, int height, Color color) {
		
		
		Float max = (float) MIN_VALUE; // set max value
		for(Float val : data.values())
			if (val > max)
				max = val;
		
		plotFeatures(image, data, verticalPosition, margin, height, color, max);

	}
	
	public void plotFeatures(Graphics2D image, Map <Float, Float> data, int verticalPosition, int margin, int height, Color color, float max) {
		image.setPaint(color);

		setVerticalPosition(verticalPosition); // set Plotter fields
		setMargin(margin);
		setWidth(image);
		
		int start = 0;
		int startHeight = VERTICAL_POSITION + height;
		int end = 0;
		int endHeight = VERTICAL_POSITION + height;
		int levelZero = VERTICAL_POSITION + height;
		
		for(Entry <Float, Float> entry : data.entrySet()) {
			end = (int) (((float) WIDTH) * entry.getKey());
			int breakLineHeight = (int) (((float) height) * (entry.getValue() / max));
			endHeight = VERTICAL_POSITION + height - breakLineHeight;			
			if (end > start + 4) {
				image.drawLine(MARGIN + start, startHeight, MARGIN + start + 1, levelZero);
				image.drawLine(MARGIN + start + 1, levelZero, MARGIN + end - 1, levelZero);
				image.drawLine(MARGIN + end - 1, levelZero, MARGIN + end, endHeight);
			} else {
				image.drawLine(MARGIN + start, startHeight, MARGIN + end, endHeight);
			}
			start = end;
			startHeight = endHeight;
		}		
	}
}
