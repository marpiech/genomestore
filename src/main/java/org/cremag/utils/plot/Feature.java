package org.cremag.utils.plot;

public class Feature {
	float relativeStart;
	float relativeEnd;
	float height;
	boolean center;
	
	public Feature(float start, float end, float height, boolean center) {
		setRelativeStart(start);
		setRelativeEnd(end);
		setHeight(height);
		setCenter(center);
	}

	public float getRelativeStart() {
		return relativeStart;
	}

	public void setRelativeStart(float relativeStart) {
		this.relativeStart = relativeStart;
	}

	public float getRelativeEnd() {
		return relativeEnd;
	}

	public void setRelativeEnd(float realtiveEnd) {
		this.relativeEnd = realtiveEnd;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public boolean isCenter() {
		return center;
	}

	public void setCenter(boolean center) {
		this.center = center;
	}
	
}
