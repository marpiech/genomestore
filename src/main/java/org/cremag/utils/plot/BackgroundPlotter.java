package org.cremag.utils.plot;

import java.awt.Color;
import java.awt.Graphics2D;

public class BackgroundPlotter extends Plotter {
	
	public void plotBackground(Graphics2D image, int verticalPosition, int height, int margin, Color color) {
		setMargin(margin);
		setWidth(image);
		setVerticalPosition(verticalPosition);
		setHeight(height);
		image.setPaint(color);
		image.fillRect(margin, verticalPosition, WIDTH, HEIGHT);
	}
	
	public void plotLinedBackground(Graphics2D image, int verticalPosition, int height, int margin, Color color, int breaks, Color breakColor) {
		setMargin(margin);
		setWidth(image);
		setVerticalPosition(verticalPosition);
		setHeight(height);
		image.setPaint(color);
		image.fillRect(margin, verticalPosition, WIDTH, HEIGHT);
		float breakSize = ((float) WIDTH) / ((float) breaks);
		image.setPaint(breakColor);
		for(int i = 0; i < breaks; i++) {
			int horizontalPosition = (int) (i * breakSize);
			image.drawLine(MARGIN + horizontalPosition, VERTICAL_POSITION, 
					MARGIN + horizontalPosition, VERTICAL_POSITION + HEIGHT);
		}
	}
}
