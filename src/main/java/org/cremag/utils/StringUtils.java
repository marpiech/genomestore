package org.cremag.utils;

import java.util.List;
import java.util.Arrays;
import java.util.Iterator;


public class StringUtils {

	public static String join(List<String> list, String delimiter) {
		StringBuffer buffer = new StringBuffer();
		Iterator<String> iter = list.iterator();
		if (iter.hasNext()) {
			buffer.append(iter.next());
			while (iter.hasNext()) {
				buffer.append(delimiter);
				buffer.append(iter.next());
			}
		}
		return buffer.toString();
	}

	public static String join(String[] array, String delimiter) {
		return StringUtils.join(Arrays.asList(array), delimiter);
	}
	
	public static String renderXMLElement(String tag, String attributes, String body) {
		return "<" + tag + " " + attributes + ">" + body + "</" + tag + ">";
	}

	public static String renderXMLElement(String tag, String attributes) {
		return "<" + tag + " " + attributes + "/>";
	}
	
	public static String renderXMLAttribute(String name, String value) {
		return name + "=" + StringUtils.quote(value);
	}
	
	public static String quote(String s, String c) {
		return c + s + c;
	}

	public static String quote(String s) {
		return StringUtils.quote(s, "\"");
	}
	
	public static double roundToSignificantFigures(double num, int n) {
	    if(num == 0) {
	        return 0;
	    }

	    final double d = Math.ceil(Math.log10(num < 0 ? -num: num));
	    final int power = n - (int) d;

	    final double magnitude = Math.pow(10, power);
	    final long shifted = Math.round(num*magnitude);
	    return shifted/magnitude;
	}
	
	public static float roundToSignificantFigures(float num, int n) {
	    if(num == 0) {
	        return 0;
	    }

	    final float d = (float) Math.ceil(Math.log10(num < 0 ? -num: num));
	    final int power = n - (int) d;

	    final float magnitude = (float) Math.pow(10, power);
	    final long shifted = Math.round(num*magnitude);
	    return shifted/magnitude;
	}
	
}