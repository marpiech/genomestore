package org.cremag.utils.genome;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.cremag.genomic.BedItem;
import org.cremag.genomic.Position;
import org.cremag.utils.genome.gtf.GtfItem;


public class Gene {
	private static final Logger logger = Logger.getLogger(Gene.class);
	
	String geneId;
	List <BedItem> gftItems; //exons
	SortedSet <Position> breakPoints;
	Map <String, Transcript> transcripts;
	BedItem geneRange;
	
	public BedItem getRange() {
		return geneRange;
	}
	
	public int getNumberOfTtanscripts() {
		return transcripts.size();
	}
	
	public Gene (String id) {
		logger.trace("Gene constructor()");
		this.geneId = id;
		this.gftItems = new ArrayList<BedItem>();
		this.breakPoints = new TreeSet<Position>();
		this.transcripts = new TreeMap <String, Transcript>();
	}
	
	public List <BedItem> getSpecificityIndex() {
		List <BedItem> specificityIndex = new ArrayList <BedItem>();
		Position startBorder = null;
		Position endBorder = null;
		for(Position point : breakPoints) {
			if(startBorder == null) {
				logger.trace("Position: " + point.getPosition());
			} else {
				int start;
				int end;
				endBorder = point;
				if(startBorder.isStart())
					start = startBorder.getPosition();
				else
					start = startBorder.getPosition() + 1;
				if(endBorder.isStart())
					end = endBorder.getPosition() + 1;
				else
					end = endBorder.getPosition();
				BedItem item = new BedItem(geneRange.getChromosome(), start, end);
				float score = 0;
				for(Transcript transcript : transcripts.values())
					if(transcript.containsBed(item))
						score++;
				if(score > 0)
					score = transcripts.size() / (float) Math.pow(2.0, (double) (score - 1));
				item.setScore("" + score);
				if(item.getLength() > 30)
					specificityIndex.add(item);
				logger.trace("Position: " + point.getPosition() + " " +  point.isStart() + " Item: " + item.getStart() + "-" + item.getEnd() + "Score:" + score); 
			}
			startBorder = point;
		}
		return specificityIndex;
	}
	
	public boolean containsBed(BedItem bedItem) {
		for(BedItem item : this.gftItems)
			if(item.overlaps(bedItem, true))
				return true;
		return false;
	}
	
	public Collection<Transcript> getTranscripts() {
		return transcripts.values();
	}
	
 	public void addBedItem(BedItem bedItem) {
		setGeneRange(bedItem);
		gftItems.add(bedItem);
		breakPoints.add(new Position(bedItem.getChromosome(), 
				bedItem.getStart(), true, this));
		breakPoints.add(new Position(bedItem.getChromosome(), 
				bedItem.getEnd(), false, this));
	
		if(bedItem instanceof GtfItem) {
			GtfItem gtfItem = (GtfItem) bedItem;
			String transcriptId = gtfItem.getTranscriptId();
			Transcript transcript = transcripts.get(transcriptId); 
			if (transcript == null) {
				transcript = new Transcript(transcriptId);
				transcripts.put(transcriptId, transcript);
			}
			transcript.addBedItem(bedItem);
		}

	}

	public int getLength() {
		return geneRange.getLength();
	}
	
	public List<BedItem> getBedItems() {
		return gftItems;
	}
	
	public float getRelativePosition(int position) {
		return ((float)(position - geneRange.getStart())) / ((float) (geneRange.getLength()));
	}
	
	public SortedSet<Position> getBreakPoints() {
		return breakPoints;
	}

	protected void setGeneRange(BedItem bedItem) {
		if (this.geneRange == null)
			this.geneRange = new BedItem(bedItem.getChromosome(), 
					bedItem.getStart(), bedItem.getEnd());
		else {
			if (geneRange.getStart() > bedItem.getStart())
				geneRange.setStart(bedItem.getStart());
			if (geneRange.getEnd() < bedItem.getEnd())
				geneRange.setEnd(bedItem.getEnd());
		}
	}
}
