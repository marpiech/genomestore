package org.cremag.utils.genome.gtf;

import org.apache.log4j.Logger;
import org.cremag.genomic.BedItem;

public class GtfItem extends BedItem {

	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(GtfItem.class);
	
	boolean strandPlus;
	String geneId; 
	String transcriptId;
	
	/**
	 * GtfRecord constructor
	 * @param gtfLine
	 */
	public GtfItem(String gtfLine) {
		
		String[] gtfFields = gtfLine.split("\t");
		this.chromosome = gtfFields[0];
		this.score = gtfFields[2];
		this.start = new Integer(gtfFields[3]);
		this.end = new Integer(gtfFields[4]);
		if(gtfFields[6].equals("+")) this.strandPlus = true;
		else this.strandPlus = false;
		String[] idFields = gtfFields[8].split("; ");
		this.geneId = idFields[0].replaceAll("gene_id \"", "").replaceAll("\"", ""); 
		this.transcriptId = idFields[1].replaceAll("transcript_id \"", "").replaceAll("\"", "");
	}
	
	/* toString */
	
	@Override
	public String toString() {
		String out = this.chromosome + "\t" + 
				"cremagSequtils" + "\t" +
				this.score + "\t" +
				this.start + "\t" +
				this.end + "\t" +
				"." + "\t" +
				this.getStrand() + "\t" +
				"." + "\t" +
				"gene_id \"" + this.geneId + "\"; " +
				"transcript_id \"" + this.geneId + "\"";
				
		return out;
	}
	
	/* Getters */
	
	public String getStrand() {
		if(this.strandPlus)
			return "+";
		else
			return "-";
	}

	public boolean isStrandPlus() {
		return strandPlus;
	}
	public String getRecordType() {
		return score;
	}
	public String getGeneId() {
		return geneId;
	}
	public String getTranscriptId() {
		return transcriptId;
	}
	
	
}
