package org.cremag.utils.genome;

import org.cremag.genomic.BedItem;
import org.cremag.genomic.Position;

public class Transcript extends Gene {

	public Transcript(String id) {
		super(id);
	}
	@Override
	public void addBedItem(BedItem bedItem) {
		setGeneRange(bedItem);
		gftItems.add(bedItem);
		breakPoints.add(new Position(bedItem.getChromosome(), 
				bedItem.getStart(), true, this));
		breakPoints.add(new Position(bedItem.getChromosome(), 
				bedItem.getEnd(), false, this));
	}
}
