package org.cremag.utils.file;

import java.io.File;

public class FileUtils {

	public static void recursiveDelete(File dirPath) {
		String[] ls = dirPath.list();

		for (int idx = 0; idx < ls.length; idx++) {
			File file = new File(dirPath, ls[idx]);
			if (file.isDirectory())
				recursiveDelete(file);
			file.delete();
		}
		dirPath.delete();
	}
}
