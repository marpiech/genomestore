package org.cremag.utils.file;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.cremag.utils.genome.Gene;
import org.cremag.utils.genome.gtf.GtfItem;

public class GtfFile {
	private static final Logger logger = Logger.getLogger(GtfFile.class);
	
	BufferedReader gtfReader;
	
	public GtfFile(String pathToFile) throws FileNotFoundException {
		logger.trace("GtfFile contructor()");
		this.gtfReader = new BufferedReader(new FileReader(pathToFile));
	}
	
	public Gene readGene(String geneId) throws IOException {
		String line;
		Gene gene = new Gene(geneId);
		while ((line = this.gtfReader.readLine()) != null) {
			if(line.contains(geneId)) {
				GtfItem gtfRecord = new GtfItem(line);
				if(gtfRecord.getGeneId().equals(geneId)) {
					logger.trace(gtfRecord);
					gene.addBedItem(gtfRecord);	
				}	
			}
		}
		return gene;
	}
	
}
