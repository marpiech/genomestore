package org.cremag.utils.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.cremag.genomic.BedItem;

public class BamStock {
	Map <String, List<BamReader>> stock;
	
	public BamStock(String file) throws FileNotFoundException, IOException {
		this.stock = getStock(new BufferedReader(new FileReader(new File(file))));
		
	}

	public void getFoldChange(BedItem bedItem) {
		/*for(Entry <String, List<BamReader>> entry : stock.entrySet()) {
			List< Map <Integer, Float>> coverageList = new ArrayList< Map <Integer, Float>>();
			for(BamReader bamReader : entry.getValue())


		}*/
	}
	
	public Map<String, List <Map <Integer, Float>>> getSamples(BedItem bedItem, int resolution) {
		Map <String, List <Map <Integer, Float>>> histograms = new TreeMap <String, List <Map <Integer, Float>>>();
		for(Entry <String, List<BamReader>> entry : stock.entrySet()) {
			List <Map <Integer, Float>> coverageList = new ArrayList< Map <Integer, Float>>();
			for(BamReader bamReader : entry.getValue())
				coverageList.add(bamReader.getCoverageMap(bedItem, resolution));
			histograms.put(entry.getKey(), coverageList);
		}
		return histograms;
	}
	
	public Map <String, Map <Integer, Float>> getHistogram(BedItem bedItem, int resolution) {
		Map <String, Map <Integer, Float>> histograms = new TreeMap <String, Map <Integer, Float>>();
		for(Entry <String, List<BamReader>> entry : stock.entrySet()) {
			List< Map <Integer, Float>> coverageList = new ArrayList< Map <Integer, Float>>();
			for(BamReader bamReader : entry.getValue())
				coverageList.add(bamReader.getCoverageMap(bedItem, resolution));
			histograms.put(entry.getKey(), getAverageCoverage(coverageList));
		}
		return histograms;
		//return stock.get("Control").get(0).getCoverage(bedItem, resolution);
	}
	
	private Map <String, List<BamReader>> getStock(BufferedReader description) throws IOException {
		Map <String, List<BamReader>> stock = new TreeMap <String, List<BamReader>>();
		String descriptionLine;
		
		int numberOfFiles = 0;
		long totalSize = 0;
		
		while((descriptionLine = description.readLine()) != null) {
			String[] fileAndGroup = descriptionLine.split("\t");
			if(fileAndGroup.length == 2) {
				String group = fileAndGroup[1];
				List<BamReader> bamFiles = stock.get(group);
				BamReader bamReader = new BamReader(fileAndGroup[0]);
				if (bamFiles == null) {
					bamFiles = new ArrayList <BamReader> ();
					bamFiles.add(bamReader);
					stock.put(group, bamFiles);
				} else {
					bamFiles.add(bamReader);
				}
				numberOfFiles++;
				totalSize += bamReader.getSize();
			}
		}
		
		float averageSize = (float) (totalSize / (long) numberOfFiles);
		
		for(List<BamReader> bamFiles : stock.values())
			for(BamReader reader : bamFiles) {
				reader.setWeight(averageSize / (float) reader.getSize());
				System.out.println(reader.getWeight());
			}
		
		return stock;
	}
	
	private Map<Integer, Float> getAverageCoverage(List< Map <Integer, Float>> coverageList) {
		float numberOfCoverageHistograms = (float) coverageList.size();
		Map <Integer, Float> averageCoverage = new TreeMap <Integer, Float> ();
		for(Map <Integer, Float> coverage : coverageList)
			for(Entry <Integer, Float> entry : coverage.entrySet()) {
				Float value = averageCoverage.get(entry.getKey());
				if(value == null)
					value = entry.getValue() / numberOfCoverageHistograms;
				else
					value += entry.getValue() / numberOfCoverageHistograms;
				averageCoverage.put(entry.getKey(), value);
			}
		return averageCoverage;
	}
}
