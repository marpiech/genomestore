package org.cremag.utils.file;

import java.io.File;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.cremag.genomic.BedItem;

import net.sf.samtools.Cigar;
import net.sf.samtools.CigarElement;
import net.sf.samtools.SAMFileReader;
import net.sf.samtools.SAMRecord;
import net.sf.samtools.SAMRecordIterator;

public class BamReader {
	
	private static final Logger logger = Logger.getLogger(BamReader.class);
	SAMFileReader reader;
	long size;
	float weight = 1.0f;
	
	public BamReader(String file) {
		this.size = new File(file).length() / 1024;
		this.reader =  new SAMFileReader(new File(file));
	}
	public long getSize() {
		return size;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
	public float getCoverage(BedItem bedItem) {
		int total = 0;
		SAMRecordIterator iterator = reader.queryOverlapping(bedItem.getChromosome(), bedItem.getStart(), bedItem.getEnd());
		int bedStart = bedItem.getStart();
		int bedEnd = bedItem.getEnd();
		while (iterator.hasNext()) {
			SAMRecord record = iterator.next();
			int start = record.getAlignmentStart();
			Cigar cigar = record.getCigar();
			for(CigarElement element : cigar.getCigarElements()) {
				if(element.getOperator().toString().equals("M"))
					for(int i = 0 ; i < element.getLength() ; i++)
						if(i + start >= bedStart && i + start <= bedEnd)
							total++;
				start += element.getLength();
			} // cigarElement loop
		}
		iterator.close();
		return (float) total / (float) bedStart - bedEnd;
	}
	
	public Map<Integer, Float> getCoverageMap(BedItem bedItem, int resolution) {
		Map <Integer, Float> coverage = new TreeMap <Integer, Float>();
		SAMRecordIterator iterator = reader.queryOverlapping(bedItem.getChromosome(), bedItem.getStart(), bedItem.getEnd());
		int bedStart = bedItem.getStart();
		int bedEnd = bedItem.getEnd();
		while (iterator.hasNext()) {
			SAMRecord record = iterator.next();
			int start = record.getAlignmentStart();
			Cigar cigar = record.getCigar();
			for(CigarElement element : cigar.getCigarElements()) {
				if(element.getOperator().toString().equals("M"))
					for(int i = 0 ; i < element.getLength() ; i++) {
						Integer position = i + start;
						if(position % resolution == 0 && position >= bedStart && position <= bedEnd) {
							Float coverageElement = coverage.get(position);
							if(coverageElement == null)
								coverage.put(position, weight);
							else
								coverage.put(position, coverageElement += weight);
						}
					}
				start += element.getLength();
			} // cigarElement loop
		}
		iterator.close();
		return coverage;
	}
	
}
