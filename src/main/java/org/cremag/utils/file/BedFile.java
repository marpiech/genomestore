package org.cremag.utils.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.cremag.genomic.BedItem;
import org.cremag.genomic.Coverage;
import org.cremag.utils.file.genomic.GenomicTrack;

public class BedFile {
	
	String id;
	File file;
	
	public BedFile(String path) {
		this.file = new File(path);
		this.id = path;
	}
	
	public List<BedItem> getBedItems() throws Exception {
		List<BedItem> items = new ArrayList<BedItem> ();
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line;
		while((line = reader.readLine()) != null) {
			BedItem item = new BedItem(line);
			items.add(item);
		}
		reader.close();
		return items;
	}
}
