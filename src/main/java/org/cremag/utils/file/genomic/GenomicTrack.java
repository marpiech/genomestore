package org.cremag.utils.file.genomic;

import java.awt.Graphics2D;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.cremag.genomic.BedItem;
import org.cremag.genomic.Coverage;
import org.cremag.utils.plot.PlotProperties;

public abstract class GenomicTrack implements Comparable<GenomicTrack> {
	
	public enum TrackType {BIGWIG, BIGBED}
	
	public enum TrackClass {TRANSCRIPTION, HISTONE, POLYMERASE, DNASE, FEATURE, CONTROL, OTHER;
		public static TrackClass getTrackClass (String str) {
			if (str.toLowerCase().contains("transcription"))
				return TrackClass.TRANSCRIPTION;
			if (str.toLowerCase().contains("histone"))
				return TrackClass.HISTONE;
			if (str.toLowerCase().contains("polymerase"))
				return TrackClass.POLYMERASE;
			if (str.toLowerCase().contains("dnase"))
				return TrackClass.DNASE;
			if (str.toLowerCase().contains("feature"))
				return TrackClass.FEATURE;
			if (str.toLowerCase().contains("control"))
				return TrackClass.CONTROL;
			if (str.toLowerCase().contains("input"))
				return TrackClass.CONTROL;
			return TrackClass.OTHER;
		}
		
		public static Set <TrackClass> getFilters (String filtersString) {
			
			Set <TrackClass> filters = new HashSet <TrackClass> ();
			for (String filter : filtersString.split(",")) {
				filters.add(TrackClass.getTrackClass(filter));
			}
			return filters;
		}
		
	}
	
	protected String id;
	protected String name;
	protected TrackType trackType;
	protected TrackClass trackClass;
	
	public abstract Coverage getCoverage(BedItem bed);
	public abstract void plot(PlotProperties plot, Graphics2D image, BedItem range);
	
	/* getters and setters */
	public String getId() {return id;}
	public void setId(String id) {this.id = id;}
	
	public String getName() {return name;}
	public void setName(String name) {this.name = name;}
	
	public TrackType getTrackType() {return trackType;}
	public void setTrackType(TrackType trackType) {this.trackType = trackType;}
	
	public TrackClass getTrackClass() {return trackClass;}
	public void setTrackClass(TrackClass trackClass) {this.trackClass = trackClass;}
	
	/* comparators */
	@Override
	public boolean equals(Object o) {
		return (this.id.equals(((GenomicTrack) o).id));
	}
	
	@Override
	public int compareTo(GenomicTrack genomicFile) {
		return this.id.compareTo(genomicFile.id);
	}
}
