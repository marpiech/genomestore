/*
 * Copyright (C) 2012 - Marcin Piechota
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 Unported (CC BY-NC 3.0)
 * You may obtain a copy of the License at http://creativecommons.org/licenses/by-nc/3.0/legalcode
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package org.cremag.utils.file.genomic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.cremag.utils.NetUtils;

/**
 * @author marpiech
 * Track Description is a tupple for manipulation of track description
 */
public class TrackDescription {
	private Map <String, String> description = new TreeMap <String, String>();
	private static final Logger logger = Logger
			.getLogger(TrackDescription.class);
	
	public TrackDescription (File file) {
		String path = file.getAbsolutePath();
		path = path.substring(0, path.lastIndexOf(".")) + ".desc";
		File descriptionFile = new File(path);
		if (descriptionFile.exists()) {
			logger.trace("Reading track file description");
			try {
				BufferedReader reader = new BufferedReader(new FileReader(descriptionFile));
				String line;
				while((line = reader.readLine()) != null) {
					String[] lineSplit = line.split("\t");
					if(lineSplit.length > 0 && !lineSplit[0].equals(""))
						description.put(lineSplit[0], lineSplit[1]);
				}
				reader.close();
			} catch (FileNotFoundException e) {
				logger.error(e.getMessage());
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		}
	}
	
	public TrackDescription (URL url) {
		//logger.trace("Path: " + url.getPath());
		//logger.trace("Path: " + url);
		String path = url.toString();
		path = path.substring(0, path.lastIndexOf(".")) + ".desc";
		logger.trace("Path: " + path);
		URL descriptionFile = null;
		
		try {
			descriptionFile = new URL(path);
		} catch (MalformedURLException e) {
			logger.error(e.getMessage());
		}
		
		if (NetUtils.exists(path)) {
			logger.trace("Reading track file description");
			try {
				//logger.trace("Reading line");
				BufferedReader reader = new BufferedReader(new InputStreamReader(descriptionFile.openStream()));
				String line;
				while((line = reader.readLine()) != null) {
					logger.trace("Line: " + line);
					String[] lineSplit = line.split("\t");
					//logger.trace("Line: " + lineSplit[0]);
					if(lineSplit.length > 0 && !lineSplit[0].equals(""))
						description.put(lineSplit[0].replace("(", "").replace(")", ""), lineSplit[1]);
				}
				reader.close();
			} catch (FileNotFoundException e) {
				logger.error(e.getMessage());
			} catch (IOException e) {
				logger.error(e.getMessage());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		//logger.trace("Finished reading track description");
	}
	
	public Map <String, String> getDescriptionMap() {
		return description;
	}
}
