package org.cremag.utils.file.genomic;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.DataFormatException;

import org.apache.log4j.Logger;
import org.broad.igv.bbfile.BBFileReader;
import org.broad.igv.bbfile.BedFeature;
import org.broad.igv.bbfile.BigBedIterator;
import org.cremag.genomic.BedItem;
import org.cremag.genomic.Coverage;
import org.cremag.genomic.Position;
import org.cremag.utils.plot.PlotProperties;

public class BigBed extends GenomicTrack {
	
	@SuppressWarnings("unused")
	private static final Logger logger = Logger
			.getLogger(BigBed.class);
	
	BBFileReader reader;
	
	public BigBed (BBFileReader reader) {
		this.id = reader.getBBFilePath();
		this.reader = reader;
		this.trackType = TrackType.BIGBED;
	}

	
	@Override
	public Coverage getCoverage(BedItem bed) {
		return null;
	}
	
	@Override
	public void plot(PlotProperties plot, Graphics2D image, BedItem range) {
		
		Font font = new Font("Arial", Font.BOLD, 14);
	    image.setFont(font);
		FontMetrics fontMetrics = image.getFontMetrics();
		int stringHeight = fontMetrics.getAscent();
		String message = plot.getLegend();
		image.drawString(message, plot.getX(), plot.getY() + stringHeight);
		
		int y = plot.getY();
		y = y + 15;
		plot.setHeight(15);
		
		BigBedIterator iterator;
		try {
			iterator = reader.getBigBedIterator(range.getChromosome(), range.getStart(), range.getChromosome(), range.getEnd(), false);

		int count = 0; for ( ; iterator.hasNext() ; ++count ) iterator.next();
		iterator = reader.getBigBedIterator(range.getChromosome(), range.getStart(), range.getChromosome(), range.getEnd(), false);

		
		
		int maximumLines = 10;
		if (count > maximumLines) {
			y = y + 30;
			plot.setHeight(plot.getHeight() + 30);
		}
		
		image.setFont(new Font("Arial", Font.BOLD, 10));
		fontMetrics = image.getFontMetrics();
		
		List <String> symbols = new ArrayList <String>();
		
		while(iterator.hasNext()) {
			
			BedFeature feature = iterator.next();
			
			String symbol = feature.getRestOfFields()[0];
			
			if (count <= 10) {
				y = y + 12;
				plot.setHeight(plot.getHeight() + 12);
				message = symbol;
				int width = fontMetrics.stringWidth(message);
				image.drawString(message, Math.max(plot.getX() - width - 15, plot.getX() - 45), y);
			} else {
				if(!symbols.contains(symbol) && symbol.length() < 7) {
					int convertedStart = plot.convertHorizontal(new Position(feature.getChromosome(), feature.getStartBase()), range);
					message = symbol;
					image.drawString(message, Math.max(convertedStart, plot.getX()), y - 15);
					symbols.add(symbol);
				}
			}
			
			draw(plot, 
					image, 
					1, 
					new BedItem(feature.getChromosome(), feature.getStartBase(), feature.getEndBase()),
					range,
					feature.getRestOfFields()[2], y);
			
			int boldStart = new Integer(feature.getRestOfFields()[3]);
			int boldEnd = new Integer(feature.getRestOfFields()[4]);
			
			//logger.info("RANGE: " + range.toString());
			
			int numberOfExons = new Integer(feature.getRestOfFields()[6]);

			for(int i = 0; i < numberOfExons; i++) {
				int width = 1;
				int start = feature.getStartBase() + new Integer(feature.getRestOfFields()[8].split(",")[i]);
				int end = feature.getStartBase() + new Integer(feature.getRestOfFields()[8].split(",")[i]) + new Integer(feature.getRestOfFields()[7].split(",")[i]);
				
				//logger.info("FEATURE: " + start + "-" + end + " BOLD: " + boldStart + "-" + boldEnd);
				
				/* ------ */
				if ((boldStart < start && boldEnd < start) || (boldStart > end && boldEnd > end) || (boldStart == boldEnd)) {
					width = 5;
					draw(plot, image, width, new BedItem(feature.getChromosome(), start, end), range, null, y);	
				}
				
				/* ---III */
				if (boldStart > start && boldStart < end && boldEnd >= end) {
					width = 5;
					draw(plot, image, width, new BedItem(feature.getChromosome(), start, boldStart), range, null, y);
					width = 10;
					draw(plot, image, width, new BedItem(feature.getChromosome(), boldStart, end), range, null, y);
				}
				
				/* IIIIII */
				if (boldStart <= start && boldEnd >= end) {
					width = 10;
					draw(plot, image, width, new BedItem(feature.getChromosome(), start, end), range, null, y);
				}				

				/* III--- */
				if (boldStart <= start && boldEnd > start && boldEnd < end) {
					width = 10;
					draw(plot, image, width, new BedItem(feature.getChromosome(), start, boldEnd), range, null, y);
					width = 5;
					draw(plot, image, width, new BedItem(feature.getChromosome(), boldEnd, end), range, null, y);
				}

				/* --II-- */
				if (boldStart > start && boldEnd < end) {
					width = 5;
					draw(plot, image, width, new BedItem(feature.getChromosome(), start, boldStart), range, null, y);
					width = 10;
					draw(plot, image, width, new BedItem(feature.getChromosome(), boldStart, boldEnd), range, null, y);
					width = 5;
					draw(plot, image, width, new BedItem(feature.getChromosome(), boldEnd, end), range, null, y);
				}

				
				
			}
			
			/*logger.info(feature.getChromosome() + ":" + feature.getStartBase() + "-" + feature.getEndBase());
			for(String str : feature.getRestOfFields())
				logger.info(str);*/


			
		}
		
		} catch (DataFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		plot.setHeight(plot.getHeight() + 10);
		

	}
	
	private void draw(PlotProperties plot, Graphics2D image, int width, BedItem feature, BedItem range, String strand, int y) {
		
		//logger.info("Draw:" + feature + " " + range + " " + strand);
		y = y - width / 2;
		
		Position start = new Position(feature.getChromosome(), feature.getStart());
		Position end = new Position(feature.getChromosome(), feature.getEnd());
		int convertedStart = plot.convertHorizontal(start, range);
		if(convertedStart < plot.getX())
			convertedStart = plot.getX();
		int convertedEnd = plot.convertHorizontal(end, range);
		if(convertedEnd > plot.getX() + plot.getWidth())
			convertedEnd = plot.getX() + plot.getWidth();			
		int len = convertedEnd - convertedStart;
		image.fillRect(convertedStart, y, 
				 len, width);
		
		if (strand != null) {
		
			int directionLine = 0;
			try {
				if(strand.equals("+"))
					directionLine = -2;
				if(strand.equals("-"))
					directionLine = 2;
			} catch (Exception e) {}
			
			int arrowDist = 5;
			if (width == 1 && directionLine != 0) {
				int arrowStart = (convertedStart + arrowDist) - ((convertedStart + arrowDist) % arrowDist);
				for(int i = arrowStart; i < convertedStart + len - arrowDist; i += arrowDist) {
					image.drawLine(i, y + 1, i + directionLine, y + 2);
					image.drawLine(i, y, i + directionLine, y - 1);
				}
			}
		}
	}
	
}
