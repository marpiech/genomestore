package org.cremag.utils.file.genomic;

import org.apache.log4j.Logger;
import org.broad.igv.bbfile.BBFileReader;
import org.cremag.genomeStore.Genome;

public class GenomicFileFactory {
	private static final Logger logger = Logger
			.getLogger(GenomicFileFactory.class);
	
	public GenomicTrack createGenomicFile(String path) {
		try {
			BBFileReader reader=new BBFileReader(path);
			if (reader.isBigWigFile())
				return(new BigWig(reader));
			if (reader.isBigBedFile())
				return(new BigBed(reader));
		} catch (Exception e) {
			logger.error("Unknown file type");
		}
		return null;
	}
}
