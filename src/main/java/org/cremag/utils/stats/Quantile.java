package org.cremag.utils.stats;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

/*
 * @author marpiech
 * This class stores (n - 1) thresholds for (n) quantiles.
 * For example there should be 3 threshold for quartiles.
 */
public class Quantile {
	
	private static final Logger logger = Logger.getLogger(Quantile.class);
	
	private int quantiles; // number of quantiles
	private List <Double> thresholds; // list of thresholds
	
	public Quantile (int n) {
		this.thresholds = new ArrayList<Double>();
		this.quantiles = n;
	}

	public int size() {return quantiles;}
	
	public boolean isFilled() {
		if(thresholds.size() == this.size() - 1)
			return true;
		if(thresholds.size() < this.size() - 1)
			return false;
		throw new RuntimeException("SEVERE ERROR: More thresholds than quantiles!");
	}
	
	public boolean add(double threshold) {
		if(this.isFilled()) {
			logger.error("Quantile thresholds collection is full");
			return false;
		}
		this.thresholds.add(threshold);
		Collections.sort(this.thresholds);
		return true;
	}
	
	public Iterator<Double> getIterator() {
		return this.thresholds.iterator();
	}
	
}
