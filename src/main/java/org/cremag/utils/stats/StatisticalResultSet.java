package org.cremag.utils.stats;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.json.JSONArray;

public class StatisticalResultSet {
	
	/**
	 * Map containing statistical significance (Track id, P value)
	 */
	Map <String, StatisticalResult> resultSet = new TreeMap <String, StatisticalResult> ();
	
	public StatisticalResultSet() {	
	}
	
	public synchronized void put(String id, String name, Double pValue, Double fold, Double backgroundMean, Double queryMean) {
		StatisticalResult result = new StatisticalResult();
		result.setId(id);
		result.setName(name);
		result.setPValue(pValue);
		result.setFoldChange(fold);
		result.setBackgroundMean(backgroundMean);
		result.setQueryMean(queryMean);
		this.resultSet.put(id, result);
	}
	
	public Map <String, StatisticalResult> getResult() {
		return resultSet;
	}
	
	public JSONArray getJSON() {
		JSONArray jsonArray = new JSONArray();
		List <StatisticalResult> results = new ArrayList<StatisticalResult>(resultSet.values());
		Collections.sort(results);
		for(StatisticalResult result : results)
			jsonArray.put(result.getJSON(results.size()));
		return jsonArray;
	}
	
	@Override
	public String toString() {
		String out = "";
		for(Entry <String, StatisticalResult> entry : resultSet.entrySet()) {
			out += entry.getKey() + "\t" + entry.getValue() + "\n";
		}
		return out;
	}

	public String getCSV() {
		String out;
		out = "";
		out += "id" + "\t";
		out += "name" + "\t";
		out += "pvalue" + "\t";
		out += "bonferroni" + "\t";
		out += "foldchange" + "\t";
		out += "backgroundmean" + "\t";
		out += "querymean" + "\n";
		List <StatisticalResult> results = new ArrayList<StatisticalResult>(resultSet.values());
		Collections.sort(results);
		for(StatisticalResult result : results)
			out += result.getCSV(results.size()) + "\n";
		out = out.substring(0, out.length()-1);
		return out;
	}
}
