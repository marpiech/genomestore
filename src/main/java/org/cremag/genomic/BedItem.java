package org.cremag.genomic;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.batik.svggen.SVGGraphics2D;
import org.cremag.exception.InvalidBedStringException;
import org.cremag.utils.plot.PlotProperties;

public class BedItem implements Comparable<BedItem> {
	protected String chromosome;
	protected int start;
	protected int end;
	protected String name;
	protected String strand;
	protected String score;
	
	public BedItem() {}
	
	public BedItem(String position) {
		if (position.contains(":")) {
			position = position.replace(",", "");
			this.chromosome = position.split(":")[0];
			this.start = new Integer(position.split(":")[1].split("-")[0]);
			this.end = new Integer(position.split(":")[1].split("-")[1]);
		} else
			if(position.contains("\t")) {
				this.chromosome = position.split("\t")[0];
				this.start = new Integer(position.split("\t")[1]);
				this.end = new Integer(position.split("\t")[2]);
				if (position.split("\t").length > 3)
					this.strand = position.split("\t")[3];
			}
			else throw new InvalidBedStringException("Invalid string. Could not create bed item. '" + position + "'");
				
	}
	
	public BedItem(String chromosome, int start, int end) {
		setChromosome(chromosome);
		setStart(start);
		setEnd(end);
	}
	
	public boolean overlaps(BedItem item, boolean include) {
		
		if(include)
			if(item.start >= start && item.end <= end)
				return true;
		if(!include)
			if((item.start <= start && item.end >= start) ||
					(item.start <= end && item.end >= end) || 
					(item.start <= start && item.end >= end) || 
					(item.start >= start && item.end <= end))
				return true;
		return false;
	}
	
	public int getLength() {
		return end - start;
	}
	public int getCenter() {
		return (start + end) / 2;
	}
	
	public Position getMiddlePosition() {
		return new Position(this.chromosome, this.getCenter());
	}
	
	public String getChromosome() {
		return chromosome;
	}
	public void setChromosome(String chromosome) {
		this.chromosome = chromosome;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getEnd() {
		return end;
	}
	public void setEnd(int end) {
		this.end = end;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	
	public String toBed(boolean full) {
		String str = chromosome + "\t" + start + "\t" + end;
		if (full && name != null) {
			str += "\t" + name;
			if (strand != null) {
				str += "\t" + strand; 
			}
		}
		return str;
	}
	
	@Override
	public String toString() {
		return chromosome + ":" + start + "-" + end;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof BedItem) {
			BedItem bedItem = (BedItem) o;
			if (bedItem.start == this.start && bedItem.end == this.end && bedItem.chromosome == this.chromosome)
				return true;
			else
				return false;
		} else
			return false;
	}

	public int compareTo(BedItem bed) {
		if (this.chromosome.equals(bed.chromosome))
			return this.start - bed.start;
		else {
			String thisChr = this.getChromosome().substring(3);
			String bedChr = bed.getChromosome().substring(3);
			try {
				Integer thisChrInt = new Integer(thisChr);
				Integer bedChrInt = new Integer(bedChr);
				return thisChrInt.compareTo(bedChrInt);
			} catch (Exception e) { /* if they are not numbers */
				return thisChr.compareTo(bedChr);
			}
		}
			
	}

	public void plot(PlotProperties plot, Graphics2D image) {
		image.setPaint(plot.getColor());
		
		// calculate legend levels
		int yLevel1 = plot.getY() + 1 * plot.getHeight() / 3;
		int yLevel2 = plot.getY() + 2 * plot.getHeight() / 3;
		int yLevel3 = plot.getY() + 3 * plot.getHeight() / 3;
		
		// draw legend
		
		Font font = new Font("Arial", Font.BOLD, 14);
	    image.setFont(font);
		FontMetrics fontMetrics = image.getFontMetrics();
		int stringHeight = fontMetrics.getAscent();
		
		String message = "scale";
		int stringWidth = fontMetrics.stringWidth(message);
		image.drawString(message, (plot.getX() - stringWidth) - 10, yLevel1 + 3 * stringHeight / 4);

		message = this.getChromosome() + ":";
		stringWidth = fontMetrics.stringWidth(message);
		image.drawString(message, (plot.getX() - stringWidth) - 10, yLevel2 + 5 * stringHeight / 4);

		int len = this.getLength();
		int bp = 10;
		int bpFactor = 5;
		
		while((len / bp) > 10) {
			bp = bp * bpFactor;
			if(bpFactor == 2) bpFactor = 5; else bpFactor = 2;
		}
		
		int start = plot.convertHorizontal(new Position(
				this.chromosome, this.getStart() + this.getLength() / 2 - bp / 2), this);
		int end = plot.convertHorizontal(new Position(
				this.chromosome, this.getStart() + this.getLength() / 2 + bp / 2), this);		
		image.drawLine(start, yLevel1 + 3, start, yLevel2 - 3);
		image.drawLine(start, (yLevel1 + yLevel2) / 2, end, (yLevel1 + yLevel2) / 2);
		image.drawLine(end, yLevel1 + 3, end, yLevel2 - 3);
		
		if (bp >= 1000) message = "" + (bp / 1000) + " kb";
		else message = "" + bp + " bp";
		stringWidth = fontMetrics.stringWidth(message);
		image.drawString(message, start - stringWidth - 10, yLevel1 + 3 * stringHeight / 4);
		
		int pos = this.getStart() + bp - (this.getStart() % bp);

		/*System.out.println("Start: " + this.getStart());
		System.out.println("Length: " + this.getLength());
		System.out.println("Len: " + len / bp);
		System.out.println("Scale: " + bp);
		System.out.println("Mod: " + pos);*/

		boolean doPrintLegend = true;
		while(pos + bp < this.getEnd()) {
			start = plot.convertHorizontal(new Position(this.chromosome, pos), this);
			end = plot.convertHorizontal(new Position(this.chromosome, pos + bp), this);
			image.drawLine(start, yLevel2, start, yLevel2 + 10);
			image.drawLine((start + end) / 2, yLevel2, (start + end) / 2, yLevel2 + 5);
			image.drawLine(start, yLevel2, end, yLevel2);
			message = "" + pos;			
			stringWidth = fontMetrics.stringWidth(message);
			if(doPrintLegend)
				image.drawString(message, start + 1, yLevel2 + 5 * stringHeight / 4);
			if(len / bp > 4)
				if(doPrintLegend)
					doPrintLegend = false;
				else
					doPrintLegend = true;
			pos = pos + bp;
		}
		image.drawLine(end, yLevel2, end, yLevel2 + 10);
		
		int y = plot.getY() + plot.getHeight() / 4;
		
		int convertedStart = plot.convertHorizontal(new Position(this.chromosome, this.start), this);
		int convertedEnd = plot.convertHorizontal(new Position(this.chromosome, this.end), this);
		int wide = convertedEnd - convertedStart;
		
		//image.drawLine(convertedStart, y, convertedEnd, y);
		
	}
	
	public static void mergeBedItems (List <BedItem> bedItems) {
		Collections.sort(bedItems);
		List <BedItem> mergedItems = new ArrayList<BedItem>();
		BedItem last = null;
		for(BedItem bedItem : bedItems) {
			if (last == null) last = bedItem;
			else {
				if(bedItem.overlaps(last, false))
					last = new BedItem(last.getChromosome(), last.getStart(), bedItem.getEnd());
				else {
					mergedItems.add(last);
					last = bedItem;
				}
			}
		}
		mergedItems.add(last);
		bedItems.clear();
		bedItems.addAll(mergedItems);
	}
	
}
