package org.cremag.genomic;

import org.cremag.utils.genome.Gene;
import org.json.JSONObject;

public class Position implements Comparable<Position> {
	String chromosome;
	int position;
	char base;
	boolean start;
	Gene gene;
	Float relativePosition;
	Float shrinkedPosition;
	String strand;
	
	public Position(String chromosome, int position) {
		this.chromosome = chromosome;
		this.position = position;		
	}
	
	public Position(String chromosome, int position, String strand) {
		this.chromosome = chromosome;
		this.position = position;
		this.strand = strand;
	}
	
	public Position(String chromosome, int position, boolean start, Gene gene) {
		this.chromosome = chromosome;
		this.position = position;
		this.start = start;
		this.gene = gene;
		
	}
	
	public float getRelativePosition() {
		if(relativePosition == null)
			this.relativePosition = this.gene.getRelativePosition(position);
		return relativePosition;
	}
	
	public BedItem getBedItem(int extendBy) {
		return new BedItem(this.chromosome, this.position - extendBy, this.position + extendBy);
	}
	
	public boolean isStart() {
		return start;
	}
	
	public void setStart(boolean start) {
		this.start = start;
	}
	
	public int getPosition() {
		return position;
	}
	
	public String getChromosome() {
		return chromosome;
	}

	public Float getShrinkedPosition() {
		return shrinkedPosition;
	}

	public void setShrinkedPosition(Float shrinkedPosition) {
		this.shrinkedPosition = shrinkedPosition;
	}

	public int getDistanceTo(Position pos) {
		if (!this.chromosome.equals(pos.chromosome))
			return 100000000;
		else
			if(this.position - pos.position >= 0)
				return this.position - pos.position;
			else
				return pos.position - this.position;
	}
	
	@Override
	public String toString() {
		//return this.chromosome + ":" + this.position + "\t" + this.getRelativePosition();
		return this.chromosome + ":" + this.position;
	}

	public int compareTo(Position pos) {
		if (this.chromosome.equals(pos.chromosome))
			return this.position - pos.position;
		else
			return this.chromosome.compareTo(pos.chromosome);
	}
	
	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		json.put("value", this.toString());
		json.put("id", this.toString());
		return json;
	}
}
