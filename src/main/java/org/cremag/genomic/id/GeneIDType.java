package org.cremag.genomic.id;

public enum GeneIDType {
	BED,
	SYMBOL,
	REFSEQ,
	ENSEMBL
}
