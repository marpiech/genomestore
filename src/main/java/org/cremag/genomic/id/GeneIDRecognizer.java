package org.cremag.genomic.id;

import java.util.List;

import org.cremag.dao.GenomicFeaturesDAO;
import org.cremag.genomic.BedItem;
import org.springframework.beans.factory.annotation.Autowired;

public class GeneIDRecognizer {
	
	@Autowired
	GenomicFeaturesDAO genomicDAO;
	
	public GeneIDType identifyQueryType (List<String> queryLines, String genome) {
		
		int	count = 0;
		
		/* counting bed */
		for(String item : queryLines) {
			try {new BedItem(item); count++;
				if (count > 0) return GeneIDType.BED; 
			} catch (Exception e) {/* if not a BedItem than don't count */}
		}
		
		count = genomicDAO.getEnsemblIDsBySymbol(queryLines, genome).size();
		if(count > 0) return GeneIDType.SYMBOL;
		count = genomicDAO.getEnsemblIDsByRefseq(queryLines, genome).size();
		if(count > 0) return GeneIDType.REFSEQ;
		count = genomicDAO.getStartPositionsByEnsemblID(queryLines, genome).size();
		if(count > 0) return GeneIDType.ENSEMBL;
		
		return null;
	}
}
