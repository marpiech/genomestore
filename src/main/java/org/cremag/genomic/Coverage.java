package org.cremag.genomic;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.io.Serializable;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.cremag.utils.plot.PlotProperties;

public class Coverage implements Serializable {
	
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 3296333114022433894L;
	
	BedItem bedItem;
	Map <Integer, Float> coverage;
	
	public Coverage(BedItem bedItem) {
		this.bedItem = bedItem;
		this.coverage = new TreeMap <Integer, Float>();
	}
	
	public void put(int position, int val) {
		this.put(position, (float) val);
	}
	
	public void put(int position, float val) {
		if (position >= bedItem.getStart() && position < bedItem.getEnd())
			coverage.put(position, val);
	}
	
	public void add(int position, float val) {
		Float origin = coverage.get(position);
		if (origin == null)
			origin = val;
		else
			origin += val;
		this.put(position, origin);
	}
	
	public Set<Entry<Integer, Float>> getEntrySet() {
		return coverage.entrySet();
	}

	public float getSum() {
		float sum = 0;
		for(Float value : coverage.values())
			sum= sum + value;
		return sum;
	}
	
	void plotCoverage() {
		
	}

	public float getMax() {
		float max = -10000f;
		for(float val : coverage.values())
			if(val > max)
				max = val;
		if (max < 0)
			max = 0;
		return max;
	}
	
	public float getAverage() {
		float avg = this.getSum() / ((float) this.bedItem.getLength());
		return avg;
	}
	
	public float getFragmentAverage(float percentStart, float percentEnd) {
		int start = bedItem.getStart() + new Float(bedItem.getLength() * percentStart).intValue();
		int end = bedItem.getStart() + new Float(bedItem.getLength() * percentEnd).intValue();
		int length = 0;
		float sum = 0;
		for(int i = start; i <= end; i++) {
			length++;
			Float value = coverage.get(i);
			if (value != null)
				sum = sum + value;
		}
		return sum / length;
	}
	
	public float getValueForPosition(int position) {
		Float value = 0f;
		if (coverage.containsKey(position))
				value = coverage.get(position); 
		return value;
	}
	
	private float getRelativePosition(int position) {
		float relativePosition = ((float) (position - bedItem.getStart())) / 
				((float) bedItem.getLength());
		return relativePosition;
	}
	
	public void plotTrack(Graphics2D image, int horizontalPosition,
			int verticalPosition, int width, int height, Color color) {
		float scaleFactor = getMax();
		this.plotTrack(image, horizontalPosition,
				verticalPosition, width, height, color, 2, scaleFactor, 10);
	}
	public void plotTrack(Graphics2D image, int horizontalPosition,
			int verticalPosition, int width, int height, Color color, int thickness, float scaleFactor, int detail) {
		image.setPaint(color);
		image.setStroke(new BasicStroke(thickness));

		int previousX = horizontalPosition;
		int previousY = verticalPosition + height;
		int currentX = horizontalPosition;
		int currentY = verticalPosition + height;
		for(Entry <Integer, Float> entry : coverage.entrySet()) {
			if(entry.getKey() % detail == 0) {
				currentX = horizontalPosition + (int) (getRelativePosition(entry.getKey()) * (float) width);
				currentY = verticalPosition + height - (int) ((float) height * entry.getValue() / scaleFactor);
				image.drawLine(previousX, previousY, currentX, currentY);
				previousX = currentX;
				previousY = currentY;
			}
		}
		previousX = horizontalPosition + width;
		previousY = verticalPosition + height; 
		image.drawLine(previousX, previousY, currentX, currentY);
	}

	public void plotTrack(PlotProperties plot, Graphics2D image) {
		
		// setting color
		image.setPaint(plot.getColor());
		
		// printing name
		Font font = new Font("Arial", Font.BOLD, 14);
	    image.setFont(font);
		FontMetrics fontMetrics = image.getFontMetrics();
		int stringHeight = fontMetrics.getAscent();
		String message = plot.getLegend();
		int stringWidth = fontMetrics.stringWidth(message);
		image.drawString(message, plot.getX(), plot.getY() + stringHeight + 2);
		
		// setting scale
		float scaleFactor = getMax();
		//System.out.println("Factor: " + scaleFactor);
		if (scaleFactor < 2f)
			scaleFactor = 2f;
		
		// drawing scale
		image.setFont(new Font("Arial", Font.PLAIN, 10));
		message = "" + ((int) scaleFactor);
		stringWidth = fontMetrics.stringWidth(message);
		image.drawString(message, plot.getX() - 8 - stringWidth, plot.getY() + stringHeight / 2);
		
		image.setStroke(new BasicStroke(1));
		image.drawLine(plot.getX() -7 , plot.getY(), plot.getX() -7, plot.getY() + plot.getHeight());
		for(int i = 0; i <= 1; i++) {
			int y = plot.getY() + i * plot.getHeight() / 1;
			image.drawLine(plot.getX() - 7, y, plot.getX() -2, y);
		}
		
		// drawing bottom line
		image.drawLine(plot.getX(), plot.getY() + plot.getHeight(), plot.getX() + plot.getWidth(), plot.getY() + plot.getHeight());
		
		// drawing coverage
		image.setStroke(new BasicStroke(1));
		int previousX = plot.getX();
		int previousHeight = 0;
		for(Entry <Integer, Float> entry : coverage.entrySet()) {
			Position x = new Position("", entry.getKey());
			int currentX = plot.convertHorizontal(x, this.bedItem);
			int currentHeight = (int) ((float) plot.getHeight() * entry.getValue() / scaleFactor);
			if (currentX > previousX) {

				image.fillRect(previousX, plot.getY() + plot.getHeight() - previousHeight, 1, previousHeight);
				previousX = currentX;				
				previousHeight = currentHeight;
				
			} else {
				previousX = currentX;
				if(currentHeight > previousHeight) {
					previousHeight = currentHeight;
				}
			}
			
			//previousX = currentX;
			//previousHeight = currentHeight;
		}
		previousHeight = plot.getY() + plot.getHeight(); 
		//image.fillRect(previousX, plot.getY() + plot.getHeight() - previousHeight, 1, previousHeight);
		//image.drawLine(previousX, previousY, currentX, currentY);
		
	}
	@Override
	public String toString() {
		String out = "";
		for(Entry<Integer, Float> entry : coverage.entrySet()) {
			out += entry.getKey() + ": " + entry.getValue() + "\n";
		}
		return out;
	}
}
