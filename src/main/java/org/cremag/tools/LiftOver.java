package org.cremag.tools;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.cremag.genomic.BedItem;

public class LiftOver {

	public static BedItem lift(String from, String to, BedItem sourceItem) throws IOException, InterruptedException {
//		System.out.println("FROM: " + from);
//		System.out.println("TO: " + to);
//		System.out.println("BED" + sourceItem);
		List <BedItem> oneElementList = new ArrayList <BedItem> ();
		oneElementList.add(sourceItem);
		return lift(from , to, oneElementList).get(0);
	}
	
	public static List<BedItem> lift(String from, String to, List <BedItem> sourceItems) throws IOException, InterruptedException {

		/* convert bed items to string */
		String bedString = "\"";
		for(BedItem item : sourceItems)
			bedString += item.toBed(false) + "\n";
		bedString = bedString.trim() + "\"";

		/* set chain file */
		String prefix = "src/main/resources/liftover/";
		String chainFile = null;
		String options = "";
		
		// TODO: hacks, hacks and hacks
		if(from.equals("hg19") && to.equals("mm9")) {
			chainFile = prefix + "hg19ToMm9.over.chain.gz";
			options = "-minMatch=0.1";
		}

		if(from.equals("mm10") && to.equals("mm9")) {
			chainFile = prefix + "mm10ToMm9.over.chain.gz";
			options = "";
		}
		
		if(from.equals("mm10") && to.equals("hg19")) {
			chainFile = prefix + "mm10ToHg19.over.chain.gz";
			options = "-minMatch=0.1";
		}

		if(from.equals("mm9") && to.equals("hg19")) {
			chainFile = prefix + "mm9ToHg19.over.chain.gz";
			options = "-minMatch=0.1";
		}
		
		InputStream procIn = null;

		try {
			OutputStream os = new ByteArrayOutputStream();
			
			String[] cmd = {
					"/bin/sh",
					"-c",
					"echo " + bedString + " | " + "src/main/resources/liftover/liftOver " + options + " /dev/stdin " + chainFile + " /dev/stdout /dev/null"
					};
			
			ProcessBuilder builder = new ProcessBuilder(cmd);
			builder.redirectErrorStream(true);
			Process process = builder.start();
			procIn = process.getInputStream();
			process.waitFor();
			pipeStream(procIn, os);
			process.destroy();
			
			List <BedItem> liftedItems = new ArrayList <BedItem>();
			for(String line : os.toString().split("\n")) {
				try {liftedItems.add(new BedItem(line));
				} catch (Exception e) {}
			}
			return liftedItems;
			
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return null;
	}

	public static void pipeStream(InputStream input, OutputStream output)
			throws IOException {
		
		byte buffer[] = new byte[1024];
		int numRead = 0;

		do {
			numRead = input.read(buffer);
			output.write(buffer, 0, numRead);
		} while (input.available() > 0);

		output.flush();
	}

}
