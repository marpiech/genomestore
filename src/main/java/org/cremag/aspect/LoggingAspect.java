package org.cremag.aspect;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.cremag.utils.NetUtils;
 
@Aspect
public class LoggingAspect {
 
	SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm");
	
	@Before("execution(* org.cremag.seqinterpreter.SeqinterpreterController.*(..))" + " || " +
			"execution(* org.cremag.seqinspector.SeqinspectorController.*(..))" + "||" + 
			"execution(* org.cremag.genomeStore.web.*Controller.*(..))")
	public void logController(JoinPoint joinPoint) {
 
		String ip = "unknown";
		//String method = joinPoint.getSignature().getName();
		String date = dt.format(new Date());
		String clazz = joinPoint.getTarget().getClass().getCanonicalName();
		String uri = "";
		clazz = clazz.substring(clazz.lastIndexOf(".") + 1);
		for (Object obj : joinPoint.getArgs()) {
			if (obj instanceof HttpServletRequest) {
				HttpServletRequest request = (HttpServletRequest) obj;
				ip = NetUtils.getIP(request);
				uri = request.getRequestURI();
			}
		}
		System.out.println("NET " + date + " : " + clazz + " - Requested " + uri + " from " + ip);
	}
 
}
