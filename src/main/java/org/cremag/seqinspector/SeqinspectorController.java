/*
 * Copyright (C) 2012 - Marcin Piechota
 * Licensed under the Creative Commons Attribution-NonCommercial 3.0 Unported (CC BY-NC 3.0)
 * You may obtain a copy of the License at http://creativecommons.org/licenses/by-nc/3.0/legalcode
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

package org.cremag.seqinspector;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.JPEGTranscoder;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.log4j.Logger;
import org.cremag.dao.GenomicFeaturesDAO;
import org.cremag.genomeStore.Genome;
import org.cremag.genomeStore.GenomeStore;
import org.cremag.genomeStore.QuerySet;
import org.cremag.genomic.BedItem;
import org.cremag.genomic.Position;
import org.cremag.genomic.id.GeneIDRecognizer;
import org.cremag.genomic.id.GeneIDType;
import org.cremag.tools.LiftOver;
import org.cremag.utils.file.genomic.GenomicTrack;
import org.cremag.utils.file.genomic.GenomicTrack.TrackClass;
import org.cremag.utils.plot.ColorBrewer;
import org.cremag.utils.plot.PlotProperties;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author marpiech
 */
@Controller
public class SeqinspectorController {

	private static final Logger logger = Logger.getLogger(SeqinspectorController.class);
	
	@Autowired
	GenomeStore genomeStore;
	
	@Autowired
	GenomicFeaturesDAO genomicDAO;
	
	@Autowired
	GeneIDRecognizer geneIDRecognizer;
	
	/* Core view of the seqinspector */
	@RequestMapping("/seqinspector.htm")
	public ModelAndView seqinspectorRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		ModelAndView mav = new ModelAndView("seqinspector");
		return mav;
		
	}
	
	@RequestMapping(value = "/gettss.htm", method = RequestMethod.GET)
	public @ResponseBody
	String gettss(HttpServletRequest request, @RequestParam String symbol, @RequestParam String genome){
	
		List <String> query = new ArrayList <String>();
		query.add(symbol);
		GeneIDType geneIDType = geneIDRecognizer.identifyQueryType(query, genome);
		
		JSONArray jsonArray = new JSONArray();
		
		if (geneIDType == GeneIDType.SYMBOL) {
			List <Position> tss = this.genomicDAO.getPositionsBySymbol(symbol, genome, 500);
			
			for(Position pos : tss)
				jsonArray.put(pos.toJSON());
		}
		
		if (geneIDType == GeneIDType.BED) {
			BedItem bed = new BedItem(symbol);
			Position pos = bed.getMiddlePosition();
			JSONObject json = pos.toJSON();
			json.put("extend", bed.getLength() / 2);
			jsonArray.put(json);
		}

		if (geneIDType == GeneIDType.REFSEQ) {
			Position position = this.genomicDAO.getPositionsByRefseq(query, genome).get(0);
			jsonArray.put(position.toJSON());				
		}

		if (geneIDType == GeneIDType.ENSEMBL) {
			Position position = this.genomicDAO.getStartPositionsByEnsemblID(query, genome).get(0);
			jsonArray.put(position.toJSON());				
		}

		if (jsonArray.length() < 1) throw new RuntimeException("Unidentified query");
		return jsonArray.toString();
    }
	
	@RequestMapping("/getscoresforbed.htm")
	public void bedScoresRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		String originGenome = request.getParameter("genome");
		Genome genome = genomeStore.getGenome(request.getParameter("database"));
		BedItem bed = new BedItem(request.getParameter("range"));
		
		Set <TrackClass> filters = TrackClass.getFilters(request.getParameter("filters"));
		QuerySet querySet = genome.getBackgroundById(request.getParameter("background").toString());
		
		/*logger.debug("Calculating z-scores for: " + bed + 
				" genome: " + originGenome + 
				" database: " + genome.getName() +
				" background: " + querySet.getId());*/
		
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "no-cache");

		/* lift over genome of origin to genomeStore genome */ 
		if(!originGenome.equals(genome.getName()))
			bed = LiftOver.lift(originGenome, genome.getName(), bed);
			//bed = genome.liftFrom(originGenome, bed);
	
		/*logger.debug("Lifted bed " + bed);*/
		
		JSONArray jsonArray = querySet.zScoreTest(bed, filters).getJSON();
		
		ServletOutputStream out = response.getOutputStream();
		out.print(jsonArray.toString());
	}
	
	@RequestMapping("/getscoresforbedcsv.htm")
	public void bedScoresRequestCSV(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		String originGenome = request.getParameter("genome");
		Genome genome = genomeStore.getGenome(request.getParameter("database"));
		BedItem bed = new BedItem(request.getParameter("range"));
		
		Set <TrackClass> filters = TrackClass.getFilters(request.getParameter("filters"));
		QuerySet querySet = genome.getBackgroundById(request.getParameter("background").toString());
		
		/*logger.debug("Calculating z-scores for: " + bed + 
				" genome: " + originGenome + 
				" database: " + genome.getName() +
				" background: " + querySet.getId());*/
		
		response.setContentType("application/json");
		response.setHeader("Content-Disposition", "attachment; filename=statistics.tsv");
		response.setHeader("Cache-Control", "no-cache");

		/* lift over genome of origin to genomeStore genome */ 
		if(!originGenome.equals(genome.getName()))
			bed = LiftOver.lift(originGenome, genome.getName(), bed);
			//bed = genome.liftFrom(originGenome, bed);
	
		/*logger.debug("Lifted bed " + bed);*/
		
		String csv = querySet.zScoreTest(bed, filters).getCSV();
		
		ServletOutputStream out = response.getOutputStream();
		out.print(csv);
	}
	
	/**
	 * @param request
	 * @param response
	 * @throws Exception
	 * plots svg histogram
	 */
	@RequestMapping("/getrangehist.htm")
	public void rangeHistRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		//logger.info("Requested /getrangehist.htm from " + request.getRemoteAddr());
		
		String originGenome = request.getParameter("genome");
		Genome genome = genomeStore.getGenome(request.getParameter("database"));
		BedItem bed = new BedItem(request.getParameter("range"));
		boolean svg = true;
		if (request.getParameter("jpg") != null) {
			svg = false;
		}
		String[] tracks = request.getParameter("tracks").toString().split(",");
		
		//logger.debug("Genomic plot for:: bed: " + bed + " sourceGenome: " + originGenome + " targetGenome: " + genome.getName());
		
		/* liftover if source and target are not the same genomes */
		if(!originGenome.equals(genome.getName()))
			bed = LiftOver.lift(originGenome, genome.getName(), bed);
		
		if (svg)
			response.setContentType("image/svg+xml");
		else {
			//response.setContentType("image/jpeg");
			response.setContentType("image/svg+xml");
			response.setHeader("Content-Disposition", "attachment; filename=histogram.svg"); 
		}
		
		SVGGraphics2D svgGenerator = new SVGGraphics2D(
        		GenericDOMImplementation.getDOMImplementation().createDocument(
        				"http://www.w3.org/2000/svg", "svg", null)
        				);
		
		PlotProperties plot = new PlotProperties();
		plot.setX(80);
		plot.setY(0);
		plot.setWidth(600);
		plot.setHeight(40);
		plot.setColor(ColorBrewer.GREY_80);
		bed.plot(plot, svgGenerator);
		
		plot.setY(plot.getY() + plot.getHeight() + 20);
		plot.setHeight(50);
		plot.setLegend("Ensembl genes");
		
		GenomicTrack track = genome.selectTracksByKeyAndElement("name", "ensembl").iterator().next();
		track.plot(plot, svgGenerator, bed);
		
		if (tracks != null)
		for(String trackId : tracks) {
			
			//logger.trace("Plotting: " + trackId);
			
			track = genome.getTrackById(trackId);
			plot.setLegend(track.getName());
			plot.changeColor();
			plot.setY(plot.getY() + plot.getHeight() + 5);
			plot.setHeight(40);
			track.plot(plot, svgGenerator, bed);
		}
		
		svgGenerator.setSVGCanvasSize(new Dimension(plot.getWidth(), plot.getY() + plot.getHeight() + 5));
		
		//TransformerFactory transfac = TransformerFactory.newInstance();
		//Transformer trans = transfac.newTransformer();
		//trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		//trans.setOutputProperty(OutputKeys.INDENT, "yes");

		//create string from xml tree
		StringWriter sw = new StringWriter();
		svgGenerator.stream(sw);
		String xmlString = sw.toString();
		
		ServletOutputStream out = response.getOutputStream();
		
		out.print(xmlString);
		
		/*else {
			BufferedImage image = ImageIO.read(new InputStream(svgGenerator.str));
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			ImageIO.write(svgGenerator,"jpeg", os);
		}*/
		out.close();
		
	}

	/**
	 * @param request
	 * @param response
	 * @throws Exception
	 * plots svg histogram
	 */
	@RequestMapping("/getrangehistjpeg.htm")
	public void rangeHistRequestJpeg(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		String originGenome = request.getParameter("genome");
		Genome genome = genomeStore.getGenome(request.getParameter("database"));
		BedItem bed = new BedItem(request.getParameter("range"));
		boolean svg = true;
		String[] tracks = request.getParameter("tracks").toString().split(",");
		
		if(!originGenome.equals(genome.getName()))
			bed = LiftOver.lift(originGenome, genome.getName(), bed);
		
		response.setContentType("image/png");
		response.setHeader("Content-Disposition", "attachment; filename=histogram.png"); 

		int height = 110 + 45 * tracks.length + 20;
		int width = 800;
		
		BufferedImage bufferedImage = new BufferedImage(width, height,
	            BufferedImage.TYPE_INT_RGB);
		
		Graphics2D svgGenerator = bufferedImage.createGraphics();
		
		svgGenerator.setColor(Color.WHITE);
		svgGenerator.fillRect(0, 0, width, height);
		
		PlotProperties plot = new PlotProperties();
		plot.setX(80);
		plot.setY(0);
		plot.setWidth(600);
		plot.setHeight(40);
		plot.setColor(ColorBrewer.GREY_80);
		bed.plot(plot, svgGenerator);
		
		plot.setY(plot.getY() + plot.getHeight() + 20);
		plot.setHeight(50);
		plot.setLegend("Ensembl genes");
		
		GenomicTrack track = genome.selectTracksByKeyAndElement("name", "ensembl").iterator().next();
		track.plot(plot, svgGenerator, bed);
		
		if (tracks != null)
		for(String trackId : tracks) {
			
			//logger.trace("Plotting: " + trackId);
			
			track = genome.getTrackById(trackId);
			plot.setLegend(track.getName());
			plot.changeColor();
			plot.setY(plot.getY() + plot.getHeight() + 5);
			plot.setHeight(40);
			track.plot(plot, svgGenerator, bed);
		}
		
				
		ServletOutputStream out = response.getOutputStream();
		ImageOutputStream ios = ImageIO.createImageOutputStream(out);
		ImageIO.write(bufferedImage, "png", ios);
		ios.close();
		out.close();
		
	}
}