package pl.intelliseq.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * Hello world!
 */
public class BashExecutor {

	public String execute(String processString, String stdinString)
			throws IOException, InterruptedException {

		String out = "";

		ProcessBuilder pb = new ProcessBuilder("/bin/sh", "-c", processString);
		pb.redirectErrorStream(true);

		Process process = pb.start();
		
		OutputStream stdin = process.getOutputStream();
		InputStream stdout = process.getInputStream();
		BufferedWriter writer = new BufferedWriter(
				new OutputStreamWriter(stdin));
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(stdout));

		writer.write(stdinString);
		writer.newLine();
		writer.flush();
		writer.close();

		process.waitFor();
		while (reader.ready())
			out += reader.readLine() + "\n";
		process.waitFor();
		process.destroy();

		return out;

	}

	public String execute(String processString)
			throws IOException, InterruptedException {

		String out = "";

		ProcessBuilder pb = new ProcessBuilder("/bin/sh", "-c", processString);
		pb.redirectErrorStream(true);

		Process process = pb.start();
		
		InputStream stdout = process.getInputStream();
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(stdout));

		process.waitFor();
		while (reader.ready())
			out += reader.readLine() + "\n";
		process.waitFor();
		process.destroy();

		return out;

	}
	
}
